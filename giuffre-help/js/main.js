function accendiCard(pulsante) {
    $(pulsante).parents('.schedaDomanda').each(
        function () {
            $(this).css("opacity", 1);
        }
    )
}

function spegniCard(pulsante) {
    $(pulsante).parents('.schedaDomanda').each(
        function () {
            $(this).css("opacity", 0.2);
        }
    )
}

function mostraContenutoCard(pulsante, altezzaCard) {
    $(pulsante)
        .attr("data-interruttoreAcceso", true)
        .html('Nascondi')
        .parents('.card-style-1')
        .find('.card-body')
        .stop()
        .slideDown()
    ;
    // $(pulsante).parents('.card-style-1').each(
    //     function () {
    //         $(this).removeClass('size-height70');
    //         $(this).addClass('size-height' + altezzaCard);
    //     }
    // )
}

function nascondiContenutoCard(pulsante, altezzaCard) {
    $(pulsante)
        .removeAttr("data-interruttoreAcceso")
        .html('Mostra')
        .parents('.card-style-1')
        .find('.card-body')
        .stop()
        .slideUp()
    ;

    // $(pulsante).parents('.card-style-1').each(
    //     function () {
    //         $(this).removeClass('size-height' + altezzaCard)
    //         $(this).addClass('size-height70');
    //     }
    // )
}

function accendiPulsante(pulsante) {
    $(pulsante).attr("data-interruttoreAcceso", true);
    $(pulsante).removeClass("button-style2");
    $(pulsante).addClass("button-style1");
}

function spegniPulsante(pulsante) {
    $(pulsante).removeAttr("data-interruttoreAcceso");
    $(pulsante).removeClass("button-style1");
    $(pulsante).addClass("button-style2");
}

$(document).ready(function () {

    var mobileBreakpoint = 1024;

    $('.interruttoreTipoCard').click(
        function () {
            var isPulsantePremutoAcceso = ($(this).attr("data-interruttoreAcceso") === "true");
            var tipoPulsantePremuto = $(this).attr("data-interruttoreTipoCard");
            $('div.schedaDomanda').find('button[data-interruttoreTipoCard!="' + tipoPulsantePremuto + '"]').each(
                function () {
                    if (isPulsantePremutoAcceso) {
                        accendiCard(this); // riaccendo le altre card
                    } else {
                        spegniPulsante(this); // spengo gli altri pulsanti
                        spegniCard(this); // spengo le altre card
                    }
                }
            );
            $('div.schedaDomanda').find('button[data-interruttoreTipoCard="' + tipoPulsantePremuto + '"]').each( // agisco dopo sulle rilevanti, visto che ci sono card che possono avere pulsanti di un tipo e dell'altro, e con ordine inverso verrebbero spente anche quando contengono un pulsante rilevante
                function () {
                    if (isPulsantePremutoAcceso) {
                        spegniPulsante(this); // mi limito a spegnere tutti i pulsanti del tipo selezionato
                    } else {
                        accendiPulsante(this); // accendo tutti i pulsanti del tipo selezionato
                        accendiCard(this); // accendo le relative card se ho premuto un pulsante di una card sfumata
                    }
                }
            )
        });
    $('.interruttoreContenutoCard').click(
        function () {
            var isPulsantePremutoAcceso = ($(this).attr("data-interruttoreAcceso") === "true");
            var altezzaCard = $(this).attr("data-interruttoreContenutoCard");
            if (isPulsantePremutoAcceso) {
                nascondiContenutoCard(this, altezzaCard);
            } else {
                mostraContenutoCard(this, altezzaCard);
            }
        });


    $('.btn-menu').click(function () {
        if (window.innerWidth < mobileBreakpoint-1) {
            console.log(1)
            $('.btn-menu span').toggle();
            $('#side-menu').toggle();
        }
    });

    $('#side-menu a').click(function () {
        if (window.innerWidth < mobileBreakpoint-1) {
            $('.btn-menu span').toggle();
            $('#side-menu').hide();
        }
    });

    $(window).resize(function () {
        if (window.innerWidth > mobileBreakpoint) {
            $('#side-menu').show();
        } else {
            $('#side-menu').hide();
        }
        $('.btn-menu__hide').hide();
        $('.btn-menu__show').show();
    });

});
