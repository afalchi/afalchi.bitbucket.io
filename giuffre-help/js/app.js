var app = angular.module('docs', [])

    .config(function ($locationProvider) {
        // $locationProvider.hashPrefix('!');
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    })

    .controller('menuController', function ($anchorScroll, $location) {
        this.scrollTo = function (hash) {
            $location.hash(hash);
            $anchorScroll();
        };
    })

    .controller('comeFaccioController', function ($location) {
        var vm = this;

        vm.categories = [
            {
                label: "generale",
                colorClass: ""
            },
            {
                label: "depositi",
                colorClass: "secondary"
            },
            {
                label: "notifiche",
                colorClass: "info"
            },
            {
                label: "polisweb",
                colorClass: "calm"
            },
            {
                label: "fatturazione",
                colorClass: "neutral"
            },
            {
                label: "agenda",
                colorClass: "danger"
            },
        ];

        vm.activeCategories = [];
        vm.searchResults = [];
        vm.currentOpen = '';
        vm.visibleItems = [];

        vm.setActiveCategory = function (category) {
            var catIndex = vm.activeCategories.indexOf(category);
            catIndex === -1 ?
                vm.activeCategories.push(category) :
                vm.activeCategories.splice(catIndex, 1)
            ;
        };

        vm.isActive = function (category) {
            return vm.activeCategories.indexOf(category) >= 0;
        };

        vm.query = '';

        vm.search = function (query) {
            vm.visibleItems = [];
            $('.schedaDomanda').each(function (i, e) {
                if (e.innerText.toLowerCase().indexOf(query.toLowerCase()) >= 0) {
                    vm.visibleItems.push(e.dataset.id);
                }
            });
        };

        vm.setCurrentOpen = function (id) {
            id ? vm.currentOpen = id : vm.currentOpen = '';
        };
    })

    .component('schedaDomanda', {
        bindings: {
            activeCategories: '<',
            currentOpen: "<",
            id: "@",
            onUpdate: '&',
            tags: "@",
            title: '@',
            visible: '<'
        },
        controller: function ($location) {
            var ctrl = this;

            ctrl.$onInit = function () {

                ctrl.isOpen = $location.hash() === ctrl.id || ctrl.currentOpen === ctrl.id;
                ctrl.pills = ctrl.tags.split(',').map(function (item) {
                    return item.trim();
                });

                ctrl.isVisible = function () {
                    return ctrl.visible.length === 0 || ctrl.visible.indexOf(ctrl.id) >= 0 ? 
                        true : false
                    ;

                    return ctrl.activeCategories.length === 0 || ctrl.activeCategories.some(function (item) {
                        return ctrl.pills.indexOf(item.label) >= 0;
                    });
                }

                ctrl.toggle = function () {
                    !ctrl.isOpen ? ctrl.onUpdate({ id: ctrl.id }) : ctrl.onUpdate({ id: null });
                };
            }

            ctrl.$onChanges = function (changes) {
                ctrl.isOpen = ctrl.currentOpen === ctrl.id;
            };

        },
        require: {
            // comeFaccioCtrl: '^comeFaccioController'
        },
        templateUrl: 'scheda-domanda',
        transclude: true
    })

    ;