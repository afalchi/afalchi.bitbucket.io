'use strict';

{
  $(function () {
    $('.lanci-fiere-news .news-group').slick({
      mobileFirst: true,
      dots: true,
      arrows: false,
      responsive: [{
        breakpoint: 992,
        settings: 'unslick'
      }]
    });
  });
}
//# sourceMappingURL=lanci-fiere-news.js.map
