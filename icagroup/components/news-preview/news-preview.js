'use strict';

{

  $(function () {

    $('.news-preview .slider').slick({
      arrows: false,
      mobileFirst: true,
      responsive: [{
        breakpoint: 576,
        settings: {
          slidesToShow: 2
        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 2
        }
      }, {
        breakpoint: 992,
        settings: {
          slidesToShow: 3
        }
      }]
    });
  });
}
//# sourceMappingURL=news-preview.js.map
