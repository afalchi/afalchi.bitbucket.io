'use strict';

(function () {

  new Vue({
    el: '#test-request',
    data: {
      filter: {
        tipologia: 'all',
        settore: 'all',
        linea: 'all',
        query: ''
      },
      filteredList: [], // _.cloneDeep(list),
      fullList: [],
      selectedItems: [],
      testList: []
    },
    methods: {
      filterBySelect: function filterBySelect(selectFilter) {
        var vm = this;

        if (vm.filter[selectFilter] === 'all') {
          this.resetFilters();
        } else {
          vm.resetFilterObject(selectFilter);
          vm.filteredList.forEach(function (item) {
            item[selectFilter] === vm.filter[selectFilter] ? item.visible = true : item.visible = false;
          });
        }
      },
      filterByQuery: function filterByQuery() {
        var vm = this;
        vm.filteredList.forEach(function (item) {
          var has = void 0;
          for (var prop in item) {
            if (typeof item[prop] === 'string') {
              if (item[prop].toLowerCase().includes(vm.filter.query.toLowerCase())) {
                item.visible = true;
                break;
              } else {
                item.visible = false;
              }
            }
          }
        });

        // 
        vm.filter.query = '';
      },
      resetFilters: function resetFilters() {
        this.filteredList.forEach(function (item) {
          return item.visible = true;
        });
        this.resetFilterObject();
      },
      resetFilterObject: function resetFilterObject(dontReset) {

        if (!dontReset) {
          this.filter = {
            tipologia: 'all',
            settore: 'all',
            linea: 'all',
            query: ''
          };
        } else {
          for (var key in this.filter) {
            key !== dontReset && key !== 'query' ? this.filter[key] = 'all' : null;
            key === 'query' ? this.filter[key] = '' : null;
          }
        }

        // forEach( item => item.visible = true )
      },
      selectItem: function selectItem(itemIndex) {
        var vm = this;

        if (vm.filteredList[itemIndex].samples > 0) {
          vm.filteredList[itemIndex].samplesError = false;
          vm.selectedItems.push({
            index: itemIndex,
            samples: vm.filteredList[itemIndex].samples
          });
          vm.filteredList[itemIndex].selected = true;
          $('html, body').animate({ scrollTop: $('.test-request__footer').offset().top }, 1000);
        } else {
          vm.filteredList[itemIndex].samplesError = true;
        }
      },
      toggleSelection: function toggleSelection(itemIndex) {
        var vm = this;

        if (vm.filteredList[itemIndex].selected) {
          vm.unselectItem(itemIndex);
        } else {
          vm.selectedItems.push({
            index: itemIndex,
            samples: vm.filteredList[itemIndex].samples
          });
          vm.filteredList[itemIndex].selected = true;
        }
      },
      unselectItem: function unselectItem(itemIndex) {
        var vm = this;
        vm.selectedItems = vm.selectedItems.filter(function (item) {
          return item.index !== itemIndex;
        });
        vm.filteredList[itemIndex].selected = false;
        vm.filteredList[itemIndex].samples = 0;
      }
    },
    mounted: function mounted() {
      var vm = this;

      $.get('pages/richiedi-test/data/test.json').done(function (response) {
        vm.fullList = response;

        // Estensione dell'oggetto nel json per gestione
        // app Vue
        vm.filteredList = response.map(function (item) {
          Vue.set(item, 'samples', 0);
          Vue.set(item, 'selected', false);
          Vue.set(item, 'visible', true);
          Vue.set(item, 'samplesError', false);
          return item;
        });
      }).fail(function () {
        alert('Error loading data!');
      });
    }
  });

  /* -----------------------
    DOC READY
  ------------------------*/
  $(function () {

    // Scroll reveal
    setTimeout(function () {
      scrollReveal.reveal('.tests-website [data-reveal]', {
        distance: '20%',
        duration: 1000,
        interval: 50,
        scale: 1,
        viewFactor: 0.7
      });
    }, 200);
  });
})();
//# sourceMappingURL=test-request.js.map
