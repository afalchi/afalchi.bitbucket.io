'use strict';

$(function () {

  $('.sottolinee-lanci .slider').on('init', function (ev, slider) {

    console.log(slider);

    // Nasconde punti se c'è una sola slide (bug di slick)
    if (slider.options.slidesToShow >= slider.slideCount) {
      $(slider.$slider).find('.slick-dots').hide();
    }
  }).slick({
    arrows: false,
    dots: true,
    mobileFirst: true,
    infinite: false,
    responsive: [{
      breakpoint: 576,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 992,
      settings: {
        slidesToShow: 4
      }
    }, {
      breakpoint: 1400,
      settings: {
        slidesToShow: 5
      }
    }]
  });
});
//# sourceMappingURL=sottolinee-lanci.js.map
