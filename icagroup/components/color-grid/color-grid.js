'use strict';

{

  /* ------------------------------
    Filtri
  ------------------------------ */
  Vue.component('color-filter', {
    data: function data() {
      return {
        active: false
      };
    },

    methods: {
      selected: function selected(ev) {
        this.active = !this.active;
        this.$emit('selected');
      }
    }
  });

  /* ------------------------------
  Color tile
  ------------------------------ */
  Vue.component('color-tile', {
    data: function data() {
      return {
        isSelected: false,
        row: 0
      };
    },

    props: {
      currentHoverItem: {
        type: Number,
        required: true
      },
      data: {
        type: Object,
        required: true
      },
      index: {
        type: Number,
        required: true
      },
      image: {
        type: String,
        required: true
      },
      isDetailOpen: {
        type: Boolean,
        required: true
      },
      json: {
        type: String,
        required: true
      },
      openId: {
        type: Number,
        required: true
      }
    },
    methods: {
      openDetail: function openDetail(ev) {},
      mouseenterHandler: function mouseenterHandler() {
        this.$emit('hover', this.index);
      },
      mouseoutHandler: function mouseoutHandler() {
        this.$emit('hover', -1);
      }
    },
    mounted: function mounted() {
      var vm = this;
    }
  });
  // colorTile()


  /* ------------------------------
    Grid component
  ------------------------------ */
  Vue.component('color-grid', {
    data: function data() {
      return {
        colorsData: undefined,
        colorsFilteredList: [],
        currentPage: 0,
        currentTilesPerPage: 16,
        detailData: undefined,
        filterKeys: ['kerlite', '14mm', '20mm'],
        hoverItemId: -1,
        openId: 0,
        openRow: 0,
        isDetailOpen: false,
        showNav: false,
        screenSize: 'sm',
        tilesPerPage: {
          xxs: 60,
          sm: 60,
          lg: 60
        },
        totalPages: 0
      };
    },

    methods: {
      closeDetail: function closeDetail() {
        this.isDetailOpen = false;
      },
      filter: function filter(key) {

        var vm = this,
            keyIndex = this.filterKeys.indexOf(key);
        this.isDetailOpen = false;
        keyIndex === -1 ? this.filterKeys.push(key) : this.filterKeys.splice(keyIndex, 1);
        this.colorsFilteredList = this.colorsData.filter(function (item) {
          var has = false;
          vm.filterKeys.forEach(function (filter) {
            return item.tech.includes(filter) ? has = true : null;
          });
          return has;
        });
      },
      goNext: function goNext() {
        this.currentPage += 1;
        $('.slider', this.$el).slick('slickNext');
      },
      goPrev: function goPrev() {
        this.currentPage -= 1;
        $('.slider', this.$el).slick('slickPrev');
      },
      initSlider: function initSlider() {
        var _this = this;

        Vue.nextTick(function () {
          $('.slider', _this.$el).slick({
            arrows: false,
            dots: false,
            infinite: true
          });
        });
      },
      loadDetailsJSON: function loadDetailsJSON(ev) {
        var _this2 = this;

        $.get(ev.json).done(function (response) {
          Vue.nextTick(function () {
            _this2.detailData = response;
            _this2.openRow = ev.row;
            _this2.openId = ev.id;
            _this2.isDetailOpen = true;
          });
        }).fail(function (err) {
          _this2.isDetailOpen = false;
          alert('Error loading JSON');
        });
      },
      setOpenRow: function setOpenRow(ev) {
        this.loadDetailsJSON(ev);
      },
      setHoverItem: function setHoverItem(ev) {
        this.hoverItemId = ev;
      }
    },
    watch: {
      screenSize: function screenSize(oldVal, newVal) {
        // Distrugge e riavvia slick quando cambia il breakpoint
        this.currentTilesPerPage = this.tilesPerPage[this.screenSize];
        this.totalPages = Math.ceil(this.colorsData.length / this.currentTilesPerPage);

        $('.slider', this.$el).slick('unslick');
        this.currentPage = 0;
        this.initSlider();
      }
    },
    mounted: function mounted() {

      var vm = this,
          colorsJSON = vm.$el.dataset.json;

      // Carica JSON tessere colori
      $.get(colorsJSON).done(function (response) {
        Vue.nextTick(function () {
          vm.colorsData = response;
          vm.colorsFilteredList = response;
          vm.totalPages = Math.ceil(vm.colorsData.length / vm.currentTilesPerPage);
          vm.initSlider();
        });
      }).fail(function (err) {
        alert('Error loading colors JSON');
      });

      var resizeHandler = _.debounce(function () {
        Vue.nextTick(function () {
          if (window.innerWidth < 576) {
            vm.screenSize = 'xxs';
          } else if (window.innerWidth >= 576 && window.innerWidth < 992) {
            vm.screenSize = 'sm';
          } else {
            vm.screenSize = 'lg';
          }
        });
      }, 200);

      window.addEventListener('resize', resizeHandler);
      resizeHandler();
    }
  });

  /* ------------------------------
  Main app
  Aggancia Vue a ogni istanza di ".scheda__colori"
  ------------------------------ */
  new Vue({
    el: '.scheda__colori'
  });
}
//# sourceMappingURL=color-grid.js.map
