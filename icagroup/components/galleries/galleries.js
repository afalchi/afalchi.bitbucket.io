'use strict';

(function () {

  /* ------------------------------------ 
    Vertical Gallery
  ------------------------------------ */
  function vertical() {

    if (!document.querySelector('.vertical-gallery')) {
      return;
    }

    new Vue({
      el: '.vertical-gallery',
      data: {
        activeIndex: 0
      },
      computed: {
        slides: function slides() {
          return $('.slide', this.$el).length;
        }
      },
      methods: {
        gotoSlide: function gotoSlide(index) {
          this.activeIndex = index;
          $('.slider', this.$el).slick('slickGoTo', index);
        }
      },
      mounted: function mounted() {

        var vm = this;

        $('.slider', vm.$el).slick({
          autoplay: true
          // fade: true,  
        }).on('afterChange', function (ev, slick, currentSlide) {
          Vue.nextTick(function () {
            vm.activeIndex = currentSlide;
          });
        });
        ;
      }
    });
  }

  /* ------------------------------------ 
    Horizontal Gallery
  ------------------------------------ */
  function horizontal() {

    if (!document.querySelector('.horizontal-gallery')) {
      return;
    }

    var $slider = $('.horizontal-gallery[data-direction="ltr"]');

    $slider.slick({
      slidesToShow: 1,
      arrows: false,
      dots: true,
      infinite: false,
      mobileFirst: true,
      responsive: [{
        breakpoint: 768,
        settings: {
          dots: false
        }
      }]
    });
  }

  /* ------------------------------------ 
    Horizontal Gallery Inverted
  ------------------------------------ */
  function horizontalInverted() {

    if (!document.querySelector('.horizontal-gallery')) {
      return;
    }

    var $slider = $('.horizontal-gallery[data-direction="rtl"]');

    $slider.slick({
      slidesToShow: 1,
      arrows: false,
      dots: true,
      infinite: false,
      mobileFirst: true,
      responsive: [{
        breakpoint: 992,
        settings: {
          dots: false,
          rtl: true,
          variableWidth: true
        }
      }]
    });

    // Aggiunta attributo dir per navigazione inversa
    function setDirection() {
      window.innerWidth > 991 ? $slider.attr('dir', 'rtl') : $slider.attr('dir', 'ltr');
    }

    var resizeHandler = _.debounce(function () {
      setDirection();
    }, 200);

    window.addEventListener('resize', resizeHandler);
    setDirection();
  }

  /* ------------------------------------ 
    Centered Gallery
  ------------------------------------ */
  function centered() {

    if (!document.querySelector('.centered-gallery')) {
      return;
    }

    var $slider = $('.centered-gallery');

    $slider.slick({
      slidesToShow: 1,
      arrows: false,
      dots: false,
      infinite: true,
      mobileFirst: true,
      responsive: [{
        breakpoint: 992,
        settings: {
          centerMode: true,
          variableWidth: true
        }
      }]
    });
  }

  // INIT
  $(function () {

    vertical();
    horizontal();
    horizontalInverted();
    centered();
  });
})();
//# sourceMappingURL=galleries.js.map
