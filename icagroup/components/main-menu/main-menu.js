'use strict';

{

  $(function () {

    var $menu = $('.main-menu'),
        $menuWrapper = $('.main-menu .wrapper'),
        $primary = $menuWrapper.find('.primary'),
        $secondary = $menuWrapper.find('.secondary'),
        mobileBreakpoint = 1200,
        $mobileToggler = $(".mobile-toggler", $menuWrapper);

    $('> li:has(.megamenu)', $primary).addClass('menu-dropdown-icon');

    $('> ul > li > ul:not(:has(ul))', $menuWrapper).addClass('normal-sub');

    $("> li", $primary)

    // Desktop  
    .on('mouseenter', function (ev) {
      var self = this,
          elemsToAnimate = '.link-group';
      ev.preventDefault();
      if ($(window).width() >= mobileBreakpoint) {
        $(elemsToAnimate, this).removeAttr('style');
        $('.megamenu', this).stop(true).slideDown(150, function () {

          setTimeout(function () {
            anime({
              targets: self.querySelectorAll(elemsToAnimate),
              opacity: [0, 1],
              translateX: [-20, 0],
              easing: 'easeOutQuad',
              delay: function delay(el, i, l) {
                return i * 20;
              }
            });
          }, 100);
        });
      }
    }).on('mouseleave', function (ev) {
      var self = this,
          elemsToAnimate = '.link-group';
      ev.preventDefault();
      if ($(window).width() >= mobileBreakpoint) {
        $(elemsToAnimate, this).removeAttr('style');
        $('.megamenu', this).stop(true).slideUp(150);
      }
    });

    // Click on $("> li", $primary)
    $("> li a", $primary).click(function (ev) {
      var self = this,
          liElem = this.parentElement,
          $thisMegamenu = $('.megamenu', liElem),
          elemsToAnimate = '.link-group';

      // Mobile
      if ($(window).width() < mobileBreakpoint) {
        $('.megamenu').not($thisMegamenu).stop().slideUp();

        // Apertura menu
        if (!$(this).is('.open')) {
          $thisMegamenu.stop().slideDown(200, function () {
            anime({
              targets: liElem.querySelectorAll(elemsToAnimate),
              opacity: [0, 1],
              translateX: [-20, 0],
              easing: 'easeOutQuad',
              delay: function delay(el, i, l) {
                return i * 80;
              }
            });
          });
        }

        // Chiusura menu
        else {
            $thisMegamenu.stop().slideUp(200, function () {
              $(elemsToAnimate, liElem).removeAttr('style');
            });
          }

        liElem.classList.toggle('open');
      }

      // Desktop
      else {
          if ($thisMegamenu.length > 0) {
            return false;
          }
        }
    });

    $mobileToggler.click(function (e) {
      e.preventDefault();
      $menu.toggleClass('open');
      $mobileToggler.find('img').toggle();
      $primary.add($secondary).stop().slideToggle();
    });

    var resizeHandler = _.debounce(function () {
      if (window.innerWidth > mobileBreakpoint) {
        $('.main-menu .primary').removeAttr('style');
      }
    }, 200);

    $(window).resize(resizeHandler).resize();

    var scrollHandler = _.debounce(function () {
      if (window.innerWidth > mobileBreakpoint) {
        window.scrollY > 0 ? $menu.addClass('scrolled') : $menu.removeClass('scrolled');
      }
    }, 200);

    // $(window).scroll( scrollHandler ).scroll();
  });
}
//# sourceMappingURL=main-menu.js.map
