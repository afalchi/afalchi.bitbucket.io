'use strict';

(function () {

  /* -----------------------------------------
  Spalma il contenuto di una cella sulle successive
  se queste hanno lo stesso testo
  ------------------------------------------*/
  function cellsColspan() {
    var tabComp = $('.tab-comparazione'),
        table = $('.feats', tabComp),
        rows = Array.from(document.querySelectorAll('.tab-comparazione .feats tr'));

    rows.forEach(function (row, trIndex) {
      var cells = Array.from(row.querySelectorAll('td'));

      cells.forEach(function (item) {
        return item.setAttribute('colspan', 1);
      });
      cells.reverse().forEach(function (item, index) {
        var prev = item.previousElementSibling;
        if (prev) {
          var currentColspan = item.colSpan,
              prevColspan = prev.colSpan;
          if (item.innerHTML === prev.innerHTML) {
            prev.setAttribute('colspan', prevColspan + currentColspan);
            item.style.display = 'none';
          }
        }
      });
    });
  }

  /* -----------------------------------------
  Mostra tutte
  ------------------------------------------*/
  function showAll() {
    $('.tab-comparazione .toggler').click(function () {
      $('.tab-comparazione .hidden-feats').toggle();
      $('.tab-comparazione .toggler').toggle();
    });
  }

  $(function () {

    cellsColspan();
    showAll();
  });
})();
//# sourceMappingURL=tab-comparazione.js.map
