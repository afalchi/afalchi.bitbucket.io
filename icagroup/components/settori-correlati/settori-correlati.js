'use strict';

(function () {

  function revealFX() {
    scrollReveal.reveal('.settori-correlati [data-reveal]', {
      distance: '20%',
      duration: 1000,
      interval: 50,
      origin: 'bottom',
      //reset: true,
      scale: 1,
      viewFactor: 0.7
    });
  }

  $(function () {

    var resizeHandler = _.debounce(function () {
      if (window.innerWidth < 576) {
        $('.settori-correlati .settori img').each(function (i, e) {
          e.width = e.dataset.width / 2;
          e.height = e.dataset.height / 2;
        });
        revealFX();
      } else {
        $('.settori-correlati .settori img').each(function (i, e) {
          e.width = e.dataset.width;
          e.height = e.dataset.height;
        });
        revealFX();
      }
    }, 500);

    $(window).resize(resizeHandler).resize();
  });
})();
//# sourceMappingURL=settori-correlati.js.map
