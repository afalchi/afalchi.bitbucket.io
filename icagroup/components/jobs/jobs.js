'use strict';

$(function () {

  /* -------------------------------------
    Reveal
  ---------------------------------------*/
  scrollReveal.reveal('.jobs__intro [data-reveal]', {
    distance: '20px',
    duration: 1000,
    interval: 50,
    origin: 'right',
    scale: 1,
    viewFactor: 0.2
  });
});
//# sourceMappingURL=jobs.js.map
