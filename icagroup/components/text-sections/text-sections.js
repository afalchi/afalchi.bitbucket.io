'use strict';

{

  $(function () {

    $('.text-section, .full-img-bg').each(function (i, e) {
      e.dataset.revealGroup = i;

      scrollReveal.reveal('[data-reveal-group="' + i + '"] [data-reveal]', {
        // container: `[data-reveal-group="3"]`, 
        distance: '20px',
        duration: 1000,
        origin: 'right',
        reset: true,
        scale: 1,
        viewFactor: 0.7,
        beforeReveal: function beforeReveal(el) {}
      }, 50);
    });
  });
}
//# sourceMappingURL=text-sections.js.map
