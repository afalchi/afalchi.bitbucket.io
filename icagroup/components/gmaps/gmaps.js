"use strict";

(function () {

  var icaGroup = {
    lat: 43.287406,
    lng: 13.6865864
  };

  var styles = [{ "featureType": "all", "stylers": [{ "saturation": 0 }, { "hue": "#e7ecf0" }] }, { "featureType": "road", "stylers": [{ "saturation": -70 }] }, { "featureType": "transit", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "stylers": [{ "visibility": "simplified" }, { "saturation": -60 }] }];

  var map = new google.maps.Map(document.getElementById('gmap'), {
    center: icaGroup,
    zoom: 8,
    styles: styles
  });

  var marker = new google.maps.Marker({
    icon: 'components/gmaps/img/pin.png',
    map: map,
    position: icaGroup
  });
})();
//# sourceMappingURL=gmaps.js.map
