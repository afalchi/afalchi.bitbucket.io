'use strict';

{

  $(function () {

    setTimeout(function () {
      scrollReveal.reveal('.vert-img-and-text [data-reveal]', {
        distance: '20px',
        duration: 1000,
        interval: 50,
        origin: 'right',
        scale: 1,
        viewFactor: 0.2
      });
    }, 200);
  });
}
//# sourceMappingURL=vert-img-and-text.js.map
