'use strict';

{

  $(function () {

    scrollReveal.reveal('.vernici-faidate [data-reveal]', {
      distance: '20%',
      duration: 1000,
      reset: true,
      scale: 1,
      viewFactor: 0.7
    }, 50);
  });
}
//# sourceMappingURL=banner.js.map
