'use strict';

{

  $(function () {

    setTimeout(function () {
      scrollReveal.reveal('.vernici-faidate [data-reveal]', {
        distance: '20%',
        duration: 1000,
        interval: 50,
        scale: 1,
        viewFactor: 0.7
      });
    }, 200);
  });
}
//# sourceMappingURL=vernici-faidate.js.map
