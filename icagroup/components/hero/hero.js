'use strict';

{

  /* -------------------------------------
    Slide
  --------------------------------------*/
  Vue.component('hero-slide', {
    data: function data() {
      return {
        isPlaying: false
      };
    },

    props: ['currentSlide'],
    computed: {
      slideIndex: function slideIndex() {
        return this.currentSlide;
      }
    },
    watch: {
      slideIndex: function slideIndex() {
        this.stop();
      }
    },
    methods: {
      playPause: function playPause($event) {
        var video = $('video', this.$el)[0];
        $event.preventDefault();
        video.paused ? video.play() : video.pause();
        this.isPlaying = !this.isPlaying;
        this.$emit('playing', !video.paused);
      },
      stop: function stop() {
        var video = $('video', this.$el)[0];
        this.isPlaying = false;
        video.load();
        this.$emit('playing', false);
      }
    }
  });

  /* -------------------------------------
    Main app
  --------------------------------------*/
  new Vue({
    el: '.hero',
    data: {
      currentSlide: 0,
      videoPlaying: false
    },
    computed: {
      infonav: function infonav() {
        return $('.infonav-slider', this.$el);
      },
      slides: function slides() {
        return $('.hero-slide', this.$el).length;
      }
    },
    methods: {
      checkPlaying: function checkPlaying(ev) {
        this.videoPlaying = ev;
      },
      gotoSlide: function gotoSlide(index) {
        this.activeIndex = index;
        $('.slider', this.$el).slick('slickGoTo', index);
      }
    },
    mounted: function mounted() {
      var vm = this;

      // Main slider
      $('.slider', vm.$el).on('init', function (ev, slick) {

        // Nasconde punti se c'è una sola slide (bug di slick)
        if (slick.slideCount === 1) {
          $(this).css('margin-bottom', 0).find('.slick-dots').remove();
        }
      }).slick({
        dots: false,
        arrows: false,
        asNavFor: function () {
          if (vm.infonav) {
            return '.infonav-slider';
          }
        }(),
        autoplay: true,
        pauseOnHover: true,
        autoplaySpeed: 5000,
        speed: 1200,
        fade: true
      }).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        vm.currentSlide = nextSlide;
      });

      // Slider testo + nav
      if (vm.infonav) {
        $('.infonav-slider', vm.$el).slick({
          arrows: false,
          dots: false,
          slidesToShow: 1
        });
      }

      var resizeHandler = _.throttle(function () {
        // window.innerWidth < 1200
      }, 200);
    }
  });
}
//# sourceMappingURL=hero.js.map
