'use strict';

$(function () {

  /* -------------------------------------
    Filiali 
  ---------------------------------------*/
  $('.filiali-lanci .slider').slick({
    arrows: true,
    mobileFirst: true,
    responsive: [{
      breakpoint: 576,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 992,
      settings: {
        slidesToShow: 4
      }
    }, {
      breakpoint: 1400,
      settings: {
        slidesToShow: 5
      }
    }]
  });
});
//# sourceMappingURL=filiali-lanci.js.map
