'use strict';

{

  $(function () {

    var $slider = $('.double-gallery .slider'),
        twoRowsBreakpoint = 1400;

    function slickMobile() {
      $slider.slick({
        appendDots: '.double-gallery__dots',
        dots: true,
        dotsClass: 'slick-dots slick-dots--inverted',
        mobileFirst: true,
        variableWidth: true,
        infinite: false,
        responsive: [{
          breakpoint: twoRowsBreakpoint - 1,
          settings: 'unslick'
        }]
      });
    }

    function slickDesktop() {
      $slider.slick({
        appendDots: '.double-gallery__dots',
        dots: true,
        dotsClass: 'slick-dots slick-dots--inverted',
        rows: 2,
        variableWidth: true,
        infinite: false,
        responsive: [{
          breakpoint: twoRowsBreakpoint,
          settings: 'unslick'
        }]
      });
    }

    // Nav
    $('.double-gallery .slick-prev').click(function (ev) {
      ev.preventDefault();
      $slider.slick('slickPrev');
    });
    $('.double-gallery .slick-next').click(function (ev) {
      ev.preventDefault();
      $slider.slick('slickNext');
    });

    // Responsive hack
    // Necessario a causa di problemi con l'opzione "responsive"
    // di Slick
    var resizeHandler = _.debounce(function () {
      if (window.innerWidth > twoRowsBreakpoint) {
        !$slider.is('.slick-slider') ? slickDesktop() : null;
      } else {
        !$slider.is('.slick-slider') ? slickMobile() : null;
      }
    }, 200);

    window.addEventListener('resize', resizeHandler);
    resizeHandler();
  });
}
//# sourceMappingURL=double-gallery.js.map
