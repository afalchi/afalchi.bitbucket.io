'use strict';

{

  new Vue({
    el: '.tab-horizontal',
    data: {
      selected: 0,
      slides: [{
        id: 1,
        label: 'label 1',
        copy: ''
      }]
    },
    methods: {
      showSlide: function showSlide(index) {
        var vm = this,
            currentSlide = vm.$el.querySelector('.main[data-index="' + vm.selected + '"]'),
            nextSlide = vm.$el.querySelector('.main[data-index="' + index + '"]');

        var timeline = anime.timeline({
          duration: 300
        });

        timeline.add({
          targets: currentSlide.querySelectorAll('[data-anime]'),
          translateX: -20,
          opacity: 0,
          easing: 'easeInQuad',
          delay: function delay(el, i, l) {
            return i * 200;
          },
          complete: function complete() {
            Vue.nextTick(function () {
              vm.selected = index;
            });
          }
        }).add({
          targets: nextSlide.querySelectorAll('[data-anime]'),
          translateX: [20, 0],
          opacity: [0, 1],
          easing: 'easeOutQuad',
          delay: function delay(el, i, l) {
            return i * 200;
          }
        });
      }
    }
  });
}
//# sourceMappingURL=tab-horizontal.js.map
