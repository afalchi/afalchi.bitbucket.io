'use strict';

{

  $(function () {

    // const

    scrollReveal.reveal('.main-prefooter .palette', {
      distance: '100px',
      duration: 1000,
      origin: 'right',
      scale: 1,
      viewFactor: 0.7
    }, 50);
  });
}
//# sourceMappingURL=main-footer.js.map
