'use strict';

(function () {

  /* ------------------------------------ 
    Test slider
  ------------------------------------ */
  function testSlider() {
    $('.test-qa .slider').slick({
      autoplay: true,
      autplayDelay: 4000,
      speed: 2000,
      mobileFirst: true,
      responsive: [{
        breakpoint: 576,
        settings: {
          centerMode: true,
          variableWidth: true
        }
      }]
    });
  }

  /* ------------------------------------ 
    Test lanci
  ------------------------------------ */
  function testLanci() {
    $('.test-lanci__slider').slick({
      speed: 2000,
      mobileFirst: true,
      slidesToShow: 1,
      responsive: [{
        breakpoint: 576,
        settings: {
          slidesToShow: 2
        }
      }, {
        breakpoint: 768,
        settings: 'unslick'
      }]
    });
  }

  $(function () {

    testSlider();
    testLanci();
  });
})();
//# sourceMappingURL=life-performance.js.map
