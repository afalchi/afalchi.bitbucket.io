'use strict';

$(function () {

  /* -------------------------------------
    Reveal
  ---------------------------------------*/
  scrollReveal.reveal('.azienda-intro [data-reveal]', {
    distance: '20px',
    duration: 1000,
    interval: 50,
    origin: 'left',
    scale: 1,
    viewFactor: 0.2
  });
});
//# sourceMappingURL=azienda.js.map
