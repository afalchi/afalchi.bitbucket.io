'use strict';

(function () {

  new Vue({
    el: '.filters',
    data: {
      activeFilter: ''
    },
    methods: {
      setFilter: function setFilter(key) {
        this.activeFilter = key;
      },
      resetFilters: function resetFilters() {
        this.activeFilter = '';
      }
    }
  });
})();
//# sourceMappingURL=rete-vendita.js.map
