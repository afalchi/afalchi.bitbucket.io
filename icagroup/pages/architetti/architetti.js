'use strict';

$(function () {

  /* -------------------------------------
    Reveal
  ---------------------------------------*/
  scrollReveal.reveal('.architetti__intro [data-reveal]', {
    distance: '20px',
    duration: 1000,
    interval: 50,
    origin: 'right',
    scale: 1,
    viewFactor: 0.2
  });

  scrollReveal.reveal('.architetti__list [data-reveal]', {
    distance: '20px',
    duration: 1000,
    interval: 50,
    origin: 'right',
    scale: 1,
    viewFactor: 0.2
  });
});
//# sourceMappingURL=architetti.js.map
