'use strict';

$(function () {

  /* -------------------------------------
    Reveal
  ---------------------------------------*/
  scrollReveal.reveal('.storia-intro [data-reveal]', {
    distance: '20px',
    duration: 1000,
    interval: 50,
    origin: 'left',
    scale: 1,
    viewFactor: 0.2
  });

  scrollReveal.reveal('.timeline [data-reveal-left]', {
    distance: '20px',
    duration: 1000,
    interval: 50,
    origin: 'left',
    scale: 1,
    viewFactor: 0.2
  });

  scrollReveal.reveal('.timeline [data-reveal-right]', {
    distance: '20px',
    duration: 1000,
    interval: 50,
    origin: 'right',
    scale: 1,
    viewFactor: 0.2
  });

  /* -------------------------------------
    Filiali 
  ---------------------------------------*/
  $('.filiali-lanci .slider').slick({
    arrows: true,
    mobileFirst: true,
    responsive: [{
      breakpoint: 576,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 992,
      settings: {
        slidesToShow: 4
      }
    }, {
      breakpoint: 1400,
      settings: {
        slidesToShow: 5
      }
    }]
  });
});
//# sourceMappingURL=storia.js.map
