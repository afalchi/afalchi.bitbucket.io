'use strict';

(function () {

  $(function () {

    // Architetti
    scrollReveal.reveal('.architetti [data-reveal]', {
      distance: '20px',
      duration: 1000,
      origin: 'bottom',
      scale: 1,
      viewFactor: 0.7
    }, 50);

    // Innovation camp
    scrollReveal.reveal('.innovation-camp [data-reveal]', {
      distance: '20px',
      duration: 1000,
      origin: 'right',
      scale: 1,
      viewFactor: 0.7
    }, 50);
  });
})();
//# sourceMappingURL=home.js.map
