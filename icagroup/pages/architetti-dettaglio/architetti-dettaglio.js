'use strict';

(function () {

  $(function () {

    // Intro
    scrollReveal.reveal('.architetti-dettaglio__intro [data-reveal]', {
      distance: '20px',
      duration: 1000,
      origin: 'right',
      scale: 1,
      viewFactor: 0.7
    }, 50);

    // Gallery
    scrollReveal.reveal('.architetti-dettaglio__gallery [data-reveal]', {
      distance: '20px',
      duration: 1000,
      origin: 'right',
      scale: 1,
      viewFactor: 0.7
    }, 50);
  });
})();
//# sourceMappingURL=architetti-dettaglio.js.map
