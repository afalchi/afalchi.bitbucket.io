'use strict';

(function () {

  /* --------------------------------------
    Scroll Reveal
  ---------------------------------------*/
  window.scrollReveal = ScrollReveal();

  /* --------------------------------------
    Blur shadow effect
  ---------------------------------------*/
  jQuery.fn.extend({
    blurShadow: function blurShadow(settings) {

      return this.each(function (i, e) {
        var blur = this.dataset.shadow || settings || 16;

        // Backup per vecchi browser
        if (!Modernizr.cssfilters) {
          $(this).wrap('<div class="blur-shadow__wrapper"></div>');
          return;
        }

        // Top shadow
        if ($(this).hasClass('blur-shadow--invert')) {
          $(this).wrap('<div class="blur-shadow__wrapper" style="padding-top: ' + blur * .4 + '%"></div>').clone().css({
            '-webkit-filter': 'blur(' + blur + 'px)',
            'filter': 'blur(' + blur + 'px)',
            'bottom': blur * .3 + '%'
          }).appendTo(this.parentNode);
        }

        // Bottom shadow
        else {
            $(this).wrap('<div class="blur-shadow__wrapper" style="padding-bottom: ' + blur * .4 + '%"></div>').clone().css({
              '-webkit-filter': 'blur(' + blur + 'px)',
              'filter': 'blur(' + blur + 'px)',
              'top': blur * .3 + '%'
            }).appendTo(this.parentNode);
          }
      });
    }
  });

  /* --------------------------------------
    ScrollReveal global init
  ---------------------------------------*/
  function scrollRevealGlobal() {
    scrollReveal.reveal('.storia-intro [data-reveal]', {
      distance: '20px',
      duration: 1000,
      interval: 50,
      origin: 'left',
      scale: 1,
      viewFactor: 0.2
    });
  }

  /* --------------------------------------
    Slided BG
  ---------------------------------------*/
  function slidedBG() {
    $('.slided-bg').each(function (i, e) {
      var color = e.dataset.color;
      $(e).prepend('<div class="slided-bg-panel" ' + (color ? 'style="background-color:' + color + '"' : null) + '></div>');
    });
  }

  // INIT
  $(function () {

    $('.blur-shadow').blurShadow(22);
    slidedBG();
    scrollRevealGlobal();
  });
})();
//# sourceMappingURL=main.js.map
