angular.module('app')


/* -------------------------------------
  Days ago
--------------------------------------*/
.filter('daysAgo', function(msPer) {
  return function(recipeLastDate, selectingDate) {
    const last = recipeLastDate || 0,
          selecting = selectingDate || 0
    ;
    return Math.floor( (selectingDate - recipeLastDate) / msPer.day ) || 0;
  }
})


/* -------------------------------------
  Most recent day
--------------------------------------*/
.filter('mostRecent', function(today) {
  return function(datesObj) {
    if (!datesObj) {
      return today;
    } else {
      const dates = Object.keys(datesObj).map( item => parseInt(item));
      return Math.max.apply(null, dates);
    }
  }
})


/* -------------------------------------
  Urlize text
--------------------------------------*/
.filter('urlize', function() {
  return function(text) {
    return text.toLowerCase().replace(/\s/ig, '-');
  }
})



/* -------------------------------------
  Translate with Yandex
  https://glebbahmutov.com/blog/async-angular-filter/
--------------------------------------*/
.filter('yandex', function($http) {
  let cached = {};
  const yandexUrl = 'https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20180507T212523Z.4acc23eba31a0cf4.2b3592f095b464c7439cff49286137e1f193f4fc&lang=it&text=';

  function yandexFilter(input) {
    let text = encodeURIComponent(input);
    if(text) {  
      
      if(text in cached) {
        return typeof cached[text].then !== 'function' ?
            cached[text] : undefined
        ;
      } 
      
      else {
        cached[text] = $http({
          method: 'GET',      
          url: yandexUrl + text
        })
        .then(
          response => {
            cached[text] = response.data.text[0];
          },
          error => {
            console.error('Error with Yandex filter');
            cached[text] = input;
          }
        );
      }   

    }
  }

  yandexFilter.$stateful = true;
  return yandexFilter;
})


