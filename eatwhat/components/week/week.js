'use strict';

angular.module('app').component('weekTable', {
  templateUrl: 'components/week.html',
  bindings: {
    week: '='
  },
  controller: function controller($log, $scope, state, recipesFB, today, lastMonday, msPer) {
    var vm = this;
    vm.state = state;
    vm.today = today;
    vm.week = [];

    // Get data from Firebase
    recipesFB.arr.$loaded(function (data) {
      vm.recipesArr = data;
      updateWeek();
    }, function (err) {
      return $log.log('Error fetching data!');
    });

    vm.removeSelection = function (day, meal) {
      // $log.log(day, meal)
      recipesFB.ref.child(day[meal].$id + '/dates/' + day.date + '/' + meal).set(null);
      day[meal] = null;
    };

    vm.setSelection = function (date, meal) {
      state.selecting = {
        active: true,
        date: date,
        meal: meal,
        selectedRecipe: undefined
      };
    };

    function getMeal(date, meal) {
      return vm.recipesArr.find(function (item) {
        if (item.dates) {
          if (date in item.dates) {
            if (item.dates[date][meal]) {
              return item;
            }
          }
        }
      });
    }

    function updateWeek() {
      vm.week = [];
      for (var i = 0; i < 7; i++) {
        var date = lastMonday + msPer.day * i;
        vm.week.push({
          date: date,
          lunch: getMeal(date, 'lunch'),
          dinner: getMeal(date, 'dinner')
        });
      }
    }

    // $scope.$watchCollection('$ctrl.recipesArr', function(curr, prev) {
    //   $log.log('Update week')
    //   updateWeek();
    // });
  },

  transclude: true
})

/* -------------------------------
  Remove meal button
--------------------------------*/
.component('btnRemove', {
  template: '\n    <button \n      data-foo="daga"\n      href="#!/ricette"      \n      ng-click="$ctrl.onSelect()"\n    >\n      <span class="oi oi-x"></span>\n    </button>\n  ',
  bindings: {
    day: '<',
    onSelect: '&'
  },
  controller: function controller(today) {
    this.today = today;
  }
});
//# sourceMappingURL=week.js.map
