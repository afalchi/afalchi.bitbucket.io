'use strict';

angular.module('app').controller("CalendarController", function ($log, $scope, $routeParams, recipesFB, today, msPer, lastMonday, state) {
  var vm = this;

  vm.lastMonday = lastMonday;
  vm.state = state;
  vm.today = today;
  vm.week = [];
  vm.yesterday = today - msPer.day;

  vm.startSelection = function (meal) {
    vm.state.selecting.active = true;
    vm.state.selecting.meal = meal;
  };

  // Get data from Firebase
  recipesFB.arr.$loaded(function (data) {
    $log.log('data loaded');
    vm.recipesArr = data;
    updateWeek();
  }, function (err) {
    return $log.log('Error fetching data!');
  });

  vm.removeSelection = function (day, meal) {
    // $log.log(day, meal)
    recipesFB.ref.child(day[meal].$id + '/dates/' + day.date + '/' + meal).set(null);
    day[meal] = null;
  };

  vm.setSelection = function (date, meal) {
    state.selecting = {
      active: true,
      date: date,
      meal: meal,
      selectedRecipe: undefined
    };
  };

  function getMeal(date, meal) {
    return vm.recipesArr.find(function (item) {
      if (item.dates) {
        if (date in item.dates) {
          if (item.dates[date][meal]) {
            return item;
          }
        }
      }
    });
  }

  function updateWeek() {
    vm.week = [];
    for (var i = 0; i < 7; i++) {
      var date = lastMonday + msPer.day * i;
      vm.week.push({
        date: date,
        lunch: getMeal(date, 'lunch'),
        dinner: getMeal(date, 'dinner')
      });
    }
  }
});
//# sourceMappingURL=calendar.1.js.map
