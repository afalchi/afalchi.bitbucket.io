'use strict';

angular.module('app').controller("SearchController", function ($log, $http, edamam, state) {
  var vm = this;

  vm.query = 'pork';
  vm.loadingData = false;
  vm.state = state;

  vm.search = function (ev) {
    ev.preventDefault();

    vm.loadingData = true;

    $http({
      method: 'GET',
      url: edamam.base + '/search?app_id=' + edamam.appId + '&app_key=' + edamam.appKey + '&q=' + vm.query + '&to=20'
    }).then(function (response) {
      // $log.log(response.data)
      vm.response = response.data;
      vm.loadingData = false;
    }, function (error) {
      return alert(error);
    });
  };
});
//# sourceMappingURL=search.js.map
