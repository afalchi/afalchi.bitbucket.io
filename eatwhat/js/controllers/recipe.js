"use strict";

angular.module('app').controller("RecipeController", function ($log, recipesFB, state) {
  var vm = this;

  vm.recipe = state.recipeDetail;

  $log.log(vm.recipe);
});
//# sourceMappingURL=recipe.js.map
