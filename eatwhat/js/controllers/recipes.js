'use strict';

angular.module('app').controller("RecipesController", function ($filter, $log, $rootScope, $scope, currentAuth, recipesFB, state, today) {
  var vm = this;

  vm.mappedRecipesArray = [];
  vm.state = state;
  vm.sortOrder = 'daysAgo';
  vm.showAddRecipe = false;
  vm.recipesArr = [];
  vm.viewReady = false;
  vm.week = $rootScope.week;

  // Update user state
  state.user = {
    displayName: currentAuth.displayName,
    uid: currentAuth.uid
  };

  function mapRecipesArray() {
    vm.mappedRecipesArray = vm.recipesArr.map(function (item) {
      var date = vm.state.selecting.date || today;
      item.daysAgo = $filter('daysAgo')(date, $filter('mostRecent')(item.dates));
      return item;
    });
  }

  recipesFB.arr(state.user.uid).$loaded().then(function (data) {
    vm.recipesArr = data;
    mapRecipesArray();
    vm.viewReady = true;
  }, function (error) {
    return alert('Error loading from Firebase!');
  });

  // Update week on array update
  recipesFB.arr(state.user.uid).$watch(function (ev) {
    mapRecipesArray();
  });

  vm.addRecipe = function (ev) {
    ev.preventDefault();
    vm.recipesArr.$add({
      id: vm.newRecipe.toLowerCase().replace(/\s/g, '-'),
      name: vm.newRecipe,
      dates: []
    });
    vm.newRecipe = '';
    vm.showAddRecipe = false;
  };

  vm.confirmRecipe = function () {
    vm.state.selecting.selectedRecipe = vm.selectedRecipe;
    vm.state.selecting.active = false;
    recipesFB.ref(state.user.uid).child(vm.selectedRecipe + '/dates/' + state.selecting.date + '/' + state.selecting.meal).set(true);
  };

  vm.goBack = function () {
    vm.state.selecting.active = false;
    window.scrollTo(0, 0);
  };
});
//# sourceMappingURL=recipes.js.map
