"use strict";

angular.module('app').controller("LoginController", function ($log, $location, $firebaseAuth, $timeout) {
  var vm = this;
  vm.auth = $firebaseAuth();
  vm.user = null;
  vm.logged = false;

  vm.createUser = function () {
    vm.auth.$signInWithPopup("google").then(function (firebaseUser) {
      console.log("Authentication successful.");
      vm.user = firebaseUser.user;
      vm.logged = true;
      $timeout(function () {
        $location.url('/calendar');
      }, 2000);
    }).catch(function (error) {
      alert("Authentication failed:", error);
    });
  };

  vm.auth.$onAuthStateChanged(function (firebaseUser) {
    vm.firebaseUser = firebaseUser;
  });

  vm.deleteUser = function () {
    vm.auth.$deleteUser().then(function () {
      vm.message = "User deleted";
    }).catch(function (error) {
      console.error('Error deleting user');
    });
  };
});
//# sourceMappingURL=login.1.js.map
