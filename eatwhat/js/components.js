'use strict';

angular.module('app')

/* -------------------------------
  Icons
--------------------------------*/
.component('icoCheck', {
  templateUrl: 'components/ico-check.html',
  bindings: {
    selected: '<'
  }
}).component('icoArrowLeft', {
  templateUrl: 'components/ico-arrow-left.html',
  bindings: {
    color: '@'
  }
}).component('icoPlus', {
  templateUrl: 'components/ico-plus.html'
})

/* -------------------------------
State message
--------------------------------*/
.component('stateMsg', {
  templateUrl: 'components/state-msg.html',
  controller: function controller(state) {
    this.state = state;
  }
})

/* -------------------------------
  Remove meal
--------------------------------*/
.component('btnRemove', {
  template: '\n    <button \n      href="#!/ricette"      \n      ng-click="$ctrl.onSelect()"\n    >\n      <span class="oi oi-x"></span>\n    </button>\n  ',
  bindings: {
    day: '<',
    onSelect: '&'
  },
  controller: function controller(today) {
    this.today = today;
  }
}).component('toggleSelection', {
  template: '\n  <button \n    ng-click="$ctrl.selectedRecipe = $ctrl.recipeId" \n    class="recipes__select" \n    title="{{$ctrl.selectedRecipe}} {{$ctrl.recipeId}}"\n    ng-class="{\'recipes__select--selected\': $ctrl.selectedRecipe === $ctrl.recipeId}"\n  >\n    <ico-check selected="$ctrl.selectedRecipe === $ctrl.recipeId"></ico-check>\n  </button>\n  ',
  bindings: {
    selectedRecipe: '=',
    recipeId: '<'
  },
  controller: function controller() {
    var vm = this;
    vm.log = function () {
      vm.msg = '';
    };
  }
});
//# sourceMappingURL=components.js.map
