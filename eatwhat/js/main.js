'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://console.firebase.google.com/project/project-5611184154132311931/database/m1sc/data/eatwhat

console.clear();

var app = angular.module("app", ['ngRoute', 'firebase']);

app.config(function ($routeProvider, $locationProvider) {
  $routeProvider.when('/calendario', {
    templateUrl: 'partials/calendar.html',
    controller: 'CalendarController',
    controllerAs: 'ctrl'
  }).when('/ricette', {
    templateUrl: 'partials/ricette.html',
    controller: 'RecipesController',
    controllerAs: 'ctrl'
  }).when('/ricette/:recipe', {
    templateUrl: 'partials/ricetta.html',
    controller: 'RecipeController',
    controllerAs: 'ctrl'
  }).otherwise({ redirectTo: '/calendar' });

  $locationProvider.html5Mode({
    enabled: false,
    requireBase: false,
    rewriteLinks: true
  });
});

/* -----------------------------------
  Controllers
------------------------------------*/

// Calendario
app.controller("CalendarController", function ($log, $scope, $routeParams, recipes, todayFactory, msPerDay, state) {
  var ctrl = this;

  ctrl.state = state;
  ctrl.recipes = recipes;
  ctrl.recipe = $routeParams.ricetta;
  ctrl.week = [];
  ctrl.today = todayFactory;
  ctrl.yesterday = todayFactory - msPerDay;
  ctrl.startSelection = function (meal) {
    ctrl.state.type = 'selection';
    ctrl.state.selectedMeal = meal;
  };

  var today = new Date(2018, 3, 28).getTime();

  var _loop = function _loop(i) {
    var date = today + msPerDay * i;
    ctrl.week.push({
      date: date,
      lunch: function () {
        return ctrl.recipes.filter(function (item) {
          return item.date === date;
        })[0];
      }()
    });
  };

  for (var i = 0; i < 7; i++) {
    _loop(i);
  }
});

// Ricette
app.controller("RecipesController", function ($log, $scope, recipes, recipesObj, selectedLunch, state, todayFactory) {
  var ctrl = this;

  ctrl.state = state;
  ctrl.recipes = recipes;
  ctrl.sortOrder = 'name';
  ctrl.selectedRecipe = '';
  ctrl.selectedLunch = selectedLunch;
  ctrl.addRecipe = function (ev) {
    ev.preventDefault();
    ctrl.recipes.$add({
      name: ctrl.newRecipe,
      date: new Date().getTime()
    });
    ctrl.newRecipe = '';
  };
  ctrl.confirmRecipe = function () {
    $log.log('Saving...');
    ctrl.state.selectedRecipeId = ctrl.selectedRecipe;
    recipesObj[ctrl.selectedRecipe].date = todayFactory;
    recipesObj.$save();
  };
});

app.controller("RecipeController", function ($log, $scope, recipes, $routeParams) {
  var ctrl = this;

  ctrl.recipes = recipes;
  ctrl.recipe = $routeParams.recipe;
});

/* -----------------------------------
  Filters
------------------------------------*/
app.filter('daysAgo', function (msPerDay) {
  return function (date) {
    return Math.round((Date.now() - date) / msPerDay);
  };
});

/* -----------------------------------
  Factories
------------------------------------*/
app.factory('state', function () {
  return {
    type: 'none',
    selectedRecipeId: '',
    selectedMeal: ''
  };
});

app.factory('recipe', function () {
  return function () {
    function Recipe(name, difficulty) {
      _classCallCheck(this, Recipe);

      this.name = name;
      this.difficulty = difficulty;
      this.lastDate = lastDate;
    }

    _createClass(Recipe, [{
      key: 'getname',
      get: function get() {
        return this.name;
      }
    }]);

    return Recipe;
  }();
});

app.factory('recipes', function ($firebaseArray) {
  var ref = firebase.database().ref().child('eatwhat');
  return $firebaseArray(ref);
});

app.factory('recipesObj', function ($firebaseObject) {
  var ref = firebase.database().ref().child('eatwhat');
  return $firebaseObject(ref);
});

app.factory('selectedLunch', function () {
  return '';
});

app.factory('todayFactory', function () {
  var d = new Date();
  return d.getTime() - d.getMilliseconds() - d.getSeconds() * 1000 - d.getMinutes() * 60000 - d.getHours() * (1000 * 60 * 24);
});

/* -----------------------------------
  Directives
------------------------------------*/
app.directive('ewDirective', function () {
  return {
    template: '<p>Gianni</p>'
  };
});

/* -----------------------------------
  Components
------------------------------------*/
app.component('toggleSelection', {
  template: '\n  <button \n  ng-click="$ctrl.selectedRecipe = $ctrl.recipeId" \n  class="recipes__select" \n  title="{{$ctrl.selectedRecipe}} {{$ctrl.recipeId}}"\n  ng-class="{\'recipes__select--selected\': $ctrl.selectedRecipe === $ctrl.recipeId}"\n  >\n  <span class="oi oi-check"></span>\n  <span class="oi oi-x" ng-show="false"></span>\n  </button>\n  ',
  bindings: {
    selectedRecipe: '=',
    recipeId: '<'
  },
  controller: function ewComponentController() {
    var ctrl = this;
    ctrl.log = function () {
      ctrl.msg = '';
    };
  }
});

app.component('logState', {
  template: '\n    State\n    <pre>\n      {{ $ctrl.state | json}}\n    </pre>\n  ',
  controller: function controller(state) {
    this.state = state;
  }
});

/* -----------------------------------
  Constants
------------------------------------*/
app.constant('msPerDay', 86400000);
//# sourceMappingURL=main.js.map
