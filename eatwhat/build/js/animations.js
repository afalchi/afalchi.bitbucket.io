'use strict';

angular.module('app')

/* -------------------------------
  Icons
--------------------------------*/
.animation('.view-transition', function ($animateCss) {
  return {
    enter: function enter(element, done) {
      window.scrollTo(0, 0);

      element.delay(500).css({
        opacity: 0,
        position: "relative",
        left: "200px"
      }).animate({
        top: 0,
        left: 0,
        opacity: 1
      }, 300, done);
    },
    leave: function leave(element, done) {
      // TODO
      // Ricontrollare
      element.css({
        opacity: 1,
        position: "relative",
        left: "0"
      }).animate({
        left: '-200px',
        opacity: 0
      }, 300, function () {
        window.scrollTo(0, 0);
        return done;
      }());
    }
  };
});
//# sourceMappingURL=animations.js.map
