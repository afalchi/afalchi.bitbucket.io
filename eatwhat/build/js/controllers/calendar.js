'use strict';

angular.module('app').controller("CalendarController", function ($location, $log, $routeParams, $scope, currentAuth, recipesFB, lastMonday, msPer, state, updateWeek, today) {

  // Auth check
  if (!currentAuth && state.loginType !== 'withID') {
    $location.url('/login');
    return;
  }

  var vm = this;

  vm.lastMonday = lastMonday;
  vm.state = state;
  vm.today = today;
  vm.yesterday = today - msPer.day;
  vm.viewReady = false;

  // Scroll back to top
  // window.scrollTo(0,0);

  // Update user state
  // state.user = { 
  //   displayName: currentAuth.displayName,
  //   uid: currentAuth.uid
  // };


  // Get data from Firebase
  recipesFB.arr(state.user.uid).$loaded(function (data) {
    vm.recipesArr = data;
    vm.week = updateWeek(data, state.user.uid);
    vm.viewReady = true;
  }, function (err) {
    return alert('Error fetching data!');
  });

  vm.startSelection = function (meal) {
    vm.state.selecting.active = true;
    vm.state.selecting.meal = meal;
  };

  vm.removeSelection = function (day, meal) {
    recipesFB.ref(state.user.uid).child(day[meal].$id + '/dates/' + day.date + '/' + meal).set(null);
    day[meal] = null;
  };

  vm.setSelection = function (date, meal) {
    state.selecting = {
      active: true,
      date: date,
      meal: meal,
      selectedRecipe: undefined
    };
  };

  vm.editSelection = function (day, meal) {
    vm.removeSelection(day, meal);
    vm.setSelection(day.date, meal);
  };
});
//# sourceMappingURL=calendar.js.map
