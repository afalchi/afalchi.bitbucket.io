$(function(){
	
	// Proposte Viewer
	// author: Fabio Ottaviani
	// creato il: 06/03/2012
	// ultima modifica: 31/05/2012
	
	
	// vars
	var $zoom = $('<div id="zoom">').appendTo('body');
	var i = 0;
	var max = $('.proposte a').size();
	var $thumbs = $('<div id="thumbs"><div class="thumbs"><ul class="proposte"></ul></div></div>').appendTo('body');
	var $title = $('<div id="title">').appendTo('.thumbs');
	var $close = $('<div id="close">').appendTo('.thumbs');
	
	// apertura
	$('.proposte a').live('click',function(){
		if ($(this).parents('#container').size()) {
			i = $('#container .proposte a').index($(this));
		} else {
			i = $('#thumbs .proposte a').index($(this));
		}
		$('#thumbs a.on').removeClass('on');
		$('#thumbs a').eq(i).addClass('on');
		$('html,body').stop().animate({'scrollTop':0},400);
		$zoom.html('<img src="' + $(this).attr('href') + '">').css({'background-image':'url(' + $(this).attr('href') + ')','background-repeat':'no-repeat','background-position':'center top'}).find('img').css({'opacity':.01});
		$('#container').css({'height':'auto'}).css({height:0});
		$title.html('<a>' + $(this).attr('title') + '<span>(' + (i+1) + '/' + max + ')</span></a>');
		if ($zoom.is(':not(:visible)')){
			$zoom.css({'min-height':$(document).height()}).fadeIn(400);
			$close.fadeIn(400);
			$title.fadeIn(400);
		}
		return false;
	});
	
	// close
	$close.click(function(){
		$zoom.html('').css({'background-image':'none'}).fadeOut(400);
		$title.fadeOut(400);
		$(this).fadeOut(400);
		$('#container').css({height:'auto'});
		$('#thumbs .thumbs').stop().css({top:-100, opacity:0},100);
	});
	
	// doubleclick
	$('#zoom').click(function(){
		$close.click();
	});
		
	// arrow nav
	$('body').keydown(function(e){
		// esc
		if(e.which == 27){
			$close.click();
		}
		// next
		if (e.which == 39) {
			if (i < max - 1) {
				i++;
			} else {
				i = 0;
			}
			if ($zoom.is(':visible')){
				$('.proposte a').not(':hidden').eq(i).click();
			}
		}
		// prev
		if (e.which == 37) {
			if (i < 0) {
				i = max - 1;
			} else {
				i--;
			}
			if ($zoom.is(':visible')){
				$('.proposte a').not(':hidden').eq(i).click();
			}
		}
	});
	
	// thumbs
	$('#container .proposte li').not(':hidden').each(function(i) {
        $('#thumbs ul').append($('<li>' + $(this).html() + '</li>'));
		$('#thumbs ul li a').eq(i).append('<span>' + (i+1) + '</span>');
    });
	$('#thumbs').mouseover(function(){
		if ($zoom.is(':visible')){
			$('#thumbs .thumbs').stop().animate({top:0, opacity:1},100);
		}
	}).mouseout(function(){
		$('#thumbs .thumbs').stop().animate({top:-100, opacity:0},100);
	});
	
	
});