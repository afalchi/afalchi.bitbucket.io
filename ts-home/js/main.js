
/*----------------------------------------------------------
 * Random BB
 *--------------------------------------------------------*/
function randomBG() {
  $('html').css(
    'background-image',
    'url(img/ui/bg' + Math.ceil( Math.random() * 4 ) + '.png)'
  );  
}


  
/*----------------------------------------------------------
 * Slider
 *--------------------------------------------------------*/
function slider () {

  $('.hero__slider .slider').slick({
    dots: true,
    arrows: false,
    autoplay: false,
    mobileFirst: true,
    speed: 2000, 
    responsive: [
      {
        breakpoint: 767,
        settings: {
          autoplay: false,
          autoplaySpeed: 4000,
          arrows: true,
        }
      }
    ]
  });
  
  $('.slider-default').slick({
    dots: true,
    arrows: true,
    // autoplay: true,
    autoplaySpeed: 4000,
    speed: 2000,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          arrows: false
        }
      }
    ]
  });
  

  $('.slider-full').slick({
    dots: true,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 1000,
    speed: 2000
  });


  $('.store__slider').slick({
    dots: true,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 1000,
    slidesToShow: 3,
    speed: 2000,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          arrows: false,
          slidesToShow: 2
        }
      },
      {
        breakpoint: 576,
        settings: {
          arrows: false,
          slidesToShow: 1
        }
      }
    ]
  });

}
    

/*----------------------------------------------------------
 *  Main nav
 *--------------------------------------------------------*/ 
function mainNav () {

  // Ancore che aprono un sottomenu su mobile
  var dropdownTogglers = $('.main-nav a').filter( function (i, e) {
    return $(e).next().is('ul') || $(e).next().is('.dropdown-menu'); 
  });

  dropdownTogglers.addClass('dropdown-toggler');

  
  // Menu Desktop
  var menuDesktop = {
    active: false,
    init: function () {  
      var self = this;

      $('.main-nav .nav-link').on('mouseenter', function () {
          if(self.active) {
            $('.main-nav .dropdown-menu .menu')
              .css('height', 'auto')
              .menu( "collapseAll", null, true )             
            ;
          }
        }
      );      

      $( ".dropdown-menu .menu" ).menu({
        classes: {
          "ui-menu": "highlight"
        },
        position: { 
          my: "left top", 
          at: "right top"
        },
        blur: function (event, ui) {
          
        },
        focus: function( event, ui ) {   
           
          var el = ui.item,
            $parent = $(el).parents('.menu'),   
            $child = $(el).find('.ui-menu'),       
            maxH = 0
          ;

          
          // Timeout compensa ritardo del plugin
          //setTimeout(function () {
            if ($child.height()) {
              $child.height() >= $parent.height() ? $parent.height($child.height() + 'px') : $parent.height('auto');
            }
         // }, 200);
          
        }
      });       
    }
  };


  // Menu mobile    
  var menuMobile = {
    init: function () {      

      dropdownTogglers
        .each(function(i,e) {

          $(e).on('click', function(ev) {
            ev.preventDefault();
            $(e)
              .toggleClass('open')
              .next()
              .stop()
              .slideToggle('slow')              
            ;
          });
        })
      ;
    },
    destroy: function () {
      dropdownTogglers
        .each(function(i,e) {
          $(e).off('click');
        })
      ;
    }
  };


  function checkWidth() {
    if(window.matchMedia('(min-width: 992px)').matches) {
      $('.main-nav > .navbar').addClass('fixed-top');
      if(!menuDesktop.active) {
        menuMobile.destroy();
        menuDesktop.init();
        menuDesktop.active = true;
      }
    } else {
      $('.main-nav > .navbar').removeClass('fixed-top');

      if(menuDesktop.active) {
        $( ".dropdown-menu .menu" ).menu('destroy');
        menuDesktop.active = false;
      } 

      menuMobile.init();
    } 
  }

  // Ricerca
  $('.main-nav__search input')
    .on('focus',  function () { this.placeholder = ''; })
    .on('blur',  function () { this.placeholder = this.getAttribute('aria-label'); })
  ;

  dropdownTogglers.click(function(ev){ ev.preventDefault(); });

  var debounced = _.debounce(checkWidth, 500);
  $(window).resize(debounced).resize()

 
  
}


/*----------------------------------------------------------
 *  Back to top
 *--------------------------------------------------------*/ 
function backtotop () {
  var $backtotop = $('.backtotop');

  $backtotop.click(function(){
    $('html, body').animate({scrollTop: 0});
  });

  var checkScroll = _.throttle(function () {
    window.scrollY > window.innerHeight ? $backtotop.addClass('visible') : $backtotop.removeClass('visible');
  }, 1000);

  $(window).scroll(checkScroll);
}



/*----------------------------------------------------------
 *   DOC READY
 *--------------------------------------------------------*/
$(function () {

  randomBG();
  mainNav();
  slider();
  backtotop();

});
    