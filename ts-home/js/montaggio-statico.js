function previewPages () {
  var pageName = location.search.slice(1)
  $('[data-page]').not('[data-page="' + pageName + '"]').remove();
  document.body.className = pageName;
}



/* DOC READY */
$( function () {
  previewPages();
});


new Vue({
  el: '.hero__slider'
});
 

new Vue({
  el: '.full-slider'
});


new Vue({
  el: '.eventi'
});

'welfare', 'agyo', 'fashion', 'vitivinicolo'


// Home
new Vue({
  el: '.alt-sol-dig',
  data: {
    links: [
      {
        label: 'welfare',
        id: 'welfare',
        copy: "TeamSystem Welfare è una        soluzione digitale per Welfare e        Flexible benefits; un portale web        che consente di gestire in totale        autonomia il proprio conto        Welfare, offrendo uno strumento        di controllo sia all’azienda sia ai        singoli dipendenti."
      },
      {
        label:'agyo',
        id: 'agyo',
        copy: " Piattaforma in cloud che favorisce,  in un unico circuito digitale, lo        scambio di documenti tra aziende,        professionisti, pubblica        amministrazione e agenzia delle        entrate. AGYO permee alle        aziende di scambiare e        conservare faure digitali a norma        di legge, meendo a disposizione        anche un servizio di firma        eleronica."
      },
      {
        label: 'fashion',
        id: 'fashion',
        copy: "ALYANTE Fashion è la soluzione         pensata per rispondere alle        esigenze specifiche di chi        produce e/o commercializza        abbigliamento e accessori, marchi        di lusso o promuove il fast fashion        retailer. Per raggiungere i clienti        ovunque si trovino, controllare la        supply chain, gestire i punti        vendita e le vendite online."      },
      {
        label: 'vitivinicolo',
        id: 'vitivinicolo',
        copy: "ALYANTE Vitivinicolo automatizza         tutte le attività di Aziende e         Cooperative di qualunque         dimensione operanti nella         vinificazione, imboyttigliamento,         commercializzazione del vino con         particolare riguardo al presidio         dei processi produttivi, degli         adempimenti amministrativi e         fiscali e delle operazioni di cantina"       }
    ]
  }
});


new Vue({
  el: '.store__slider'
});