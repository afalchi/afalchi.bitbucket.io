/*

  gulp dev                              Start server, browsersync
  gulp copy                             [manually add new dependency to the task]  
  gulp copy-bootstrap-sass              Copia file sass da customizzare
  minify-custom-bootstrap-css           


*/


var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var pkg = require('./package.json');
var sourcemaps = require('gulp-sourcemaps');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');


// Compiles SCSS files from /scss into /css
gulp.task('sass', function() {
	return gulp.src('scss/main.scss')
		.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    //.pipe(header(banner, {   pkg: pkg		}))
		.pipe(postcss([ autoprefixer() ]))
		.pipe(sourcemaps.write('./maps')) // Output source in separate file
    .pipe(gulp.dest('css'))
    .pipe(browserSync.reload({
      stream: true
    }))
  ;
});

// Compiles SCSS Bootstrap files from /scss into /css
gulp.task('compile-custom-bootstrap', function() {
	return gulp.src('vendor/bootstrap/custom/scss/bootstrap.scss')
		.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
		.pipe(postcss([ autoprefixer() ]))
		.pipe(sourcemaps.write('./maps')) // Output source in separate file
    .pipe(gulp.dest('vendor/bootstrap/custom/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
  ;
});


// Minify compiled CSS
gulp.task('minify-css', ['sass'], function() {
  return gulp.src('css/main.css')
    .pipe(cleanCSS({
      compatibility: 'ie10'
    }))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('css'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

// Minify compiled Bootstrap CSS
gulp.task('minify-custom-bootstrap-css', ['compile-custom-bootstrap'], function() {
  return gulp.src('vendor/bootstrap/custom/css/bootstrap.css')
  
    .pipe(cleanCSS({
      compatibility: 'ie10'
    }))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('vendor/bootstrap/custom/css'))
    .pipe(browserSync.reload({
      stream: true
    }));
});



// Minify custom JS
gulp.task('minify-js', function() {
  return gulp.src('js/main.js')
    .pipe(uglify())
    // .pipe(header(banner, { pkg: pkg }))
    .pipe(rename({ 
      suffix: '.min'
    }))
    .pipe(gulp.dest('js'))
    .pipe(browserSync.reload({
      stream: true 
    }))
});

// Copy vendor files from /node_modules into /vendor
// NOTE: requires `npm install` before running!
gulp.task('copy', function() {

 // Copy loop
 const modules = [
  'animate.css',
  'bootstrap',
  'font-awesome',
  'jquery',
  'jquery.easing',
  'jquery-ui-dist',
  'lodash',
  'modernizr',
  'slick-carousel',
  'stickyfilljs',
  'scrollreveal',
  'vue'
  ];

  modules.forEach( item => {
    gulp.src([`node_modules/${item}/**/*`]).pipe(gulp.dest(`vendor/${item}`));
  });

});


// Copy Bootstrap SCSS
// Bootstrap SASS  
gulp.task('copy-bootstrap-sass', function() {
  gulp.src(['node_modules/bootstrap/scss/**/*'])
    .pipe(gulp.dest('vendor/bootstrap/scss'));
});


// Default task
gulp.task('default', [
  'sass', 'minify-css', 
  // 'minify-js', 
  'copy', 'compile-custom-bootstrap', 'minify-custom-bootstrap-css']);


// Configure the browserSync task
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: ''
    },
  })
});

// Dev task with browserSync
gulp.task('dev', [   
  'sass',   
  // 'minify-css', 
  'compile-custom-bootstrap',
  'browserSync', 
  // 'minify-custom-bootstrap-css'
  // 'minify-js'
], 
  function() {
    gulp.watch('scss/*.scss', ['sass']);
    gulp.watch('vendor/bootstrap/custom/scss/*.scss', ['compile-custom-bootstrap']);
    // gulp.watch('css/*.css', [// 'minify-css']);
    // gulp.watch('css/*.css', [// 'minify-css']);
    // gulp.watch('js/*.js', ['minify-js']);
    // Reloads the browser whenever HTML or JS files change
    gulp.watch('*.html', browserSync.reload);
    gulp.watch('js/**/*.js', browserSync.reload);
  }
);
