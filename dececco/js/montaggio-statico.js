function previewPages () {
  var pageName = location.search.slice(1)
  $('[data-page]').not('[data-page="' + pageName + '"]').remove();
  document.body.className = pageName;
}



/* DOC READY */
$( function () {
  previewPages();
});




// Home
new Vue({
  el: '[data-page="home"]',
  data: {
    links: [
      {
        label: 'people',
        id: 'people'
      },
      {
        label:'percorsi',
        id: 'percorsi'
      },
      {
        label: 'tslife',
        id: 'ts-life'
      },
      {
        label: 'people',
        id: 'people'
      },
      {
        label: 'posizioni aperte',
        id: 'posizioni'
      }
    ]
  }
});


// Ricette
new Vue({
  el: '[data-page="ricette"]'
});



// Certificati
new Vue({
  el: '[data-page="certificati"]'
});


// Contatti
new Vue({
  el: '[data-page="contatti"]',
  data: {
    sites: [
      {
        country: 'Italia',
        name: 'Sede e stabilimento di Fara San Martino',
        address: 'F.lli De Cecco di Filippo Fara S. Martino S.p.a. \
          Zona Industriale 66015, \
          Fara San Martino (CH) - Italy',
        tel: 'Tel. 0872.9861 - Fax 0872.980426'
      },
      {
        country: 'Italia',
        name: 'Stabilimento di Ortona',
        address: 'Caldari di Ortona (CH), S.S.538 Km. 6,500 Italy ',
        tel: 'Tel. 085 9039009 - Fax 085 9039008'
      },
      {
        country: 'Italia',
        name: 'Centro direzionale Uffici Commerciali',
        address: 'Via Misticoni, 5 \
        65127 Pescara – Italy',
        tel: ' Tel. 085.454861 - Fax 085.45486385'
      },
      {
        country: 'United Kingdom',
        name: 'De Cecco U.K. LTD',
        address: ' The Old School House Station Street \
        Kibworth Beauchamp Leicester LE8 0LN',
        tel: 'Tel. 0116 - 2790022 - Fax 0872-1135492'
      },
      {
        country: 'France',
        name: 'De Cecco France',
        address: ' ZAC BOIS CHALAND 10, Rue du Bois \
        Chaland - 91029 LISSES EVRY - FRANCE',
        tel: 'Tel. +33(0)160788662 - Fax +33(0)160786794'
      }
    ]
  }
});


// News
new Vue({
  el: '.news__list',
  data: {}
});