/* ----------------------------------------------
  FX
-----------------------------------------------*/
function fxSetup() {  
    
  
  
  // Rellax
  if (document.querySelector('.rellax')) {
    var rellax = new Rellax('.rellax', {
      speed: -2,
      center: true,
      round: false,
    });
  }
    

  // Scroll Reveal
  $('[data-page] > section').each( function (i,e) {
    $('.wow', e).length > 0 ? e.dataset.wowSet = i : null;
  });

  

  window.sr = ScrollReveal();

  $('[data-wow-set]').toArray().forEach( function (e,i) {   
    var selector = '[data-wow-set="' + i + '"] .wow';    
    sr.reveal(selector, {
      //container: '[data-wow-set="' + i + '"]',
      distance: '20px',
      duration: 1000,
      easing: 'ease-out',
      mobile: false,
      reset: true,
      scale: null,
      useDelay: 'always',
      beforeReveal: function (domEl) {
      },
    }, 150);
  });


    // window.sr = new ScrollReveal();
    
    // this.wrapEl();
    
    // sr.reveal('.wow-up');



  
    // ScrollReveal
    // window.sr = ScrollReveal();
// 
    // sr.reveal('.wow', {
    //   //container: 'section',
    //   distance: '20px',
    //   duration: 1000,
    //   easing: 'ease-out',
    //   // mobile: false,
    //   // reset: false,
    //   // reset: true,
    //   scale: null,
    //   useDelay: 'always'
    // }, 300);

    /*

    sr.reveal('.wow-delay', {
      distance: '20px',
      duration: 500,
      easing: 'ease-out',
      origin: 'bottom',
      scale: null,
      useDelay: 'always'
    }, 150);

    sr.reveal('.wow-right', {
      distance: '20px',
      duration: 1000,
      easing: 'ease-out',
      origin: 'right',
      // reset: false,
      scale: null,
      useDelay: 'once'
    });
  
    sr.reveal('.wow-fade', {
      distance: 0,
      delay: 100,
      duration: 1000,
      easing: 'ease-out',
      mobile: false,
      reset: false,
      scale: .8,
      // useDelay: 'onload'
    }, 100);
  
    */
  
  
    /* Position sticky polyfill */
    var stickyElements = document.getElementsByClassName('sticky');  
    for (var i = stickyElements.length - 1; i >= 0; i--) {
        Stickyfill.add(stickyElements[i]);
    }
    
  }
  
  
  
  
  /* ----------------------------------------------
    Main Nav
  -----------------------------------------------*/
  function mainNav () {

    var $navItems = $('.main-nav .nav-item');

    // Effetto Hover invertito
    $navItems.hover(
      function() {
        $navItems.not(this).addClass('not-hover');
      },
      function() {
        $navItems.not(this).removeClass('not-hover');
      }
    );

    // Gradiente di sfondo Desktop
    $navItems
      .on('show.bs.dropdown', function () {
        $('.main-nav').addClass('desktop-open');
      })
      .on('hide.bs.dropdown', function () {
        $('.main-nav').removeClass('desktop-open');
      })
    ;
  
    
    // Gradiente di sfondo mobile
    $('.main-nav__hamburger').click(function () {
      $('.main-nav').toggleClass('open');
    });


    // Ricerca / cambio lingua
    $('.main-nav__service a').click(function(ev) {
      ev.preventDefault();
      var menu = this.dataset.menu;
      $('.menu-overlay section').hide();
      $('.menu-overlay__' + menu).show();       
      $('#menu-overlay').modal('show');  
    });


  }

  
  
  /* ----------------------------------------------
    Homepage
  -----------------------------------------------*/
  function sliderHP () {
    $('.hp-hero__slider').slick({
      dots: true,
      fade: true,
      arrows: false,
      autoplay: true,
      autoplayDelay: 3000,
      speed: 2000,
    });
  }
  
  
  /* ----------------------------------------------
    Componente riepilogo con icone
  -----------------------------------------------*/
  function infolist () {  
  
    $('.infolist__difficolta').each(function(i,e) {
      var $this = $(e),
          val = parseInt($this.text()),
          iconFullMarkup = (function () {
            if ( e.classList.contains('blu') ) {
              return '<span class="info-icon difficolta-blu"></span>';
            } else {
              return '<span class="info-icon difficolta"></span>';
            }
          } ()),
          iconEmptyMarkup = (function () {
            if ( e.classList.contains('blu') ) {
              return '<span class="info-icon difficolta-vuoto-blu"></span>';
            } else {
              return '<span class="info-icon difficolta-vuoto"></span>';
            }
          } ())
      ;
  
      $this.empty();
      for (var i=0; i<5; i++) {
        i < val ? $this.append(iconFullMarkup) : $this.append(iconEmptyMarkup);
      }
    });
  } 
  
  
  /* ----------------------------------------------
    FAQ
  -----------------------------------------------*/
  function faq () {
  
    if( $('[data-page="faq"]').length === 0 ) {
      return;
    }
  
    new Vue({
      el: '[data-page="faq"]',
      data: {
        faq: [],
        keyword: ''
      },
      computed: {
        filteredList: function () {
          var vm = this;
          return vm.faq.filter(function (item) {
            return item.q.toLowerCase().indexOf(vm.keyword) !== -1;
          });
        }
      },
      methods: {
        deepLink: function () {
          // Apertura cassetto in base all'hash nell'url
          if (location.hash !== '') {
            $('html, body').animate({
              scrollTop: $(location.hash).offset().top
            }, 2000);
            this.toggle(location.hash.substr(1));
          }

        },
        loadFAQ: function () {
          var vm = this;
          $.get('media/faq.json')
            .done(function(response) {
              vm.faq = response;
              Vue.nextTick(function () {
                vm.deepLink();
              });              
            })
            .fail(function() {
              console.log('Error loading data!');
            })
          ;
        },
        toggle: function (id) {
          var item = '#' + id;
          $(item).toggleClass('open');
          $(item + ' .faq__answer__text').stop().slideToggle();
        }      
      },
      mounted: function () {
        this.loadFAQ();
        
      }
    });
  
  
    $('.faq__item').click(function(e){
      
    });
  }
  
  
  
  /* ----------------------------------------------
    Storia
  -----------------------------------------------*/
  function storia () {
  
    if( $('[data-page="storia"]').length === 0 ) {
      return;
    }
  
  
    new Vue({
      el: '[data-page="storia"]',
      data: {
        slickInitialized: false,
        currentSlide: 0
      },
      computed: {
        anni: function () {
          return $.makeArray(document.querySelectorAll('[data-storia-anno]')).map(function(item) {
            return item.getAttribute('data-storia-anno');
          });
        }
      },
      methods: {
        gotoSlide: function (event) {
  
          var thisDate = event.target,
              index = parseInt(thisDate.dataset.navIndex);
  
          !this.slickInitialized ? this.initSlider() : null;
          
          if (this.currentSlide !== index) {          
            $('.storia__slider').slick('slickGoTo', index);
            this.currentSlide = index;
          }
            
        },
        initSlider: function () {
          var vm = this;
          $('.storia__hero .row').addClass('fadeOutUp20 animated'); 
          $('.storia__slider')
            .addClass('visible')
            .fadeIn()
            .on('init', function (ev) {
              var slick = ev.target;

              console.log(slick);
            })
            .slick({
              appendDots: '.storia__timeline__wrap',
              mobileFirst: true,
              dots: false,
              // arrows: false,              
              responsive: [
                {
                  breakpoint: 992,
                  settings: {
                    dots: false,
                    arrows: true
                  } 
                }
              ]
            })
            .on('beforeChange', function(event, slick, current, next) {
            })   
            .on('afterChange', function(slick, current) {
              vm.currentSlide = current.currentSlide;
              
            })            
          ;   

          vm.slickInitialized = true;    
        }
      },
      mounted: function() {
        var vm = this;
      }
    });
  
    
    
  }
  
  
  
  /* ----------------------------------------------
    Scheda prodotto
  -----------------------------------------------*/
  function scheda() {
  
    // Metodo
    new Vue({
      el: '.scheda__metodo',
      data: {
        playBtn: {
          top: null,
          left: null
        }
      }, 
      methods: {
        getBtnPosition: function (elem) {
  
          // Top
          this.playBtn.top = elem.offsetTop + (elem.offsetHeight / 2) + 'px';
  
          // Left
          this.playBtn.left = (function () {
            var img = elem.querySelector('img'),
                imgW = img.offsetWidth,
                imgLeft = elem.offsetLeft + img.parentElement.offsetLeft + img.offsetLeft           
            ;
            return imgLeft + (imgW / 2) + 'px';
          }());
          
  
        },
        overrideCurrentItem: function (ev) {
  
          // Find current item
          var item = $(ev.target).parents('.scheda__metodo__item')[0];
  
          // Get position
          this.getBtnPosition(item);
        }
      },
      mounted: function () {
        var vm = this;
  

        // Scrollreveal setup (desktop)
        sr.reveal('.js-scrollreveal', {
          reset: true,
          mobile: false,
          distance: '60px',
          duration: 3000,
          scale: 1,
          easing: 'ease-out',
          viewFactor: 0.4,
          beforeReveal: function (domEl) {
  
            // Find first element 
            var item = $(domEl).parents('.scheda__metodo__item')[0];
  
            // Get initial position
            vm.getBtnPosition(item);
          }
        });        


        // Slick setup (mobile)
        $('.scheda__metodo__slider').slick({
          mobileFirst: true,
          dots: true,
          arrows: false,
          adaptiveHeight: true,
          responsive: [
            {
              breakpoint: 992,
              settings: 'unslick'
            }
          ]
        });
  
      }
    });

    // Prodotto correlato
    function correlato() {
      var rellax = {
        instance: null,
        isActive: false
      };  
        
      function checkWidth() {
        if (window.matchMedia('(min-width: 992px)').matches) {
          // Parallax Setup 
          if (!rellax.isActive) {            
            rellax.instance = new Rellax('.rellax--correlato', {
              speed: -2,
              center: true,
              round: false,
            });            
            rellax.isActive = true;
          }         
          
        } else {
          if (rellax.isActive) {
            rellax.instance.destroy();
            rellax.isActive = false;
          }                
        }
      }
      
  
      // Responsive check 
      var debounceCheck = _.throttle(checkWidth, 1000);
      $(window).on('resize', debounceCheck);
      checkWidth();
    }


    // Init
    correlato();
    
  }
  
  
  
  
  /* ----------------------------------------------
    Metodo
  -----------------------------------------------*/
  function fullpage() {
  
    new Vue({
      el: '.dececco-fullpage',
      data: {
        currentChapter: document.querySelector('.fullpage .section').dataset.chapter,
        currentSection: 0,
        viewChapters: false      
      }, 
      computed: {
        spinnerValue: function () {
          return (125 / (this.totalSections - 1) ) * (this.currentSection) || 0;
        },
        totalSections: function () {
          return $('.fullpage .section').length;
        }
      },
      methods: {
        gotoChapter: function () {
          // Empty initialization
        },
        toggleVideo: function (event, action) {
          event.preventDefault();
          var $currentSection = $(event.target).parents('.section'),
              $videoRow = $currentSection.find('.fullpage__wrapper'),
              $textRow = $currentSection.find('.fullpage__video'),
              $iframe = $currentSection.find('iframe')
          ;
  
          $videoRow.add($textRow).slideToggle();
  
          // Gestione riproduzione video
          $iframe[0].contentWindow.postMessage('{"event":"command","func":"' + action + 'Video","args":""}','*');
  
        }
      },
      mounted: function () {
  
        // Setup Fullpage
        var vm = this; 
        $('#fullpage').fullpage({
          css3: false,        
          fixedElements: '#header, .footer',
          afterLoad: function(anchorLink, index) {
  
            // Defining method after Fullpage init
            vm.gotoChapter = function (section) {
              $.fn.fullpage.moveTo(section, 0);
              vm.viewChapters = false;
            }            
            
          },
          onLeave: function(index, nextIndex, direction) {
  
            var next = document.querySelector('.fullpage .section:nth-child(' + nextIndex + ')');
  
            // Set direction class for animation
            next.classList.remove('scrolled-up');
            next.classList.remove('scrolled-down');
            next.classList.add('scrolled-' + direction);
            
            vm.currentSection = nextIndex - 1;
            vm.currentChapter = next.dataset.chapter;
          },
        });
  
        // Init Youtube API
        window.onYouTubeIframeAPIReady = function() {
          new YT.Player();
        }
  
      }
    });
    
  }
  
  
  
  /* ----------------------------------------------
    News List
  -----------------------------------------------*/
  function newsList() {
  
    // Effetto hover inverso
    $(document)
      .on('mouseenter', '.news__list .card', function () {
        $('.news__list .card').not(this).addClass('unfocus');
      })
      .on('mouseleave', '.news__list .card', function () {
        $('.news__list .card').removeClass('unfocus');
      })
    ;
  }


/* ----------------------------------------------
  Servizio clienti
-----------------------------------------------*/
function servizioClienti() {

  

  $('#location-modal').on('shown.bs.modal', function(ev) {

    var position = {lat: parseFloat(this.dataset.lat), lng: parseFloat(this.dataset.lng)};
    var map = new google.maps.Map(document.getElementById('location-modal__map'), {
      zoom: 6,
      center: position,
      styles: [{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#aee2e0"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#abce83"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#769E72"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#7B8758"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"color":"#EBF4A4"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#8dab68"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#5B5B3F"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ABCE83"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#A4C67D"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#9BBF72"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#EBF4A4"}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#87ae79"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#7f2200"},{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"},{"visibility":"on"},{"weight":4.1}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#495421"}]},{"featureType":"administrative.neighborhood","elementType":"labels","stylers":[{"visibility":"off"}]}]
    });
    var marker = new google.maps.Marker({
      position: position,
      map: map
    });
  });

  $('.contatti__open-modal').click(function(ev) {
    ev.preventDefault();
    var lat = this.dataset.lat,
        lng = this.dataset.lng
    ;
    document.querySelector('#location-modal').dataset.lat = lat;
    document.querySelector('#location-modal').dataset.lng = lng;
    $('#location-modal').modal('show');

  });
}

  
  
/* ----------------------------------------------
  DOC READY
-----------------------------------------------*/
$(function () {

  // Sovrascrive stile su global.scss
  document.body.style.display = 'block';

  
  mainNav();
  sliderHP();
  infolist();
  newsList();
  servizioClienti();

  if( $('[data-page="faq"]').length ) {
    faq();
  }

  
  storia();

  if( $('[data-page="scheda"]').length ) {
    scheda();
  }

  if( $('.dececco-fullpage').length ) {
    fullpage();
  }

  fxSetup();

});
  
  
  
  
  
  
  
  
  