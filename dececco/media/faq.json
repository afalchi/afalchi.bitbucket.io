[
  { "id": "confezione",
    "q":"La pasta integrale ha una confezione in cartoncino, quella di semola secca di grano duro in plastica. Hanno esigenze di conservazione diverse?",
    "a":"Non ci sono esigenze diverse di conservazione: la pasta integrale è una specialità, la confezione in cartoncino è una esigenza di differenziazione dalla linea cosiddetta Base."
  },
  { "id": "conservazione",
    "q":"Come va conservata la pasta De Cecco perché mantenga le sue caratteristiche?",
    "a":"Come da istruzioni in etichetta, la pasta va conservata in un luogo fresco ed asciutto che tipicamente corrisponde ad una temperatura di circa 20°C. Comunque anche temperature da 5 a 30°C non apportano cambiamenti qualitativi. In estate va bene conservarla in dispensa. Consigliamo però di non porre la pasta vicino a farina, cereali e soprattutto riso e legumi."
  },
  { "id": "grano-aureo",
    "q":"De Cecco utilizza il “Grano Aureo” italiano?",
    "a":"No. Dall'antica conoscenza dei grani tramandata dal nonno Nicola, mugnaio in Fara san Martino, in De Cecco riteniamo che la semola ottenuta da monovarietà di grano non possa dare quelle garanzie di qualità che otteniamo invece da miscele di grani, italiani o meno, ognuno scelto per il meglio delle sue caratteristiche. Utilizziamo grani anche con livelli di proteine del 15 – 16 %."
  },
  { "id": "formato-preferito",
    "q":"De Cecco ha oltre 200 formati disponibili. Ce n'è qualcuno a cui siete particolarmente legati?",
    "a":"La gamma dei formati risponde alle richieste dei consumatori secondo le loro preferenze di gusto e necessità, come De Cecco il nostro obiettivo è soddisfare al meglio i bisogni dei consumatori. Comunque, tra i nostri formati più distintivi ricordiamo: spaghettoni, mezzanelli, fettuccelle e penne a candela."
  },
  { "id": "strategia",
    "q":"Qual è la strategia principale per un prodotto di qualità come la pasta De Cecco?",
    "a":"La strategia fondamentale è quella di effettuare scelte produttive volte solo alla qualità del prodotto finale. Le materie prime per la produzione della pasta sono esclusivamente semola e acqua. De Cecco sceglie tra le migliori varietà di grano duro, che miscela e stocca nei propri silos per mantenere gli stessi standard qualitativi durante tutta la produzione; i grani vengono macinati nel molino di proprietà adiacente allo stabilimento e la semola che viene utilizzata è sempre fresca, mantenendo tutta la sua fragranza. La semola De Cecco ha una granulometria grossa: questa scelta se da un lato necessita di tempi più lunghi per l'impasto, dall'altro riduce il danneggiamento dei granuli di amido e delle proteine del grano e permette che si esprimano al meglio le proprietà organolettiche della semola."
  },
  { "id": "processo-produttivo",
    "q":"Come avviene il processo produttivo De Cecco?",
    "a":"L'intero processo di pastificazione De Cecco prevede una serie di accorgimenti basilari in tutte le singole fasi, mirati ad ottenere un'elevata qualità del prodotto finale. Tali principi, uniti a soluzioni tecnologiche moderne, scelte nel rispetto dei valori naturali delle materie prime (mild-technologies), fanno della pasta De Cecco un prodotto unico. Innanzitutto si dedica cura estrema alla selezione dei grani duri, che devono corrispondere a standard qualitativi elevati (contenuto proteico, glutine, colore, etc.) e che vengono miscelati in modo da avere il mix più adeguato. La macinazione prevede l'estrazione del solo cuore del chicco e viene condotta in modo da ottenere una semola a grana grossa, nel pieno rispetto delle caratteristiche della materia prima che in questo modo subisce danni limitati. La semola viene impastata con acqua fredda, garantendo le migliori condizioni per la formazione del glutine. Infine l'essiccazione lenta a bassa temperatura, che permette di mantenere inalterate le proprietà organolettiche e nutrizionali del grano duro, limitando il danno termico. Ogni fase del processo come anche il prodotto vengono costantemente controllati, grazie all'utilizzo di tecnologie all'avanguardia e alle attente analisi di laboratorio effettuate nel nostro Centro Ricerche altamente qualificato. Possiamo quindi affermare che la qualità globale De Cecco si costruisce, tassello dopo tassello, in ogni singola fase dell'intera filiera produttiva."
  },
  { "id": "caratteristiche-trafila-in-bronzo",
    "q":"Quali sono le caratteristiche che la trafila in bronzo conferisce al prodotto?",
    "a":"Il bronzo ha il grande vantaggio di conferire ruvidità alla superficie della pasta, rendendola capace di trattenere al meglio qualsiasi condimento. La trafilatura al bronzo è un passaggio critico per il pastaio, la cui abilità sta tutta nel regolare la consistenza dell'impasto e la velocità di estrusione per ottenere una pasta di un bel giallo paglierino."
  },
  { "id": "trafila-in-bronzo",
    "q":"Quanto dura una trafila in bronzo?",
    "a":"La durata della trafila in bronzo dipende dalla tipologia del formato che viene lavorato. Alcune trafile, come quelle degli spaghetti, possono lavorare anche fino a 1400-1500 ore, mentre per altri formati che comportano una maggiore usura degli inserti, si scende a 400-500 ore."
  },
  { "id": "essiccazione-bassa-temperatura",
    "q":"Che cosa significa “essiccazione a bassa temperatura”?",
    "a":"L'essiccazione a bassa temperatura affonda le proprie radici nella tradizione dell'arte pastificatoria, quando la pasta veniva essiccata al sole affinché si potesse conservare nel tempo. La bassa temperatura - cioè inferiore a 65°C - consente di mantenere inalterate le caratteristiche organolettiche del prodotto, limitando il danno termico. Inoltre, dal punto di vista nutrizionale, con la bassa temperatura, non si ha una drastica perdita di alcuni aminoacidi essenziali, come la lisina. Il processo tradizionale non ha alcun effetto positivo sulla qualità in cottura della pasta, se non appunto quello di renderla conservabile e di preservarne la qualità. Ciò significa che la qualità in cottura della pasta essiccata a bassa temperatura è praticamente correlata alla qualità della materia prima: grani di media o bassa qualità darebbero come risultato paste con cotture non uniformi, collose e poco tenaci."
  },
  { "id": "essiccazione-tradizionale",
    "q":"Qual è il processo tradizionale di essiccazione De Cecco?",
    "a":"Il processo tradizionale De Cecco è basato proprio sull'essiccazione lenta e a bassa temperatura, per garantire la conservazione del patrimonio organolettico-nutrizionale della semola e per ottenere un prodotto finale dal gusto molto fragrante, che conserva pienamente il sapore caratteristico del grano. Inoltre l'impiego di tecnologie a bassa temperatura permette di conservare e addirittura favorire tramite il rinvenimento (una sorta di fase di “riposo” della pasta durante l'essiccazione) la caratteristica elasticità della pasta."
  },
  { "id": "essiccazione-alta-temperatura",
    "q":"Come si riconosce una pasta essiccata ad alta temperatura?",
    "a":"Un segno riconoscibile di una pasta essiccata ad alta temperatura, cioè superiore ai 75°C, è una colorazione più intensa e un certo appiattimento del sapore. Il sensibile miglioramento della qualità della pasta in cottura va attribuito ad una sorta di “impermeabilizzazione” che l'alta temperatura conferisce al prodotto, tanto che possono essere utilizzate materie prime meno pregiate e ottenere comunque un prodotto che rimane “al dente”. Tale tecnologia però potrebbe diminuire il contenuto proteico della pasta e in particolare del contenuto in lisina, un amminoacido essenziale che influisce in modo sostanziale sul valore nutrizionale della pasta."
  },
  { "id": "grano",
    "q":"Che grano utilizza De Cecco?",
    "a":"Non esistendo in natura un grano ideale, De Cecco in virtù della sua esperienza storica, seleziona un mix di grani duri italiani ed esteri - australiani, francesi e principalmente americani - scelti soprattutto per il loro contenuto proteico e per la quantità e qualità del glutine. I grani americani, ad esempio, sia per le condizioni ambientali (terreni fertili e poco sfruttati), sia per le tecniche colturali di rotazione delle coltivazioni, presentano un elevato contenuto proteico, un ottimo indice di glutine e un colore giallo piuttosto intenso. La resa di coltivazione dei grani americani è infatti di circa 20 q.li/ha (quintali per ettaro) contro i 50-60 q.li/ha in Italia: non si ha uno sfruttamento intensivo del terreno e quindi i nutrienti sono a disposizione per garantire lo sviluppo delle migliori qualità della varietà. La qualità ottimale della materia prima è infatti indispensabile per garantire la caratteristica fondamentale della pasta (la tenuta in cottura) in un processo che utilizza la bassa temperatura. Il grano viene stoccato presso i silos del molino in quantitativi molto elevati in modo da garantire la disponibilità di materia prima di ottima qualità, al fine di avere sempre un prodotto omogeneo per tutta la produzione."
  },
  { "id": "ogm",
    "q":"De Cecco utilizza grano OGM?",
    "a":"No, la politica della De Cecco è di non utilizzare Organismi Geneticamente Modificati. Inoltre, la legge non ammette l'utilizzo di grano duro OGM."
  },
  { "id": "semola",
    "q":"Come si ottiene la semola dal grano?",
    "a":"Il chicco di grano è formato da diversi strati e la semola viene estratta dal più interno, l'endosperma amilaceo. Una volta che il grano viene pulito e sanificato, si passa alla macinazione vera e propria. Inizialmente il grano passa attraverso una serie di laminatoi che progressivamente rompono i chicchi di grano, li “svestono” degli strati più esterni e li riducono successivamente di dimensione. A questo punto il prodotto macinato passa attraverso dei buratti che separano tritello (la crusca), farinaccio e farina. Infine si ha l'ultimo passaggio alle semolatrici, per ottenere i cosiddetti prodotti “nobili” della macinazione del grano duro: la semola e il semolato. Scelti i grani rispondenti al meglio alle esigenze produttive, inizia il processo di macinazione che per De Cecco ha una resa del 62%, dal momento che si è scelto di estrarre solo il cuore del chicco (contro l'oltre 70% in media di un molino moderno). Nella macinazione vengono scartate tutte le farinette, i semolini, i semolati e i sottoprodotti che andrebbero ad aumentare il tenore di ceneri con ripercussioni sul sapore: la semola De Cecco infatti si attesta ad una valore di ceneri intorno allo 0.82%, contro il limite fissato dalla legge di 0.90%."
  },
  { "id": "granulometria",
    "q":"Che cosa indica la granulometria?",
    "a":"La granulometria indica il diametro delle particelle in cui viene macinata la semola ed ha un effetto diretto sulla fattura dell'impasto: semole fini hanno il vantaggio di velocizzare la formazione dell'impasto e di permettere facilmente l'idratazione completa della semola con conseguente riduzione dei punti bianchi. La scelta di De Cecco di utilizzare semole a granulometria grossa (>450-500 micron) se da un lato comporta tempi e costi maggiori di produzione, dall'altro lato permette che vengano espresse al meglio le qualità tecnologiche del grano: non a caso in tempi passati si parlava di semola a granulometria grossa come di “semola sublime”. Inoltre il processo di macinazione comporta dei danneggiamenti alla struttura del grano che, nel caso di una semola a granulometria grossa, sono molto inferiori rispetto ad una semola sottile."
  },
  { "id": "acqua-impasto",
    "q":"Che importanza ha l'acqua nell'impasto?",
    "a":"L'acqua è necessaria per impastare la semola; De Cecco utilizza acqua fredda, circa 8°C, di una sorgente montana di proprietà, per creare le migliori condizioni per la formazione del glutine, anche se questo comporta tempi più lunghi di impastamento. Infine, impasti con acqua fredda conferiscono un sapore più dolce al prodotto."
  },
  { "id": "glutine-aggiunto",
    "q":"De Cecco aggiunge glutine nella pasta?",
    "a":"No, la pasta De Cecco è fatta esclusivamente con semola di grano duro e acqua, senza ulteriore aggiunta di glutine. Per ottenere un prodotto qualitativamente superiore e che abbia un'ottima tenuta in cottura, De Cecco punta sull'accurata selezione di grani duri che presentino un elevato contenuto proteico e un ottimo indice di glutine. Peraltro la legge dice che è possibile l'aggiunta di altri ingredienti oltre alla semola di grano duro e all'acqua, ma questi devono essere espressamente dichiarati in etichetta."
  },
  { "id": "punti",
    "q":"Cosa sono i punti bianchi o neri visibili sulla pasta?",
    "a":"La pasta De Cecco viene prodotta utilizzando semola a granulometria grossa e pastificata con acqua fredda in modo da limitare il danneggiamento dei granuli di amido e per permettere uno sviluppo ottimale del glutine. Questa scelta comporta una maggiore difficoltà nell'idratazione dei granuli di semola con conseguente formazione di puntini bianchi. I punti neri, che ogni tanto si trovano sulla pasta, sono il risultato della macinazione del grano, in cui si possono occasionalmente trovare semi di altre piante (quali ad esempio la veccia)."
  },
  { "id": "stabilimenti",
    "q":"Quanti stabilimenti ha De Cecco?",
    "a":"De Cecco possiede due stabilimenti a pochissimi chilometri di distanza l'uno dall'altro. Non c'è nessuna differenza tra i due stabilimenti sia per quanto riguarda la materia prima che per i processi produttivi."
  },
  { "id": "",
    "q":"De Cecco è certificata?",
    "a":"Gli standard adottati dalla De Cecco sono tutti volontari. Le certificazioni di De Cecco sono: Certificazione di prodotto, Certificazione Ambientale, Certificazione SA8000, UNI EN ISO 9001:2000, H.A.C.C.P., British Retail Consortium, International Food Standard, Certificazione Kosher."
  },
  { "id": "micotossine",
    "q":"De Cecco esegue dei test sull'eventuale presenza di micotossine nel prodotto?",
    "a":"Certo, tutte le materie prime interessate vengono controllate per le micotossine."
  },
  { "id": "visite-guidate",
    "q":"De Cecco organizza visite guidate negli stabilimenti?",
    "a":"Sì, De Cecco organizza visite agli stabilimenti."
  },
  { "id": "bollitura",
    "q":"Con l'acqua a meno di 15°C, la temperatura dell'impasto come arriva fino a 45-50°C?",
    "a":"La massima temperatura a cui arriva l'impasto è 30-40°C per effetto della lavorazione e della temperatura della semola."
  },
  { "id": "temperatura-supera-limite",
    "q":"Se durante l'impasto la temperatura supera il limite, cosa succede alla partita scartata?",
    "a":"L'uso dell'acqua fredda fa sì che la temperatura dell'impasto non superi mai i limiti previsti."
  },
  { "id": "tipi-impasto",
    "q":"Ci sono diversi tipi di impasto a seconda dei diversi tipi di pasta?",
    "a":"La qualità delle materie prime è sempre elevata, le paste speciali prevedono l'aggiunta di ingredienti caratterizzanti (es: uova, spinaci, pomodoro)."
  },
  { "id": "conservazione-grano",
    "q":"Come viene conservato il grano dopo la selezione?",
    "a":"In silos di cemento."
  },
  { "id": "supermercati",
    "q":"In che catene di supermercati è disponibile la linea integrale De Cecco?",
    "a":"Le catene più rappresentative sono Gruppo Sma/Auchan, Iper Coop e Metro."
  },
  { "id": "data-scadenza",
    "q":"In base a che cosa viene stabilita la data di scadenza presente sulla confezione di pasta?",
    "a":"Innanzitutto desideriamo specificare che la pasta non ha una data di scadenza, ma un termine minimo di conservazione, cioè alla fine di questo periodo di tempo la pasta non si altera, ma tende solo a perdere parte delle sue caratteristiche organolettiche, se conservata in modo corretto. Questo termine minimo di conservazione decorre dalla data di produzione della pasta stessa ed è stabilito sulla base di test cosiddetti di “shelf life”, che seguono il prodotto nel tempo verificandone le caratteristiche con valutazioni sensoriali."
  },
  { "id": "sughi-olio",
    "q":"Dove vengono prodotti i sughi e l'olio De Cecco?",
    "a":"I sughi sono prodotti in Italia presso produttori qualificati dalla De Cecco su ricetta esclusiva De Cecco. L'olio è imbottigliato in Italia presso lo stabilimento De Cecco, tranne gli oli DOP e IGP e da agricoltura biologica. Gli oli DOP, IGP, Esclusivi, Pregiato e da agricoltura biologica sono di provenienza italiana mentre gli altri oli sono originari del territorio dell'Unione Europea."
  },
  { "id": "smaltimento",
    "q":"Come si smaltisce la confezione della pasta De Cecco?",
    "a":"Gli imballi delle confezioni di pasta De Cecco sono riciclabili. Pertanto possono essere separati nelle diverse frazioni (carta o plastica) e come tali smaltiti nella raccolta differenziata."
  },
  { "id": "logo",
    "q":"Quando e come è nato il logo De Cecco?",
    "a":"Il marchio della De Cecco con la giovane contadina che tiene tra le mani due mannelli di grano si richiama ai costumi tradizionali femminili, diffusi in ogni città e paese d’Abruzzo. Il marchio, registrato nel 1952, ha conosciuto numerose versioni e continui rifacimenti."
  },
  { "id": "nuovo-formato",
    "q":"Come avviene l’ideazione di un nuovo formato di pasta De Cecco?",
    "a":"L’attività di progettazione di nuovi formati si basa sulle indicazioni provenienti dal mercato (sia attraverso l’analisi di banche dati col ranking dei formati, sia attraverso segnalazioni provenienti direttamente dalla nostra Forza di Vendita) in modo da conoscere e soddisfare al meglio i bisogni dei consumatori. Operativamente si costituisce un gruppo di lavoro con i rappresentanti delle varie funzioni aziendali, al fine di progettare e realizzare il nuovo formato che naturalmente dovrà rispettare tutti i rigidi parametri qualitativi che contraddistingue ciascun prodotto De Cecco."
  },
  { "id": "ricettario",
    "q":"Esiste un ricettario De Cecco?",
    "a":"Sì, nel corso degli anni sono stati realizzati dei ricettari, l’ultimo con la collaborazione dello chef Heinz Beck. Sempre con la partecipazione dei principali chef stellati, sono stati realizzati tre volumi (Pasta, Pasta d’Autore e il recentissimo Pasta Damare) dove viene celebrata la pasta grazie all’interpretazione di una ricetta da ciascuno degli chef coinvolti."
  }
]