﻿/*--------------------------------------------------
S E E J A Y   S O C I A L   W A L L
Website by Websolute
--------------------------------------------------*/


/*--------------------------------------------------
SocialWall
--------------------------------------------------*/
var $socialWallGrid = $('.social-wall-grid'),
  socialWall = {
    codiceSocialWall: $socialWallGrid.attr('data-code'),
    accessToken: '634737799ba786bbfe74ff48a52b3f54725782e5eab78',
    maxFeed: $socialWallGrid.attr('data-max-feed') || 10,
    withMasonry: $socialWallGrid.attr('data-masonry') || 'false',
    currentFeed: 0,
    tooManyFeeds: false,
    media: [],
    loadedImages: 0,
    $data: null,
    init: function () {
      $.ajax({
        url: 'http://www.seejay.co/api/v1.0/wall/content/' + socialWall.codiceSocialWall + '?access_token=' + socialWall.accessToken,
        dataType: 'json',
        sync: true,
        cache: false,
        crossDomain: true,
        success: function (data) {
          socialWall.$data = data;

          if (socialWall.withMasonry === 'true') {
            socialWall.masonry();
          }

          socialWall.loadImages();
        }
      });
    },
    loadImages: function () {

      for (i = socialWall.currentFeed; i < socialWall.maxFeed; i++) {
        if (typeof socialWall.$data.data.items[i] != 'undefined') {

          var feed = socialWall.$data.data.items[i],
            media = feed.media || undefined;

          var img = new Image();
          socialWall.media.push(media.url);

          img.onload = function () {
            socialWall.loadedImages++;
            // console.log(socialWall.loadedImages);
            if (socialWall.loadedImages == socialWall.maxFeed - 1) {
              socialWall.createStoriesGrid();
            }
          }
          img.onerror = function () {
            inArray = $.inArray(this.src, socialWall.media);
            socialWall.$data.data.items.splice(inArray, 1);
          }
          img.src = media.url;

        }

      }
    },
    createStoriesGrid: function () {

      if (socialWall.$data.data.items.length < socialWall.maxFeed) {
        socialWall.maxFeed = socialWall.$data.data.items.length;
        socialWall.tooManyFeeds = true;
      }

      for (i = socialWall.currentFeed; i < socialWall.maxFeed; i++) {

        if (typeof socialWall.$data.data.items[i] != 'undefined') {

          var feed = socialWall.$data.data.items[i],
            $item = $(
              '<div class="stories__outer-wrapper rellax--stories"> \
                  <a href="" class="stories__item" target="_blank"> \
                    <div class="stories__inner-wrapper"> \
                      <div class="stories__text"> \
                          <div> \
                          <span class="fa" aria-hidden="true"></span> \
                          <div class="stories__author"> </div> \
                          <div class="stories__copy"> </div> \
                          </div> \
                      </div> \
                  </div> \
                </a> \
              </div>'),
            // $item = $('<div class="grid-item"><div class="grid-wrap"><div class="box"></div></div></div>'),
            source = feed.source,
            feedID = feed.id,
            permalink = feed.permalink,
            date = new Date(parseInt(feed.date)),
            text = urlify(feed.text),
            media = feed.media || undefined,
            user = feed.user,
            avatar = user.avatar.replace('https', 'http'),
            id = user.id,
            userPermalink = user.permalink,
            icon,
            socialUrl
            ;


          // Se non è Google Plus e contiene foto
          if (source != 'GP' && typeof media != 'undefined') {

            // media
            $item
              .attr('href', permalink)
              .data('rellax-speed', (function () {
                if (i === 1) { return '2'; }
                else { return .1 + (i / 10); }
              }()))
              .css('background-image', 'url(' + media.url + ')');

            // author 
            $('.stories__author', $item).text(id);

            // text
            $('.stories__copy', $item).text(text);

            // source
            $('.fa', $item).addClass('fa-' + (function () {
              switch (source) {
                case 'PT':
                  return 'pinterest';
                  break;
                case 'IG':
                  return 'instagram';
                  break;
                case 'FB':
                  return 'facebook';
                  break;
                case 'TW':
                  return 'twitter';
                  break;
              }
            }()));

            // append
            $item.appendTo($socialWallGrid);

            if (socialWall.withMasonry === 'true') {
              $wall.masonry('appended', $item);
              $wall.imagesLoaded().progress(function () {
                $wall.masonry('layout');
              });
            }

            socialWall.currentFeed++;

          } else {
            if (!socialWall.tooManyFeeds) {
              socialWall.maxFeed++;
            }
          }

        }
      }

      var rellax = new Rellax('.rellax--stories', {
        speed: -2,
        center: true,
        round: false,
      });

    },
    masonry: function () {
      $wall = $socialWallGrid.masonry({
        itemSelector: '.grid-item'
      });

      $wall.imagesLoaded().progress(function () {
        $wall.masonry('layout');
      });

    }
  }


/*--------------------------------------------------
Urlify
--------------------------------------------------*/
function urlify(text) {
  var urlRegex = /(https?:\/\/[^\s]+)/g;
  return text.replace(urlRegex, function (url) {
    return '<a href="' + url + '" target="_blank">' + url + '</a>';
  })
}


/*--------------------------------------------------
DOC READY
--------------------------------------------------*/
$(function () {
  socialWall.init();


});