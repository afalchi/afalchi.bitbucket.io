/* ----------------------------------------------------
  Common commands

  gulp dev
  gulp custom-bootstrap
  gulp copy-vendors


---------------------------------------------------- */



/* ----------------------------------------------------
  Requires
---------------------------------------------------- */
const gulp = require('gulp'),
      sass = require('gulp-sass'),
      data = require('gulp-data'),      
      stylus = require('gulp-stylus'),
      sourcemaps = require('gulp-sourcemaps'),
      babel = require('gulp-babel'),
      webserver = require('gulp-webserver'),
      concat = require('gulp-concat'),
      postcss = require('gulp-postcss'),
      autoprefixer = require('autoprefixer'),
      browserSync = require('browser-sync').create()
;



/* ----------------------------------------------------
  Sass
---------------------------------------------------- */
gulp.task('sass', () => {
  return gulp.src('src/css/sass/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('build/css'))
  ;
});



/* ----------------------------------------------------
  Custom bootstrap
---------------------------------------------------- */
gulp.task('custom-bootstrap', () => {
  return gulp.src('src/css/bootstrap/scss/**/*.scss')
    .pipe(sourcemaps.init())     
    .pipe(sass())
    .pipe(postcss([autoprefixer]))
    .pipe(sourcemaps.write('css')) 
    .pipe(gulp.dest('build/css/bootstrap'))
  ;
});



/* ----------------------------------------------------
  Stylus
---------------------------------------------------- */
gulp.task('stylus', () => {
  return gulp.src('src/css/styl/**/!(_*.styl)')
    .pipe(sourcemaps.init()) 
    .pipe(stylus())
    .pipe(postcss([
      autoprefixer({ browsers: ['last 2 versions'] })])
    ) 
    .pipe(sourcemaps.write('css'))    
    .pipe(gulp.dest('build/css'))
    .pipe(browserSync.stream())
    //   gulp.watch("app/*.html").on('change', browserSync.reload);
  ;
});




/* ----------------------------------------------------
  Babel
---------------------------------------------------- */
gulp.task('babel', () => {
  return gulp
    .src('src/js/**/*.js')
    // .pipe(sourcemaps.init())
    .pipe(babel({ 
      // plugins: ['transform-runtime'],
      presets: ['env'] 
    }))
    // .pipe(concat('main.js'))
    // .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('build/js'))

    .pipe(browserSync.stream())
  ;
});



/* ----------------------------------------------------
  Webserver
---------------------------------------------------- */
gulp.task('webserver', function() {
  gulp.src('./')
    .pipe(webserver({
      fallback:   'index.html',
      livereload: true,
      directoryListing: true,
      port: 8080,
      open: 'http://localhost:8080/build/index.html' 
    }));
});



/* ----------------------------------------------------
  Copy vendor files from /node_modules into /vendor
---------------------------------------------------- */
gulp.task('copy-vendors', function() {
  
  // [ folder, /Subfolder to include]
  const folders = [
    ['animejs', '/'],
    ['animate.css', '/'],
    ['bootstrap', '/dist'],
    ['font-awesome', ''],
    ['jquery', '/dist'],
    ['scrollreveal', '/dist'],
    ['slick-carousel', '/slick'],
    ['vue', '/build'],
  ];

  // [ folder, filename ]
  const files = [
    ['lodash', 'lodash.min.js']
  ];

  folders.forEach( item => {
    gulp
      .src([`node_modules/${item[0] + (item[1] || '')}/**/*`])
      .pipe(gulp.dest(`build/vendor/${item[0] + (item[1] || '')}`))
    ;
  });

  files.forEach( item => {
    gulp
      .src([`node_modules/${item[0]}/${item[1]}`])
      .pipe(gulp.dest(`build/vendor/${item[0]}`))
    ;
  });

});


/* ----------------------------------------------------
  Browser Sync
---------------------------------------------------- */
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: '/build'
    },
  })
});




/* ----------------------------------------------------
  Watch
---------------------------------------------------- */
gulp.task('watch', () => {
  // gulp.watch('css/sass/**/*.scss', ['sass']);
  gulp.watch('src/css/styl/**/*.styl', ['stylus']);
  gulp.watch('src/js/**/*.js', ['babel']);
}); 





/* ----------------------------------------------------
  Static Server + watching files
---------------------------------------------------- */
gulp.task('dev', ['stylus'], function() {

  browserSync.init({
      server: "./build"
  });

  gulp.watch('src/css/styl/**/*.styl', ['stylus']);
  gulp.watch('src/js/**/*.js', ['babel']);
  gulp.watch("build/*.html").on('change', browserSync.reload);
});