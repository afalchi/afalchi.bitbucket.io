'use strict';

/* ---------------------------------------------------------
  Correlati
----------------------------------------------------------*/
function correlati() {

  function initSlider() {
    $('.related .slider').slick({
      mobileFirst: true,
      dots: true,
      responsive: [{
        breakpoint: 768,
        settings: 'unslick'
      }]
    });
  }

  var resizeHandler = _.debounce(function () {
    var isMobile = window.matchMedia('(max-width: 767px)').matches,
        slickInitialized = $('.related .slider').hasClass('slick-initialized');

    if (isMobile && !slickInitialized) {
      initSlider();
    }
  }, 500);

  $(window).resize(resizeHandler).resize();
}

$(function () {
  correlati();
});