/* ---------------------------------------------------------
  Vanilla JS selector
----------------------------------------------------------*/
function S(el, context) {
  if( context ) {
    return context.querySelectorAll(el);
  } else
  return document.querySelectorAll(el);
}



/* ---------------------------------------------------------
  Utility
----------------------------------------------------------*/
function findMostCommonItem(array) {
  const countObj = array.reduce( (tot, curr, i, arr) => { 
    let count = 0;
    arr.forEach( item => {
      item === curr ? count ++ : null;
    });
    tot[curr] = count;
    return tot;
  }, {});  
  
  let max = 0,
      profile = '';

  for (let key in countObj) {
    if (countObj[key] > max) {
      max = countObj[key];
      profile = key; 
    }
  }

  return profile;
}


/* ---------------------------------------------------------
  Test data
----------------------------------------------------------*/
const questions = [
  {
    id: 'sostenibilita',
    title: 'Voglio dare il mio contributo a favore della sostenibilit\xE0 ambientale:',
    userAnswer: null,
    cursorY: '40%',
    answers: [{
      id: 'internazionalita',
      position: {
        top: '19%',
        left: '10.5%'
      },
      text: "Sensibilizzo l’opinione pubblica tramite i social network"
    }, {
      id: 'innovazione',
      position: {
        top: '19%',
        left: '34%'
      },
      text: 'Mi muovo con il car sharing'
    }, {
      id: 'etica',
      position: {
        top: '19%',
        left: '54.5%'
      },
      text: 'Sto molto attento alla raccolta differenziata '
    }, {
      id: 'ambiente',
      position: {
        top: '19%',
        left: '77.4%'
      },
      text: 'Mi iscrivo a un campo di volontariato ecologico'
    }]
  },
  {
    id: 'inquinamento',
    title: 'Per ridurre l\'inquinamento ambientale:',
    userAnswer: null,
    cursorY: '70%',
    answers: [{
      id: 'innovazione',
      position: {
        top: '10%',
        left: '5%'
      },
      text: 'Promuovo fonti di energia rinnovabile'
    }, {
      id: 'etica',
      position: {
        top: '30%',
        left: '30.2%'
      },
      text: 'Acquisto prodotti locali'
    }, {
      id: 'ambiente',
      position: {
        top: '26%',
        left: '52%'
      },
      text: 'Riduco gli sprechi'
    }, {
      id: 'internazionalita',
      position: {
        top: '23%',
        left: '75%'
      },
      text: 'Parto alla ricerca di idee e stimoli'
    }]
  }, {
    id: 'riciclo',
    title: 'Riciclo per:',
    userAnswer: '',
    answers: [{
      id: 'etica',
      position: {
        top: '22%',
        left: '6%'
      },
      text: 'Salvaguardare il futuro delle generazioni che verranno dopo di me '
    }, {
      id: 'innovazione',
      position: {
        top: '16%',
        left: '33%'
      },
      text: 'Dare nuova vita ai rifiuti'
    }, {
      id: 'internazionalita',
      position: {
        top: '41%',
        left: '52%'
      },
      text: 'Garantire la biodiversità dei nostri ecosistemi '
    }, {
      id: 'ambiente',
      position: {
        top: '23%',
        left: '76%'
      },
      text: 'Difendere l’ambiente in cui viviamo '
    }]
  }, {
    id: 'inverno',
    title: '\xC8 inverno, fa freddo!',
    userAnswer: '',
    cursorY: "20%",
    answers: [{
      id: 'innovazione',
      position: {
        top: '7%',
        left: '8%'
      },
      text: 'Ho un impianto domotico, si accende solo quando serve '
    }, {
      id: 'etica',
      position: {
        top: '30%',
        left: '29%'
      },
      text: 'Mi rifugio sotto due strati di coperte invece di accendere il riscaldamento '
    }, 
    {
      id: 'internazionalita',      
      position: {
        top: '8%',
        left: '51%'
      },
      text: 'Mi riscaldo pensando di essere alle Maldive'
    }, 
    {
      id: 'ambiente',
      position: {
        top: '30.8%',
        left: '77.5%'
      },
      text: 'Per scaldarmi prediligo gli spostamenti a piedi '
    }]
  }, {
    id: 'trasporto',
    title: 'Il mezzo di trasporto del futuro sar\xE0:',
    userAnswer: '',
    cursorY: "28%",
    answers: [
      {
        id: 'etica',
        position: {
          top: '39%',
          left: '3%'
        },
        text: 'La macchina elettrica'
      }, 
      {
        id: 'ambiente',
        position: {
          top: '51%',
          left: '28%' 
        },
        text: 'La bicicletta'
      }, 
      
      {
        id: 'internazionalita',
        position: {
          top: '48%',
          left: '52%'
        },
        text: 'Il tram'
      }, {
        id: 'innovazione',
        position: {
          top: '56%',
          left: '76%'
        },
        text: 'Lo inventerò io!'
      }
    ]
  }];

const profiles = [
  {
    id: 'etica',
    title: 'L\u2019efficiente',
    text: 'Ami la comodit\xE0 ma non gli eccessi, \n    soprattutto se questi creano danno a chi \n    ti circonda. Hai un talento per sfruttare le risorse che hai a disposizione nella maniera \n    pi\xF9 efficiente possibile, senza sprechi.',
    bg: {
      icons: [
        {
          name: 'sun',
          style: 'top: 70px; right: 50px',
          classes: 'rotate infinite'
        },
        {
          name: 'cappello',
          style: 'right: 452px; top: 176px;',
          classes: ''
        },
        {
          name: 'scia',
          style: 'right: 105px; top: 260px;',
          classes: 'fadeInRight animated infinite duration-2'
        }
      ]
    }

  }, {
    id: 'ambiente',
    title: 'Il difensore dell\'ambiente',
    text: 'Sei un amante della natura, ed \xE8 per questo \n    che sai che necessita di protezione. \n    Hai un talento per la collaborazione, \n    e sei pronto ad aiutare chi ti sta accanto. ',
    bg: {
      icons: [
        {
          name: 'arcobaleno',
          style: 'top: 147px; right: 187px;',
          classes: 'fadeInDown animated infinite duration-2'
        },
        {
          name: 'goccia',
          style: 'top: 670px; right: 140px;',
          classes: 'zoomIn animated infinite delay-1'
        },
        {
          name: 'goccia-sm',
          style: 'top: 640px; right: 90px;',
          classes: 'zoomIn animated infinite'
        },
        {
          name: 'goccia-sm-1',
          style: 'top: 620px; right: 230px;',
          classes: 'zoomIn animated infinite delay-3'
        },
        {
          name: 'goccia-sm-2',
          style: 'top: 700px; right: 500px;',
          classes: 'zoomIn animated infinite delay-9'
        }
      ]
    }
  }, {
    id: 'innovazione',
    title: 'L\'innovatore',
    text: 'Per te il progresso \xE8 tale solo se \xE8 \n    veramente sostenibile. Hai un talento per trovare nuove soluzioni che ti permettono \n    di risolvere in modo efficiente le sfide \n    che ti si pongono davanti.',
    bg: {
      icons: [
        {
          name: 'lamp',
          style: 'top: 200px; right: 500px;',
          classes: 'animated infinite bounce duration-3 delay-7'
        },
        {
          name: 'lamp-1',
          style: 'top: 100px; right: 100px;',
          classes: 'animated infinite bounce duration-3 delay-4'
        },
        {
          name: 'lamp-2',
          style: 'top: 20px; right: 440px;',
          classes: 'animated infinite bounce duration-3 delay-2'
        },
        {
          name: 'vapore',
          style: 'top: 370px; right: 47px;',
          classes: 'animated fadeInUp infinite duration-4'
        }
      ]
    }
  }, {
    id: 'internazionalita',
    title: 'Il leader',
    text: 'L\u2019unione fa la forza! Sai che il futuro della \n    Terra non \xE8 nelle mani di un singolo individuo, ma che tutti dobbiamo fare la nostra parte. \n    Hai un talento per creare nuovi legami, \n    meglio se con persone di altre culture.',
    bg: {
      icons: [
        {
          name: 'lamp',
          style: 'top: 400px; right: 70px;',
          classes: 'animated infinite bounce duration-3'
        },
        {
          name: 'nastro',
          style: 'top: 566px; right: 121px;',
          classes: ''
        },
        {
          name: 'gear-1',
          style: 'top: 200px; right: 225px;',
          classes: 'rotate infinite'
        },
        {
          name: 'gear-2',
          style: 'top: 222px; right: 261px;',
          classes: 'rotate infinite'
        },
        {
          name: 'gear-3',
          style: 'top: 280px; right: 245px;',
          classes: 'rotate infinite'
        }
      ]
    }
  }
];


/* ---------------------------------------------------------
  Vue App
----------------------------------------------------------*/
const app = new Vue({
  el: '.test',
  data: {
    isMobile: '',
    cursorX: 0,
    stage: 0,
    step: 0,
    score: 0,
    answers: [],
    hoverAnswer: '',
    questions: questions,
    profiles: profiles,
    profileResult: '' // ''
  },
  methods: {

    calculateProfile () {
      const vm = this;
      vm.profileResult = findMostCommonItem(vm.answers); 

      vm.stage = 2;
      
      const timeline = anime.timeline({
        easing: 'easeInOutQuart',
      });

      timeline
        .add({
          targets: '.profile__bg',
          // translateX: [100, 0],
          opacity: [0,1],
          duration: 3000
        })
        .add({
          targets: '.profile__result',
          translateX: [300, 0],
          opacity: [0,1],          
          duration: 2000,
          offset: 0
        })
        .add({
          targets: '.profile__share',
          translateX: [300, 0],
          opacity: [0,1],          
          duration: 2000,
          offset: 0,
          delay (el, i, l) {
            return 50 * i;
          }
        })
      ;

    },
    
    confirmAnswer (question, answer) {
      const vm = this;

      question.userAnswer = answer;

      setTimeout( () => {
        vm.updateScore(answer);
        vm.step += 1;
        vm.profileResult = findMostCommonItem(vm.answers);
        vm.slideAnimOut(vm.goNext);
      }, 500); 

    },

    fadeOutIntro () {
      const vm = this;
      const timeline = anime.timeline();

      timeline
        .add({
          targets: '.test__intro .animate',
          translateY: '-50px',
          opacity: 0,
          duration: 1000,
          delay (el, i, l) {
            return 100 * i;
          }
        })
        .add({
          targets: '.test__intro',
          opacity: 0,
          duration: 1000,
          delay: 0,
          offset: '-=1000',
          complete () {
            vm.scrollTo('.test', vm.initTest() )
          }
        })
      ;
        

    },

    goNext() {
      const vm = this;        
      if( vm.step !== questions.length) {
        vm.scrollTo('.test__slides', $('.test__slides').slick('slickNext') );
      } else {
        vm.calculateProfile();        
      }
    },

    halfSize(ev) {
      const img = ev.target;
      img.style.width = img.naturalWidth / 2 + 'px';
      img.style.height = img.naturalHeight / 2 + 'px';
    },

    hoverAnswerHandler (ev, id) {
      this.hoverAnswer = id;
      this.setCursor(ev);
    },

    initTest () {
      const vm = this;

      vm.stage = 1;

      Vue.nextTick(function () {

        $('.test__slides')
          .on('init', function (event, slick) {
            vm.slideAnimIn();

          })
          .slick({
            dots: false,
            infinite: false,
            arrows: false,
            draggable: false,
            swipe: false,
            touchMove: false,
            lazyLoad: false,
            // adaptiveHeight: true
          })
          .on('afterChange', function (event,slick,currentSlide) {
            const thisSlide = slick.$slides[currentSlide];
            vm.slideAnimIn();
          })
        ;
      });

    },

    scrollTo(el, callback) {
      $('html, body').animate({scrollTop: $(el).offset().top - 50}, 400, 'swing', callback);
    }, 

    slideAnimIn (callback) {

      const timeline = anime.timeline({
        easing: 'easeInOutQuart'
      });

      timeline
        .add({
          targets: '.slick-active .test__question-title',
          translateY: [100, 0],
          opacity: [0, 1]
        })
        .add({
          targets: '.slick-active .test__thumb',
          translateY: [100, 0],
          opacity: [0, 1],
          duration: 1000,
          offset: 200,
          delay (el, i, l) {
            return 150 * i;
          }
        })
        .add({
          targets: '.slick-active .test__answers h5',
          translateX: [100, 0],
          opacity: [0, 1],
          duration: 1000,
          offset: 400, 
          delay (el, i, l) {
            return 100 * i;
          }
        })
        .add({
          targets: '.slick-active .test__wrap-button',
          translateX: [100, 0],
          opacity: [0, 1],
          duration: 1000,
          offset: 600,
          complete() {
            callback ? callback() : null;
          }
        })
      ;
    },

    slideAnimOut (callback) {
      const timeline = anime.timeline({
        easing: 'easeInOutQuart',
      });

      timeline
        .add({
          targets: '.slick-active .test__question-title',
          translateX: -100,
          opacity: 0,
        })
        .add({
          targets: '.slick-active .test__thumb',
          translateX: -100,
          opacity: 0,
          offset: 100,
          delay (el, i, l) {
            return 150 * i;
          }
        })
        .add({
          targets: '.slick-active .test__answers h5',
          translateX: -100,
          opacity: 0,
          offset: 200, 
          delay (el, i, l) {
            return 50 * i;
          }
        })
        .add({
          targets: '.slick-active .test__wrap-button',
          translateX: -100,
          opacity: 0,
          offset: 300,
          complete() {
            callback ? callback() : null;
          }
        })
      ;
    },


    setCursor (ev) {
      const stage = document.querySelector('.test__stage'),
            thumbLeft = parseInt(ev.currentTarget.style.left);      
      this.cursorX = Math.round( (stage.clientWidth / 100) * thumbLeft ) + 40;      
    },

    updateScore (answer) {
      this.answers.push(answer)
    }
  },

  mounted () {

    const vm = this;
    
    anime({
      targets: '.test__intro .animate',
      translateY: [100, 0],
      opacity: [0, 1],
      easing: 'easeInOutQuart',
      duration: 1000,
      delay (el, i, l) {
        return 50 * i;
      }
    });

    const checkDevice = _.debounce( () => {
      window.matchMedia('(max-width: 991px)').matches ? 
        vm.isMobile = true :
        vm.isMobile = false;

    }, 500);

    $(window).on('resize', checkDevice).resize();


      
  }
});