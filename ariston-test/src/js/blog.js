


/* ---------------------------------------------------------
  Correlati
----------------------------------------------------------*/
function correlati () {

  function initSlider () {
    $('.related .slider').slick({
      mobileFirst: true,
      dots: true,
      responsive: [
        {
          breakpoint: 768,
          settings: 'unslick'
        }
      ]
    });
  }
    
  const resizeHandler = _.debounce( () => {
    const isMobile = window.matchMedia('(max-width: 767px)').matches,
          slickInitialized = $('.related .slider').hasClass('slick-initialized');

    if( isMobile && !slickInitialized ) {
      initSlider();
    }
  }, 500);

  $(window).resize(resizeHandler).resize();

}



$(function () {
  correlati();
});