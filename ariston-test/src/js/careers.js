


/* ---------------------------------------------------------
  Correlati
----------------------------------------------------------*/
function storie () {

  $('.storie__slider').slick({
    mobileFirst: true,
    dots: true,
    infinite: false,
    responsive: [
      {
        arrows: true,
        breakpoint: 576,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3
        }
      }
    ]
  });

}



$(function () {
  storie();
});