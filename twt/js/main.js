'use strict';

/* --------------------------------------------
  FX
---------------------------------------------*/
function fx() {
  // var rellax = new Rellax('.rellax');
  $('.reveal').each(function (i, e) {
    $(e).wrap('<div class="reveal-wrap"></div>');
  });

  window.sr = ScrollReveal({
    distance: '110%',
    duration: 1000,
    // reset: true,
    scale: 1,
    opacity: 1,
    viewFactor: 0.5
  });
  sr.reveal('.reveal');
  sr.reveal('.reveal-right', {
    origin: 'right'
  }, 100);
}

/* --------------------------------------------
  Main nav
---------------------------------------------*/
function mainNav() {

  new Vue({
    el: '.main-nav',
    computed: {
      activePage: function activePage() {
        return location.search.substr(1);
      }
    }
  });
}

/* --------------------------------------------
  Hero
---------------------------------------------*/
function hero() {

  Vue.component('hero-slide', {
    data: function data() {
      return {
        isPlaying: false
      };
    },

    props: ['currentSlide'],
    computed: {
      slideIndex: function slideIndex() {
        return this.currentSlide;
      }
    },
    watch: {
      slideIndex: function slideIndex() {
        this.stop();
      }
    },
    methods: {
      playPause: function playPause($event) {
        var video = $('video', this.$el)[0];
        $event.preventDefault();
        video.paused ? video.play() : video.pause();
        this.isPlaying = !this.isPlaying;
      },
      stop: function stop() {
        var video = $('video', this.$el)[0];
        this.isPlaying = false;
        video.load();
      }
    },
    mounted: function mounted() {
      // console.log(this.currentSlide);
    }
  });

  new Vue({
    el: '.hero',
    data: {
      currentSlide: 0
    },
    mounted: function mounted() {
      var vm = this;
      $('.hero .slider').on('init', function (a, b) {
        // Nascondi punti se c'è una sola slide
        $('.hero .slider .slick-slide').length === 1 ? $('.hero .slider .slick-dots').remove() : null;
      }).slick({
        dots: true,
        arrows: false,
        fade: true
        // lazyLoad: 'ondemand',
      }).on('afterChange', function (event, slick) {
        vm.currentSlide = slick.currentSlide;
      });
    }
  });
}

/* --------------------------------------------
  Video overflow
---------------------------------------------*/
function videoOverflow() {

  new Vue({
    el: '.video-overflow',
    data: {
      isPlaying: false,
      video: {
        width: 0
      }
    },
    methods: {
      playPause: function playPause($event) {
        var video = $event.target;
        this.isPlaying = !this.isPlaying;
        video.paused ? video.play() : video.pause();
      },
      ended: function ended() {
        this.isPlaying = false;
      }
    },
    mounted: function mounted() {
      var vm = this;
      vm.$nextTick(function () {
        var debounced = _.debounce(function () {
          $('.video-overflow').each(function (i, e) {
            var overflowWidth = ($(window).innerWidth() - $('.container').width()) / 2;

            // Set video size
            $('video', e).width(function () {
              return 'calc(100% + ' + overflowWidth + 'px)';
            }());

            // Set play button position
            $('.video-overflow__play', e).css('left', 'calc(50% - (50px + ' + overflowWidth / 2 + 'px))');
          });
        }, 200);

        $(window).resize(debounced).resize();
      });
    }
  });
}

/* --------------------------------------------
  Fullslider
---------------------------------------------*/
function fullslider() {
  $('.fullslider .slider').slick({
    arrows: false,
    autoplay: true,
    dots: true,
    centerMode: true,
    variableWidth: true,
    responsive: [{
      breakpoint: 576,
      settings: {
        centerMode: false,
        variableWidth: false
      }
    }]
  });
}

/* --------------------------------------------
  DOC READY
---------------------------------------------*/
$(function () {
  fx();
  mainNav();
  fullslider();

  if ($('[data-page="home"]').length || $('[data-page="chisiamo"]').length) {
    hero();
  }

  if ($('[data-page="home"]').length) {
    videoOverflow();
  }
});