'use strict';

function previewPages() {
  var pageName = location.search.slice(1);
  $('[data-page]').not('[data-page="' + pageName + '"]').remove();
  document.body.className = pageName;
}

/* DOC READY */
$(function () {
  previewPages();
});

new Vue({
  el: '.hexagons',
  data: {
    items: [{
      id: 'soluzioni',
      label: 'soluzioni su misura'
    }, {
      id: 'consulenza',
      label: 'consulenza b2b/b2c'
    }, {
      id: '4life',
      label: 'sistema 4life'
    }]
  }
});

new Vue({
  el: '.chisiamo__eccellenza',
  data: {
    items: ['garniga', 'rekord', 'zuani']
  }
});