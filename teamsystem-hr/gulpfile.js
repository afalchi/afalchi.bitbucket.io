/*

  gulp dev          start server, browsersync
  gulp copy         [manually add new dependency to the task]  




*/






var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var pkg = require('./package.json');
var sourcemaps = require('gulp-sourcemaps');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');

// Set the banner content
var banner = ['/*!\n',
  ' * Start Bootstrap - <%= pkg.title %> v<%= pkg.version %> (<%= pkg.homepage %>)\n',
  ' * Copyright 2013-' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
  ' * Licensed under <%= pkg.license %> (https://github.com/BlackrockDigital/<%= pkg.name %>/blob/master/LICENSE)\n',
  ' */\n',
  ''
].join('');

// Compiles SCSS files from /scss into /css
gulp.task('sass', function() {
	return gulp.src('people/scss/main.scss')
		.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    //.pipe(header(banner, {   pkg: pkg		}))
		.pipe(postcss([ autoprefixer() ]))
		.pipe(sourcemaps.write('./maps')) // Output source in separate file
    .pipe(gulp.dest('people/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
  ;
});

// Compiles SCSS Bootstrap files from /scss into /css
gulp.task('sass-bootstrap', function() {
	return gulp.src('people/vendor/bootstrap/custom/scss/bootstrap.scss')
		.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
		.pipe(postcss([ autoprefixer() ]))
		.pipe(sourcemaps.write('./maps')) // Output source in separate file
    .pipe(gulp.dest('people/vendor/bootstrap/custom/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
  ;
});


// Minify compiled CSS
gulp.task('minify-css', ['sass'], function() {
  return gulp.src('people/css/main.css')
    .pipe(cleanCSS({
      compatibility: 'ie10'
    }))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('people/css'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

// Minify compiled Bootstrap CSS
gulp.task('minify-custom-bootstrap-css', ['sass-bootstrap'], function() {
  return gulp.src('people/vendor/bootstrap/custom/css/bootstrap.css')
    .pipe(cleanCSS({
      compatibility: 'ie10'
    }))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('people/vendor/bootstrap/custom/css'))
    .pipe(browserSync.reload({
      stream: true
    }));
});



// Minify custom JS
gulp.task('minify-js', function() {
  return gulp.src('people/js/main.js')
    .pipe(uglify())
    // .pipe(header(banner, { pkg: pkg }))
    .pipe(rename({ 
      suffix: '.min'
    }))
    .pipe(gulp.dest('people/js'))
    .pipe(browserSync.reload({
      stream: true 
    }))
});

// Copy vendor files from /node_modules into /vendor
// NOTE: requires `npm install` before running!
gulp.task('copy', function() {

  gulp.src([
      'node_modules/bootstrap/dist/**/*',
      '!**/npm.js',
      '!**/bootstrap-theme.*',
      '!**/*.map'
    ]).pipe(gulp.dest('people/vendor/bootstrap'));

  gulp.src(['node_modules/jquery/dist/jquery.js', 'node_modules/jquery/dist/jquery.min.js'])
    .pipe(gulp.dest('people/vendor/jquery'));

  gulp.src(['node_modules/popper.js/dist/umd/popper.js', 'node_modules/popper.js/dist/umd/popper.min.js'])
    .pipe(gulp.dest('people/vendor/popper'));

  gulp.src(['node_modules/jquery.easing/*.js'])
    .pipe(gulp.dest('people/vendor/jquery-easing'));

  gulp.src([
      'node_modules/font-awesome/**',
      '!node_modules/font-awesome/**/*.map',
      '!node_modules/font-awesome/.npmignore',
      '!node_modules/font-awesome/*.txt',
      '!node_modules/font-awesome/*.md',
      '!node_modules/font-awesome/*.json'
    ])
    .pipe(gulp.dest('people/vendor/font-awesome'));

	gulp.src(['node_modules/vue/dist/*.js'])
    .pipe(gulp.dest('people/vendor/vue'));

  gulp.src(['node_modules/velocity-animate/*.js'])
    .pipe(gulp.dest('people/vendor/velocity-animate'));

  gulp.src(['node_modules/lodash/lodash.min.js'])
    .pipe(gulp.dest('people/vendor/lodash'));  

  gulp.src(['node_modules/open-iconic/font/**'])
    .pipe(gulp.dest('people/vendor/open-iconic'));  

  gulp.src(['node_modules/stickyfilljs/dist/*.js'])
    .pipe(gulp.dest('people/vendor/stickyfilljs'));

});


// Copy Bootstrap SCSS
// Bootstrap SASS  
gulp.task('copy-bootstrap-sass', function() {
  gulp.src(['node_modules/bootstrap/scss/**/*'])
    .pipe(gulp.dest('people/vendor/bootstrap/scss'));
});


// Default task
gulp.task('default', [
  'sass', 'minify-css', 
  // 'minify-js', 
  'copy', 'sass-bootstrap', 'minify-custom-bootstrap-css']);


// Configure the browserSync task
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: ''
    },
  })
});

// Dev task with browserSync
gulp.task('dev', [
  'browserSync',  
  'sass',   
  'minify-css', 
  'sass-bootstrap',
  'minify-custom-bootstrap-css'
  // 'minify-js'
], 
  function() {
    gulp.watch('people/scss/*.scss', ['sass']);
    gulp.watch('people/vendor/bootstrap/custom/scss/*.scss', ['sass-bootstrap']);
    gulp.watch('people/css/*.css', ['minify-css']);
    // gulp.watch('js/*.js', ['minify-js']);
    // Reloads the browser whenever HTML or JS files change
    gulp.watch('*.html', browserSync.reload);
    gulp.watch('people/js/**/*.js', browserSync.reload);
  }
);
