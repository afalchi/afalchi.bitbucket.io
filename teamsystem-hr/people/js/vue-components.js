
Vue.component('ts-icon', {
  template: '\
    <svg version="1.1" style="vertical-align: middle;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" :width="size" :height="size" x="0px" y="0px" viewBox="0 0 18 18" xml:space="preserve">\
      <g v-if="symbol === \'x\'">\
        <g>\
          <line :style="\'fill:none; stroke:\' + color + \';stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\'" x1="15.4" y1="2.6" x2="9" y2="9"/>\
          <line :style="\'fill:none; stroke:\' + color + \';stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\'" x1="15.4" y1="15.4" x2="9" y2="9"/>\
        </g>\
        <g>\
          <line :style="\'fill:none; stroke:\' + color + \';stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\'" x1="2.6" y1="2.6" x2="9" y2="9"/>\
          <line :style="\'fill:none; stroke:\' + color + \';stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\'" x1="2.6" y1="15.4" x2="9" y2="9"/>\
        </g>\
      </g>\
      <g v-if="symbol === \'next\'">\
        <line :style="\'fill:none; stroke:\' + color + \';stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\'" x1="5.8" y1="2.6" x2="12.2" y2="9"/>\
        <line :style="\'fill:none; stroke:\' + color + \';stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\'" x1="5.8" y1="15.4" x2="12.2" y2="9"/>\
      </g>\
      <g v-if="symbol === \'prev\'">\
        <line :style="\'fill:none;stroke:\' + color + \';stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\'" x1="12.2" y1="2.6" x2="5.8" y2="9"/>\
        <line :style="\'fill:none;stroke:\' + color + \';stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\'" x1="12.2" y1="15.4" x2="5.8" y2="9"/>\
      </g>\
    </svg>',
  props: {
    color: {
      type: String,
      default: 'white'
    },
    symbol: {
      type: String,
      required: true
    },
    size: {
      type: String,
      default: '14'
    }
  }
});

