'use strict';

(function () {

  // Person Class
  var Person = function() {
    this.name = { first: '',  last: '' };
    this.picture = { large: '', thumb: '' };
    this.id = { value: '' };
    this.type = '';
    this.size = 1;
  };


  /* -----------------------------------------
  People dettaglio
  ------------------------------------------*/ 
  var facemosaic = new Vue({

    el: '.facemosaic',


    data: {    
      detailVisible: false,
      mosaic: {
        cols: 3,
        hasSomeBig: false, 
        hasSomeEmpty: true,
        isHover: false,
        numpeople: 32
      },    
      people: [],        
      selectedPerson: new Person(),
      slide: {
        auto: false,
        counter: 0,
        delay: 4000,      
        isPaused: false,
        rowsToSlide: 1
      },
      
      tileSize: 0 // Valorizzata all'evento mounted
    },


    components: {
      facemosaicDetail: {},
      facemosaicPerson: {
        template: '\
          <div class="facemosaic__person" :class="{\'empty\': person.type === \'empty\'}" :style="\'width:\' + tileSize * person.size + \'px; height:\' + tileSize * person.size + \'px\'"> \
            <a href="" \
              v-if="person.type !== \'empty\'" \
             :title="person.name.first + \' \' + person.name.last" \
              @click.prevent="clickHandler" \
              @touchstart="stopAutoplay"> \
              <div class="cta">{{ cta }}</div>\
              <img :src="person.picture.large" :style="\'width:\' + tileSize * person.size + \'px; height:\' + tileSize * person.size + \'px\'" alt=""> \
            </a>\
          </div>',
        props: ['people', 'person', 'index', 'stopAutoplay', 'startAutoplay', 'tileSize', 'cta'],
        methods: {
          clickHandler: function () { 
            this.$emit('opendetail', this.person);
          }
        }     
      }
    },


    computed: {
    },  


    filters: {
      capitalize: function (string) {
        if (string) {
          return string[0].toUpperCase() + string.slice(1);
        }
      }
    },


    methods: {
      animateScrollWrapper: function () {
        var vm = this;
        var wrapper = this.$el.querySelector('.facemosaic__wrapper');
        var scroller = this.$el.querySelector('.facemosaic__scroller');

        function animate() {
          $(wrapper).animate({scrollTop: vm.tileSize * vm.slide.rowsToSlide * vm.slide.counter}, 800);
        }

        var interval = setInterval(function () {
          var scrollCondition = scroller.clientHeight > (wrapper.clientHeight + wrapper.scrollTop);
          if (scrollCondition && !vm.slide.isPaused && !vm.detailVisible) {    
            animate();
            vm.slide.counter += 1;                                 
          } else if (!scrollCondition && !vm.slide.isPaused && !vm.detailVisible) {
            animate();
            vm.slide.counter = 0; 
          } 
          
        }, vm.slide.delay);
      },    
      
      loadProfiles: function () {
        var vm = this;
        
        // Aggiunge proprietà "size"
        function addPeopleSize () {
          vm.people.map(function (item) {
            return item.size = 1;
          });
        }

        // Rende più grandi alcuni profili a caso
        function makeSomeBig () {
          vm.people.map(function (item) {
            var rndm = Math.round(Math.random()*2);
            if (rndm === 0) {
              return item.size = 1; 
            } else {
              return item.size = 2;
            }
          });
        }

        // Aggiunge dei blocchi vuoti
        function makeSomeEmpty () {
          var emptyTile = new Person();
          emptyTile.type = 'empty';
          emptyTile.size = 1;

          vm.people.forEach(function (item, index) {
            var rndm = Math.round(Math.random()*4);
            if (rndm === 0) {
              vm.people.splice(index+1, 0, emptyTile);
            } 
          });
        }
        
        var people = $.get('https://randomuser.me/api/?inc=name,picture&results=' + this.mosaic.numpeople + '&inc=id,location,nat,name,picture,age')
          .done(function (response) {
            vm.people = response.results;                
            addPeopleSize();
            vm.mosaic.hasSomeBig ? makeSomeBig() : null;
            vm.mosaic.hasSomeEmpty ? makeSomeEmpty() : null;

            
          })
          .fail(function() {
            console.log('Error loading data!');
          }) 
          ;
      },

      setSelectedPerson: function (ev) {
        this.detailVisible = true;
        this.selectedPerson = ev;
        this.slide.isPaused = true;
      },
      stopAutoplay: function () { this.slide.isPaused = true; },
      startAutoplay: function () {this.slide.isPaused = false; },
      resizeHandler: function () {
        this.tileSize = Math.floor(this.$el.clientWidth / this.mosaic.cols);
        this.slide.counter = 0;
      }
    },


    mounted: function () {
      var vm = this;
      vm.loadProfiles();
      
      vm.$nextTick(function() {
        window.addEventListener('resize', _.debounce(vm.resizeHandler, 500));

        // Updates scroll counter after user scroll
        vm.$el.querySelector('.facemosaic__wrapper').addEventListener('scroll', function () {
          /*
          console.table({
            counter: vm.slide.counter,
            scrll: this.scrollTop,
            num: Math.ceil(this.scrollTop / vm.tileSize)
          }) */
          if ( vm.slide.isPaused ) {
            vm.slide.counter = Math.ceil(this.scrollTop / vm.tileSize);
          }          
        });

        // Init
        this.resizeHandler();
      });
      if (vm.slide.auto) { vm.animateScrollWrapper();}
    }


  });


}())