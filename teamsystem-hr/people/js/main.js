


var percorsi = new Vue({
  el: '.percorsi__tabs',
  data: {
    selectedItem: '',
    isDetailOpen: false,
    programs: [
      {
        id: 'graduate',
        label: 'Graduate Recruitment Program'
      },
      {
        id: 'internship',
        label: 'Internship Program'
      },
      {
        id: 'erp',
        label: 'ERP Academy'
      }
    ]
  },
  methods: {
    showDetail: function (itemID) {
      $('.percorsi__tabs__header').slideUp();
      $('[data-program="' + itemID + '"]').slideToggle();
      this.isDetailOpen = true;
    },
    closeDetail: function () {
      $('.percorsi__tabs__header').slideDown();
      $('[data-program]').slideUp();
      this.isDetailOpen = false;
    }
  },
  mounted: function () {

  }
}); 



function initWow () {
  new WOW().init();
}


function sticky () {
  var stickyElements = document.querySelectorAll('.sticky');  
  for (var i = stickyElements.length - 1; i >= 0; i--) {
    Stickyfill.add(stickyElements[i]);
  }
}


var eventi = {
  init: function() {
    $('.eventi__slider')
      .on('init', function () {
        $('.eventi__slider__desc--visible').html( $('.slick-active .eventi__slider__desc').html() )
      })
      .slick({
        arrows: true,
        dots: false,
        infinite: true,
        // autoplay: true,
        speed: 2000,
        autoplaySpeed: 4000,
        swipeToSlide: true
      })
      .on('beforeChange', function () {
        $('.eventi__slider__desc').fadeOut('fast');
      })
      .on('afterChange', function (event, slick, currentSlide) {
        $('.eventi__slider__desc--visible')
          .html( $('.slick-active .eventi__slider__desc').html() )
          .fadeIn('fast')
        ;
      })
    ;
  }
};



// DOC READY 
$(function () {

  initWow();
  sticky();
  eventi.init();

}); 