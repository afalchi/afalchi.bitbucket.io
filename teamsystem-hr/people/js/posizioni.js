

/* -----------------------------------------
 Posizioni
------------------------------------------*/ 
var posizioni = new Vue({
  el: '[data-page="posizioni"]',
  data: {
    people: [],
    numPeople: 18,
    locations: [ 'Pesaro', 'Ancona', 'Milano', 'Roma'],   // Per mockup
    filter: {
      selectedLocation: 'all',
      locations: ['all']
    },
    animation: {
      delay: 50
    }
  },
  computed: {

    // Filtra lista risultati
    filteredList: function filteredList() {
      var vm = this;
      return vm.positions.filter(function (item) {
        if (vm.filter.selectedLocation === 'all') {
          return item;
        } else {
          return item.location.toLowerCase() === vm.filter.selectedLocation.toLowerCase().trim();
        }
      });
    },

    // Mockup json posizioni
    positions: function () { 
      
      var vm = this;    
      var posizioni = [];
      
      var areas = [
        {
          label: 'Factory',
          id: 'factory'
        },
        {
          label:'Cloud & new Business',
          id: 'cloud-newbusiness'
        },
        {
          label: 'Sales & delivery',
          id: 'sales-delivery'
        },
        {
          label: 'Customer value & support',
          id: 'customers'
        },
        {
          label: 'Vertical',
          id: 'vertical'
        },
        {
          label: 'Staff',
          id: 'staff'
        }
      ];
    
    
      for (var i=0; i<20; i++) {
        posizioni.push(
          {
            id: i,
            dept: areas[Math.floor(Math.random () * areas.length)],
            location: vm.locations[Math.floor(Math.random () * vm.locations.length)]
          }
        );
      } 
      
      
      return posizioni;
      
    }

  },
  filters: {
    capitalize: function (string) {
      return string[0].toUpperCase() + string.slice(1);
    }
  },
  methods: {
    getPeople: function getPeople() {
      var vm = this;
      $.get('https://randomuser.me/api/?results=' + this.numPeople + '&inc=id,location,nat,name,picture')
        .done(function (response) {
          
          // Salva risultati
          vm.people = response.results;

          // Crea array con una chiave a scelta per il filtro
          vm.people.forEach( function (item, index) {
            if(vm.filter.name.title.indexOf(vm.people[index].name.title) === -1) {              
              vm.filter.keys.push(item.name.title);
            };
          });
      });
    },
    checkProp: function checkProp(obj, prop) {
      return obj[prop].toLowerCase() !== this.filter[prop].toLowerCase().trim();
    },
    beforeEnter: function beforeEnter(el) {
      el.style.opacity = 0;
      el.style.height = 0;
    },

    enter: function enter(el, done) {
      var delay = el.dataset.index * this.animation.delay;
      setTimeout(function () {
        Velocity(el, { opacity: 1, height: '95px' }, { complete: done });
      }, delay);
    },
    leave: function leave(el, done) {
      var delay = el.dataset.index * this.animation.delay;
      setTimeout(function () {
        Velocity(el, { opacity: 0, height: 0 }, { complete: done });
      }, delay);
    }
  },
  mounted: function mounted() {
    // this.getPeople();
  }
});