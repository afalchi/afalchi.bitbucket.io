function previewPages () {
  var pageName = location.search.slice(1)
  $('[data-page]').not('[data-page="' + pageName + '"]').remove();
  document.body.className = pageName;
}



/* DOC READY */
$( function () {
  previewPages();
});


// Footer
new Vue({
  el: '.main-footer',
  data: {
    divisions: ['teamsystem', 'nuovamacut', 'danea', 'humus']
  }
});


// Home
new Vue({
  el: '[data-page="home"]',
  data: {
    links: [
      {
        label: 'people',
        id: 'people'
      },
      {
        label:'percorsi',
        id: 'percorsi'
      },
      {
        label: 'tslife',
        id: 'ts-life'
      },
      {
        label: 'people',
        id: 'people'
      },
      {
        label: 'posizioni aperte',
        id: 'posizioni'
      }
    ]
  }
});


// People
new Vue({
  el: '[data-page="people"]',
  data: {
    areas: [
      {
        label: 'factory',
        id: 'factory'
      },
      {
        label:'cloud & new Business',
        id: 'cloud-newbusiness'
      },
      {
        label: 'sales & delivery',
        id: 'sales-delivery'
      },
      {
        label: 'Customer value & support',
        id: 'customers'
      },
      {
        label: 'vertical',
        id: 'vertical'
      },
      {
        label: 'staff',
        id: 'staff'
      }
    ]
  },
  filters: {
    capitalize: function (string) {
      if (string) {
        return string[0].toUpperCase() + string.slice(1);
      }
    }
  },
});

// Eventi
new Vue({
  el: '[data-page="eventi"]'
});




// Eventi
new Vue({
  el: '[data-page="eventi"]'
});


// TS Life
new Vue({
  el: '[data-page="tslife"]'
});


// Styleguide
new Vue({
  el: '[data-page="styleguide"]'
});
