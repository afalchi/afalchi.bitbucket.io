'use strict';

{

  $(function () {

    $('.dl__menu button').click(function () {
      var target = document.querySelector(this.dataset.target);
      anime({
        targets: "html, body",
        scrollTop: [window.pageYOffset, target.getBoundingClientRect().top + window.scrollY - 100],
        duration: 800,
        easing: 'easeOutQuad'
      });
    });
  });
}
//# sourceMappingURL=downloads.js.map
