'use strict';

(function () {

  /* --------------------------------------
    Scroll Reveal
  ---------------------------------------*/
  function revealFX() {

    scrollReveal.reveal('[data-reveal]', {
      distance: '20px',
      duration: 1000,
      origin: 'right',
      scale: 1,
      viewFactor: 0.7
    }, 100);

    return;

    // Spessori
    scrollReveal.reveal('.fascia-spessori [data-reveal]', {
      distance: '20px',
      duration: 1000,
      origin: 'right',
      scale: 1,
      viewFactor: 0.7
    }, 50);

    // Spessori
    scrollReveal.reveal('.fascia-progetto [data-reveal]', {
      distance: '20px',
      duration: 1000,
      origin: 'left',
      scale: 1,
      viewFactor: 0.7
    }, 50);
  }

  /* --------------------------------------
    DOC READY
  ---------------------------------------*/
  $(function () {
    // revealFX();    
  });

  /* --------------------------------------
    WIN LOAD
  ---------------------------------------*/
  $(function () {
    // revealFX();    
  });
})();
//# sourceMappingURL=home.js.map
