'use strict';

{

  $(function () {

    /* --------------------------------------
    Scroll Reveal
    ---------------------------------------*/
    var scrollReveal = ScrollReveal();
    scrollReveal.reveal('.prod__intro [data-reveal]', {
      distance: '20px',
      duration: 1000,
      origin: 'right',
      scale: 1,
      viewFactor: 0.7
    }, 50);
  });
}
//# sourceMappingURL=prodotti.js.map
