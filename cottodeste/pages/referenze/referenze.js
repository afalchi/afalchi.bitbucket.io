'use strict';

{
  var filters = function filters() {

    // Voce selezione
    Vue.component('filter-btn', {
      data: function data() {
        return {
          selected: false
        };
      },

      methods: {
        toggle: function toggle() {
          this.selected = !this.selected;
        }
      },
      mounted: function mounted() {}
    });

    // Gruppo di filtri
    Vue.component('filter-group', {
      data: function data() {
        return {
          open: this.initiallyOpen
        };
      },

      methods: {
        close: function close() {
          this.open = false;
          $('.filter-types', this.$el).stop().slideUp();
        },
        toggle: function toggle() {
          this.open = this.open === null ? true : !this.open;
          $('.filter-types', this.$el).stop().slideToggle();
        }
      },
      props: {
        initiallyOpen: {
          type: Boolean,
          required: true
        },
        mobileOpen: {
          type: Boolean,
          required: true
        }
      },
      watch: {
        // Chiude le voci sottostanti quando si chiude anche
        // tutto il pannello dei filtri mobile
        mobileOpen: function mobileOpen(value) {
          if (value) {
            this.close();
          }
        }
      },
      mounted: function mounted() {}
    });

    // App filtri
    new Vue({
      el: '.prod__listing .filters',
      data: {
        isMobile: null,
        mobileOpen: false
      },
      computed: {},
      methods: {
        mobileToggle: function mobileToggle() {
          this.mobileOpen = !this.mobileOpen;
          $('.mobile-collapse', this.$el).stop().slideToggle();
        }
      },
      mounted: function mounted() {
        var vm = this;
        var checkWidth = _.debounce(function () {
          $(window).width() > 768 ? vm.isMobile = false : vm.isMobile = true;
        }, 200);

        $(window).resize(checkWidth).resize();
      }
    });
  };

  var thumbs = function thumbs() {
    $('.prod__listing .tile').each(function (i, e) {
      var $targets = $('.abstract, .cta', e);
      $(e).hover(function () {
        $targets.slideDown();
      }, function () {
        $targets.slideUp();
      });
    });
  };

  /* -----------------------------
    DOC READY
  ----------------------------- */


  $(function () {
    filters();
    thumbs();
  });
}
//# sourceMappingURL=referenze.js.map
