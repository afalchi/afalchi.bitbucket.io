'use strict';

{

  $(function () {

    // Scroll to section
    $('.faq__menu button').click(function () {
      var target = document.querySelector(this.dataset.target);
      anime({
        targets: "html, body",
        scrollTop: [window.pageYOffset, target.getBoundingClientRect().top + window.scrollY - 100],
        duration: 800,
        easing: 'easeOutQuad'
      });
    });

    // Accordion
    $('.faq__item').each(function (i, e) {
      $('.faq__header', e).click(function () {
        $('.faq__body', e).stop().slideToggle();
        $(e).toggleClass('faq__item--open');
      });
    });

    // Feedback
    $('.feedback').each(function (i, e) {
      $('[data-answer]', e).click(function () {
        var response = this.dataset.answer;
        $('.feedback__question', e).slideUp('fast', function () {
          $('.feedback__response--' + response, e).slideDown();
        });
      });
    });
  });
}
//# sourceMappingURL=faq.js.map
