'use strict';

{

  /* -----------------------------
    Finiture
  ----------------------------- */
  var scrollReveal = function scrollReveal() {};

  /* -----------------------------
    Overlay gallery
  ----------------------------- */


  var overlayGallery = function overlayGallery() {

    var $slider = $('.scheda__gallery .slider');

    $slider.on('init', function (ev, slider) {}).slick({
      appendDots: '.scheda__gallery .gallery-dots',
      slidesToShow: 1,
      arrows: false,
      dots: true,
      dotsClass: 'slick-dots slick-dots--inverted',
      infinite: false,
      mobileFirst: true,
      responsive: [{
        breakpoint: 992,
        settings: {
          rtl: true,
          variableWidth: true
        }
      }]
    });

    // Nav
    $('.scheda__gallery .slick-prev').click(function (ev) {
      ev.preventDefault();
      $slider.slick('slickNext');
    });
    $('.scheda__gallery .slick-next').click(function (ev) {
      ev.preventDefault();
      $slider.slick('slickPrev');
    });

    // Aggiunta attributo dir per desktop
    function setDirection() {
      window.innerWidth > 991 ? $slider.attr('dir', 'rtl') : $slider.attr('dir', 'ltr');
    }

    var resizeHandler = _.debounce(function () {
      setDirection();
    }, 200);

    window.addEventListener('resize', resizeHandler);
    setDirection();
  };

  /* -----------------------------
    Finiture
  ----------------------------- */


  var finiture = function finiture() {};

  /* -----------------------------
    DOC READY
  ----------------------------- */


  $(function () {

    scrollReveal();
    overlayGallery();
    finiture();
  });
}
//# sourceMappingURL=contatti.js.map
