'use strict';

(function () {

  /* --------------------------------------
    Scroll Reveal
  ---------------------------------------*/
  function revealFX() {
    window.scrollReveal = new ScrollReveal({
      distance: '20px',
      duration: 1000,
      origin: 'right',
      reset: false,
      scale: 1,
      viewFactor: 0.7
    });

    scrollReveal.reveal('[data-reveal]', {
      interval: 100,
      origin: 'right'
    });

    scrollReveal.reveal('[data-reveal-left]', {
      interval: 100,
      origin: 'left'
    });

    scrollReveal.reveal('[data-reveal-bottom]', {
      origin: 'bottom'
    });

    scrollReveal.reveal('[data-reveal-zoom]', {
      distance: 0,
      origin: 'bottom',
      scale: .8
    });
  }

  /* --------------------------------------
    DOC READY
  ---------------------------------------*/
  $(function () {
    // revealFX();    
  });

  /* --------------------------------------
    WIN LOAD
  ---------------------------------------*/
  window.addEventListener('load', function () {

    // Inizializzazione di ScrollReveal
    // dentro setTimeout a causa di un contrasto 
    // con slick slider
    setTimeout(function () {
      revealFX();
    }, 100);
  });
})();
//# sourceMappingURL=main.js.map
