'use strict';

requirejs.config({
  baseUrl: '../components',
  paths: {
    main: '../js/main',
    colorGrid: 'color-grid/color-grid',
    socialize: 'socialize/socialize'
  }
});

require(['main'], function (main) {});
//# sourceMappingURL=config.js.map
