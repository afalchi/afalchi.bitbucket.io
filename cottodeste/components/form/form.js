'use strict';

{

  $(function () {

    /* ----------------------------
      Effetto di movimento label
    ---------------------------- */
    function labelFX() {
      $('.form-group').each(function (i, e) {

        // Campo select
        if (e.classList.contains('fake-select')) {
          var $toggler = $('.dropdown-toggle', e),
              $dropdown = $('.dropdown-menu', e);
          var changed = false;
          $toggler.focus(function () {
            e.classList.add('not-empty');
          }).blur(function () {
            // Attende aggiornamento variabile "change"
            setTimeout(function () {
              !changed ? e.classList.remove('not-empty') : null;
            }, 500);
          });
          $dropdown.find('button').click(function () {
            changed = true;
          });
        }

        // Altri campi
        else {
            $('.form-control', e).focus(function () {
              e.classList.add('not-empty');
            }).blur(function () {
              this.value === '' ? e.classList.remove('not-empty') : null;
            });
          }
      });
    }

    /* ----------------------------
      Custom select
    ---------------------------- */
    function customSelect() {
      $(".form-group .dropdown-menu button").click(function (ev) {
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText).prev('input').val($(this).attr('data-value'));
      });
    }

    /* ----------------------------
      Block checkbox(news)
    ---------------------------- */
    function blockCheckbox() {
      $(".block-checkbox").click(function (ev) {
        this.classList.toggle('block-checkbox--selected');
      });
    }

    /* ----------------------------
      Init
    ---------------------------- */
    labelFX();
    customSelect();
    blockCheckbox();
  });
}
//# sourceMappingURL=form.js.map
