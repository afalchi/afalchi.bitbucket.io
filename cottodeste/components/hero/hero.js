'use strict';

$(function () {

  // Effetto di comparsa del testo
  function setText() {
    $('.hero .slide .title span').each(function (i, e) {
      var text = e.innerHTML.split('').map(function (item) {
        return item !== ' ' ? item : '&nbsp;';
      });
      var newHTML = text.reduce(function (tot, next, index, array) {
        if (index !== array.length) {
          if (next !== '&nbsp;') {
            return tot + '<span class="animejs">' + next + '</span>';
          } else {
            return tot + '</span><span>' + next + '</span><span class="word">';
          }
        } else {
          return tot + '<span class="animejs">' + next + '</span></span>';
        }
      }, '<span class="word">');
      e.innerHTML = newHTML;
    });
  }

  function animIMG(target) {
    anime({
      targets: target,
      scale: [1.05, 1],
      easing: 'easeOutQuad'
    });
  }

  function animText(target) {
    anime({
      targets: target,
      translateY: [50, 0],
      opacity: [0, 1],
      easing: 'easeOutQuad',
      delay: function delay(el, i, l) {
        return i * 10;
      }
    });
  }

  $('.hero .slider').on('init', function (ev, slider) {

    setText();

    // Riattiva immagini nascoste in CSS
    $('.hero .copy, .hero .image').css('opacity', 1);

    animIMG(slider.$slides[0].querySelectorAll('.media'));
    animText(slider.$slides[0].querySelectorAll('.animejs'));

    // Nasconde punti se c'è una sola slide (bug di slick)
    if (slider.options.slidesToShow >= slider.slideCount) {
      $(slider.$slider).css('margin-bottom', 0).find('.slick-dots').remove();
    }

    if (slider.$slides[0].querySelector('video')) {
      var video = slider.$slides[0].querySelector('video'),
          play = video.play;
      video.load();

      video.addEventListener('canplaythrough', function () {
        video.play();
      });
    }
  }).slick({
    // autoplay: true,
    autoplaySpeed: 5000,
    dots: true,
    arrows: false,
    fade: true,
    mobileFirst: true,
    prevArrow: '\n      <button type="button" class="slick-prev">\n      <img src="img/ui/slick-prev-white.svg"  alt="">\n      </button>',
    nextArrow: '\n      <button type="button" class="slick-next">\n      <img src="img/ui/slick-next-white.svg"  alt="">\n      </button>',
    responsive: [{
      breakpoint: 768,
      settings: {
        arrows: true
      }
    }]
  }).on('beforeChange', function (event, slick, current, next) {
    animIMG(slick.$slides[next].querySelectorAll('.media'));
    animText(slick.$slides[next].querySelectorAll('.animejs'));
    try {
      slick.$slides[current].querySelector('video').pause();
    } catch (err) {}
  }).on('afterChange', function (event, slick, current) {
    try {
      $(slick.$slides[current]).find('video')[0].play();
    } catch (err) {}
  });
});
//# sourceMappingURL=hero.js.map
