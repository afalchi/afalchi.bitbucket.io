'use strict';

{
  var doubleGallery = function doubleGallery() {
    var $slider = $('.double-gallery .slider'),
        twoRowsBreakpoint = 1400;

    function slickMobile() {
      $slider.on('init', function (slider) {}).slick({
        appendDots: '.double-gallery__dots',
        dots: true,
        dotsClass: 'slick-dots slick-dots--inverted',
        mobileFirst: true,
        variableWidth: true,
        infinite: false,
        responsive: [{
          breakpoint: twoRowsBreakpoint - 1,
          settings: 'unslick'
        }],
        useCSS: false
      });
    }

    function slickDesktop() {
      $slider.slick({
        appendDots: '.double-gallery__dots',
        dots: true,
        dotsClass: 'slick-dots slick-dots--inverted',
        rows: 2,
        variableWidth: true,
        infinite: false,
        responsive: [{
          breakpoint: twoRowsBreakpoint,
          settings: 'unslick'
        }]
      });
    }

    // Nav
    $('.double-gallery .slick-prev').click(function (ev) {
      ev.preventDefault();
      $slider.slick('slickPrev');
    });
    $('.double-gallery .slick-next').click(function (ev) {
      ev.preventDefault();
      $slider.slick('slickNext');
    });

    // Responsive hack
    // Necessario a causa di problemi con l'opzione "responsive"
    // di Slick
    var resizeHandler = _.debounce(function () {
      if (window.innerWidth > twoRowsBreakpoint) {
        !$slider.is('.slick-slider') ? slickDesktop() : null;
      } else {
        !$slider.is('.slick-slider') ? slickMobile() : null;
      }
    }, 200);

    window.addEventListener('resize', resizeHandler);
    resizeHandler();
  };

  /* -----------------------------
    Inverted gallery
  ----------------------------- */


  var invertedGallery = function invertedGallery() {

    var $slider = $('.inverted-gallery .slider');

    $slider.on('init', function (ev, slider) {}).slick({
      appendDots: '.inverted-gallery .gallery-dots',
      slidesToShow: 1,
      arrows: false,
      dots: true,
      dotsClass: 'slick-dots slick-dots--inverted',
      infinite: false,
      mobileFirst: true,
      responsive: [{
        breakpoint: 992,
        settings: {
          rtl: true,
          variableWidth: true
        }
      }]
    });

    // Nav
    $('.inverted-gallery .slick-prev').click(function (ev) {
      ev.preventDefault();
      $slider.slick('slickNext');
    });
    $('.inverted-gallery .slick-next').click(function (ev) {
      ev.preventDefault();
      $slider.slick('slickPrev');
    });

    // Aggiunta attributo dir per desktop
    function setDirection() {
      window.innerWidth > 991 ? $slider.attr('dir', 'rtl') : $slider.attr('dir', 'ltr');
    }

    var resizeHandler = _.debounce(function () {
      setDirection();
    }, 200);

    window.addEventListener('resize', resizeHandler);
    setDirection();
  };

  /* -----------------------------
    Scroll reveal
  ----------------------------- */


  var revealFX = function revealFX() {
    var scrollReveal = ScrollReveal();
    scrollReveal.reveal('.inverted-gallery', {
      distance: '20px',
      duration: 1000,
      origin: 'right',
      scale: 1,
      viewFactor: 0.7
    }, 50);
  };

  // INIT


  $(function () {
    doubleGallery();
    invertedGallery();
    // revealFX();
  });
}
//# sourceMappingURL=galleries.js.map
