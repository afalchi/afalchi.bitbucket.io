'use strict';

{

  $(function () {

    var $slider = $('.tech-area-related .slider');

    $slider.slick({
      mobileFirst: true,
      slidesToShow: 1,
      responsive: [{
        breakpoint: 768,
        settings: {
          slidesToShow: 2
        }
      }, {
        breakpoint: 992,
        settings: {
          slidesToShow: 3
        }
      }]
    });

    // Nav
    $('.tech-area-related .slick-prev').click(function (ev) {
      ev.preventDefault();
      $slider.slick('slickPrev');
    });
    $('.tech-area-related .slick-next').click(function (ev) {
      ev.preventDefault();
      $slider.slick('slickNext');
    });
  });
}
//# sourceMappingURL=tech-area.js.map
