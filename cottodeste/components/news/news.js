'use strict';

{

  $(function () {

    var $slider = $('.news-preview .slider');

    $slider.slick({
      mobileFirst: true,
      slidesToShow: 1,
      responsive: [{
        breakpoint: 768,
        settings: {
          slidesToShow: 2
        }
      }, {
        breakpoint: 992,
        settings: {
          slidesToShow: 3
        }
      }]
    });

    // Nav
    $('.news-preview .slick-prev').click(function (ev) {
      ev.preventDefault();
      $slider.slick('slickPrev');
    });
    $('.news-preview .slick-next').click(function (ev) {
      ev.preventDefault();
      $slider.slick('slickNext');
    });
  });
}
//# sourceMappingURL=news.js.map
