'use strict';

{
    var filters = function filters() {
        // Voce selezione
        Vue.component('filter-btn', {
            props: {
                id: {
                    type: Number,
                    required: true
                },
                name: {
                    name: String,
                    required: true
                },
                selected: {
                    name: Boolean
                },
                visible: {
                    name: Boolean
                }
            },
            template: '<button :class="{\'selected\': selected}" v-on:click="$emit(\'click\')" v-show="visible"> \n    {{name}}\n    <img src="img/ui/close.svg" class="remove" :class="{\'selected\': selected}">\n</button>',
            mounted: function mounted() {}
        });

        // Gruppo di filtri
        Vue.component('filter-group', {
            props: {
                initiallyOpen: {
                    type: Boolean,
                    default: false
                },
                mobileOpen: {
                    type: Boolean //,
                    //required: true
                },
                name: {
                    type: String,
                    required: true
                },
                filters: {
                    type: Array,
                    required: true
                }
            },
            data: function data() {
                return {
                    open: true
                };
            },

            computed: {
                notEmpty: function notEmpty() {
                    for (var i in this.filters) {
                        if (this.filters[i].visible) return true;
                    }return false;
                }
            },
            methods: {
                close: function close() {
                    this.open = false;
                    $('.filter-types', this.$el).stop().slideUp();
                },
                clickFilter: function clickFilter(item) {
                    item.selected = !item.selected;
                    this.$emit('click', item);
                },
                toggle: function toggle() {
                    this.open = this.open === null ? true : !this.open;
                    $('.filter-types', this.$el).stop().slideToggle();
                }
            },
            template: '<div class="filter-group" v-if="notEmpty">\n    <h5 class="title" @click="toggle">{{name}}\n        <img src="pages/prodotti/img/filter-arrow.svg" class="arrow" alt="" :class="{\'open\': open}">\n    </h5>\n    <div class="filter-types" v-show="initiallyOpen" v-for="item in filters">\n        <filter-btn v-bind="item" v-on:click="clickFilter(item)"></filter-btn>\n    </div>  \n </div>',
            watch: {
                // Chiude le voci sottostanti quando si chiude anche
                // tutto il pannello dei filtri mobile
                mobileOpen: function mobileOpen(value) {
                    if (value) {
                        this.close();
                    }
                }
            },
            mounted: function mounted() {}
        });

        var filtriSelezionati = [];

        // App filtri
        new Vue({
            el: '.listing .filters',
            data: {
                isMobile: $(window).width() <= 768,
                mobileOpen: false,
                data: window.filtriIniziali
            },
            computed: {},
            methods: {
                mobileToggle: function mobileToggle() {
                    this.mobileOpen = !this.mobileOpen;
                    $('.mobile-collapse', this.$el).stop().slideToggle();
                },
                onClick: function onClick(item) {
                    if (item.selected) {
                        filtriSelezionati.push(item.id);
                    } else {
                        var nuoviFiltri = [];
                        for (var i = 0; i < filtriSelezionati.length; i++) {
                            if (filtriSelezionati[i] !== item.id) {
                                nuoviFiltri.push(filtriSelezionati[i]);
                            }
                        }

                        filtriSelezionati = nuoviFiltri;
                    }

                    if (window.filtriModificati) window.filtriModificati(filtriSelezionati);
                }
            },
            mounted: function mounted() {
                var vm = this;
                var checkWidth = _.debounce(function () {
                    $(window).width() > 768 ? vm.isMobile = false : vm.isMobile = true;
                }, 200);

                $(window).resize(checkWidth).resize();

                for (var i = 0; i < this.data.length; i++) {
                    for (var j = 0; j < this.data[i].filters.length; j++) {
                        if (this.data[i].filters[j].selected) {
                            filtriSelezionati.push(this.data[i].filters[j].id);
                        }
                    }
                }

                $(this.$el).css('visibility', '');
            }
        });
    };

    window.thumbsCollezioni = function () {
        $('.listing .tile').each(function (i, e) {
            var $targets = $('.detail, .abstract, .cta', e);
            $(e).hover(function () {
                $targets.slideDown();
            }, function () {
                $targets.slideUp();
            });
        });
    };

    /* -----------------------------
      DOC READY
    ----------------------------- */
    $(function () {
        filters();
        thumbsCollezioni();
    });
}
//# sourceMappingURL=listing.js.map
