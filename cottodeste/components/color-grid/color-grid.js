'use strict';

{

  /* ------------------------------
    Filtri
  ------------------------------ */
  Vue.component('color-filter', {
    data: function data() {
      return {
        active: false
      };
    },

    props: {
      filterKey: {
        type: String,
        required: true
      }
    },
    methods: {
      selected: function selected(ev) {
        this.active = !this.active;
        this.$emit('selected');
      }
    }
  });

  /* ------------------------------
  Color tile
  ------------------------------ */
  Vue.component('color-tile', {
    data: function data() {
      return {
        isSelected: false,
        row: 0
      };
    },

    props: {
      data: {
        type: Object,
        required: true
      },
      index: {
        type: Number,
        required: true
      },
      image: {
        type: String,
        required: true
      },
      isDetailOpen: {
        type: Boolean,
        required: true
      },
      json: {
        type: String,
        required: true
      },
      openId: {
        type: Number,
        required: true
      }
    },
    computed: {
      style: function style() {
        return {
          'order': this.row
        };
      }
    },
    methods: {
      getRow: function getRow() {
        var vm = this;

        // Calcola quanti elementi sono visibili per riga
        var itemsPerRow = function () {
          var flexBasis = window.getComputedStyle(vm.$el).getPropertyValue('flex-basis');
          var result = Math.floor(100 / parseInt(flexBasis));
          return result;
        }();

        this.row = (Math.floor(this.index / itemsPerRow) + 1) * 10;
      },
      closeDetail: function closeDetail(ev) {
        // Se si è cliccato sul chiudi e non sul resto della thumb
        if (ev.target.tagName === 'BUTTON') {
          this.isSelected = false;
          this.$emit('closed');
        }
      },
      openDetail: function openDetail(ev) {
        var vm = this;

        // Se si è cliccato sul chiudi e non sul resto della thumb
        // termina la funzione
        if (ev.target.tagName === 'BUTTON') {
          return;
        }

        this.isSelected = true;

        // Calcola quanti elementi sono visibili per riga
        var itemsPerRow = function () {
          var flexBasis = window.getComputedStyle(vm.$el).getPropertyValue('flex-basis');
          var result = Math.floor(100 / parseInt(flexBasis));
          return result;
        }();

        var currentRow = (Math.floor(this.index / itemsPerRow) + 1) * 10;

        vm.$emit('selected', {
          row: currentRow,
          id: vm.index,
          json: vm.json
        });
      }
    },
    mounted: function mounted() {
      var vm = this;

      vm.getRow();
      var checkWidth = _.debounce(function () {
        vm.getRow();
      }, 200);

      $(window).resize(checkWidth).resize();
    }
  });
  // colorTile()


  /* ------------------------------
    Pannello dettaglio
  ------------------------------ */
  Vue.component('grid-detail', {
    props: {
      data: {
        type: Object
      },
      isOpen: {
        type: Boolean,
        required: true
      },
      row: {
        type: Number,
        required: true
      }
    },
    watch: {
      // Riaggancia eventi quando viene caricato un nuovo json
      data: function data(olddata, newdata) {
        var _this = this;

        Vue.nextTick(function () {
          var vm = _this,
              $slider = $('.slider', _this.$el);

          anime({
            targets: "html, body",
            scrollTop: [window.pageYOffset, vm.$el.getBoundingClientRect().top + window.scrollY - 200],
            duration: 800,
            easing: 'easeOutQuad'
          });

          $slider.is('.slick-slider') ? $slider.slick('unslick') : null;

          $slider.slick({
            autoplay: true,
            autoplaySpeed: 4000,
            dots: false,
            arrows: false,
            variableWidth: true,
            slidesToShow: 1
          });

          $("[data-fancybox]", _this.$el).fancybox({
            infobar: true,
            toobar: true,
            buttons: ['close']
          });

          if (window.WishListSetup) window.WishListSetup();
        });
      }
    },
    mounted: function mounted() {}
  });
  // grid-content

  Vue.component('color-tile-thumb', {
    props: {
      w: {
        type: Number,
        required: true
      },
      h: {
        type: Number,
        required: true
      }
    },
    template: '<svg xmlns="http://www.w3.org/2000/svg" :viewBox="\'0 0 \' + w + \' \' + h" :style="\'width: \' + w"><g style="fill:none; stroke: #fff"><rect :width="w" :height="h" style="stroke:none"/><rect x="0.5" y="0.5" :width="w-1" :height="h-1" style="fill:none" /></g></svg>'
  });

  /* ------------------------------
  Main app
  ------------------------------ */
  new Vue({
    el: '.scheda__colori',
    data: {
      colorsData: undefined,
      colorsFilteredList: [],
      detailData: undefined,
      filterKey: '',
      filterKeys: ['kerlite', '14mm', '20mm'],
      openId: 0,
      openRow: 0,
      isDetailOpen: false
    },
    methods: {
      closeDetail: function closeDetail() {
        this.isDetailOpen = false;
      },


      // Filtro "additivo" (non usato)
      filter: function filter(key) {
        var vm = this,
            keyIndex = this.filterKeys.indexOf(key);
        this.isDetailOpen = false;
        keyIndex === -1 ? this.filterKeys.push(key) : this.filterKeys.splice(keyIndex, 1);
        this.colorsFilteredList = this.colorsData.filter(function (item) {
          var has = false;
          vm.filterKeys.forEach(function (filter) {
            return item.tech.includes(filter) ? has = true : null;
          });
          return has;
        });
      },


      // Filtro "esclusivo"
      // Permette di selezionare solo una tecnologia alla volta
      filterInverted: function filterInverted(key) {
        var vm = this;
        vm.isDetailOpen = false;
        console.log(key);

        if (key !== vm.filterKey) {
          vm.filterKey = key;
          vm.colorsFilteredList = vm.colorsData.filter(function (item) {
            return item.tech.includes(key);
          });
        } else {
          vm.filterKey = '';
          vm.colorsFilteredList = vm.colorsData;
        }

        return;

        keyIndex === -1 ? vm.filterKeys.push(key) : vm.filterKeys.splice(keyIndex, 1);
        vm.colorsFilteredList = vm.colorsData.filter(function (item) {
          var has = false;
          vm.filterKeys.forEach(function (filter) {
            return item.tech.includes(filter) ? has = true : null;
          });
          return has;
        });
      },
      loadDetailsJSON: function loadDetailsJSON(ev) {
        var _this2 = this;

        $.get(ev.json).done(function (response) {
          Vue.nextTick(function () {
            _this2.detailData = response;
            _this2.openRow = ev.row;
            _this2.openId = ev.id;
            _this2.isDetailOpen = true;
          });
        }).fail(function (err) {
          _this2.isDetailOpen = false;
          alert('Error loading JSON');
        });
      },
      setOpenRow: function setOpenRow(ev) {
        this.loadDetailsJSON(ev);
      }
    },
    mounted: function mounted() {
      this.colorsData = colori;
      this.colorsFilteredList = colori;
    }
  });
}
//# sourceMappingURL=color-grid.js.map
