'use strict';

{

  $(function () {

    var $menu = $('.main-menu'),
        $menuWrapper = $('.main-menu .wrapper'),
        $primary = $menuWrapper.find('.primary'),
        mobileBreakpoint = 1200,
        $mobileToggler = $(".mobile-toggler", $menuWrapper);

    $('> li:has(.megamenu)', $primary).addClass('menu-dropdown-icon');

    $('> ul > li > ul:not(:has(ul))', $menuWrapper).addClass('normal-sub');

    $("> li", $primary)

    // Desktop  
    .on('mouseenter', function (ev) {
      var self = this;
      ev.preventDefault();
      if ($(window).width() >= mobileBreakpoint) {
        $('li', this).removeAttr('style');
        $('.megamenu', this).stop(true).slideDown(150, function () {
          anime({
            targets: self.querySelectorAll('li'),
            opacity: [0, 1],
            translateX: [-20, 0],
            easing: 'easeOutQuad',
            delay: function delay(el, i, l) {
              return i * 20;
            }
          });
        });
      }
    }).on('mouseleave', function (ev) {
      var self = this;
      ev.preventDefault();
      if ($(window).width() >= mobileBreakpoint) {
        $('li', this).removeAttr('style');
        $('.megamenu', this).stop(true).slideUp(150);
      }
    })

    // Mobile
    .click(function (ev) {
      var self = this,
          $thisMegamenu = $('.megamenu', self);
      // ev.preventDefault();      

      if ($(window).width() < mobileBreakpoint) {
        $('.megamenu').not($thisMegamenu).stop().slideUp(function () {
          $thisMegamenu.stop().slideToggle(200);
        });

        if (!$(this).is('.open')) {
          anime({
            targets: self.querySelectorAll('li'),
            opacity: [0, 1],
            translateX: [-20, 0],
            easing: 'easeOutQuad',
            delay: function delay(el, i, l) {
              return i * 80;
            }
          });
        }
        self.classList.toggle('open');
      }
    });
    ;

    $mobileToggler.click(function (e) {
      e.preventDefault();
      $menu.toggleClass('open');
      $mobileToggler.find('img').toggle();
      $primary.stop().slideToggle();
    });
    //when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story (thanks mwl from stackoverflow)

    var resizeHandler = _.debounce(function () {
      if (window.innerWidth > mobileBreakpoint) {
        $('.main-menu .primary').removeAttr('style');
      }
    }, 200);

    $(window).resize(resizeHandler).resize();

    var scrollHandler = _.debounce(function () {
      if (window.innerWidth > mobileBreakpoint) {
        window.scrollY > 0 ? $menu.addClass('scrolled') : $menu.removeClass('scrolled');
      }
    }, 200);

    $(window).scroll(scrollHandler).scroll();
  });
}
//# sourceMappingURL=main-menu.js.map
