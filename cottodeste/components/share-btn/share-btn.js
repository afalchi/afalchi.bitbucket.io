'use strict';

$(function () {

  $(document).on('click', '.btn-share', function (ev) {
    var self = this;
    $('.text-link-line', this).hide();
    $('.btn-share__social', this).show();

    anime({
      targets: self.querySelectorAll('.btn-share__social a'),
      opacity: [0, 1],
      translateX: [20, 0],
      duration: 50,
      delay: function delay(el, i, l) {
        return (l - i) * 50;
      }
    });
  });
});
//# sourceMappingURL=share-btn.js.map
