'use strict';

function previewPages() {
  var pageName = location.search.slice(1);
  $('[data-page]').not('[data-page="' + pageName + '"]').remove();
  document.body.className = pageName;
}

/* DOC READY */
$(function () {
  previewPages();
});

new Vue({
  el: '.trasf-dig',
  data: {
    blocks: [{
      target: 'ERP per <strong>imprese</strong>',
      name: 'Alyante',
      abstract: 'ALYANTE Enterprise è il sistema ERP di TeamSystem per le medie e le grandi imprese: completo, modulare, integrato, con una straordinaria esperienza utente. <br>         L’evoluzione del gestionale nell’era digitale.'
    }, {
      target: 'Piattaforma gestionale <strong>studi</strong>',
      name: 'Lynfa',
      abstract: 'LYNFA Studio è la piattaforma gestionale per gli studi e i professionisti che cercano qualcosa di più di un semplice software, un alleato che copra tutte le aree di necessità dello studio.'
    }]
  }
});

new Vue({
  el: '.ts-cards'
});

new Vue({
  el: '.quotes'
});

new Vue({
  el: '.blog',
  data: {
    blogs: [{
      title: '<strong>Energia digitale</strong>',
      titleBG: 'tit-bg--red',
      icons: 'ico-energia',
      abstract: 'Il portale di formazione online gratuita per aziende e professionisti sulla trasformazione digitale'
    }, {
      title: 'Blog di <strong>Alyante</strong>',
      titleBG: 'tit-bg--blue',
      icons: 'ico-aziende',
      abstract: 'Il Blog di ALYANTE che si rivolge alle piccole    e medie imprese'
    }, {
      title: 'Blog di <strong>Lynfa</strong>',
      titleBG: 'tit-bg--orange',
      icons: 'ico-privati',
      abstract: 'Il Blog LYNFA rivolto a commercialisti e consulenti del lavoro'
    }]
  }
});