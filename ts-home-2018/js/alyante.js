'use strict';

/*--------------------------------------------------
I N I T
www.websolute.it
website by websolute
--------------------------------------------------*/
var myHeight;

function menuMobile() {
    setTimeout(function () {
        if ($(window).width() < 767) {
            $('.navbar-toggle').on('click', function () {
                $(this).toggleClass('active');
                $('.top-menu').toggleClass('active');
                return false;
            });
        }
    }, 100);
}

function tornaSu() {
    $('#back-top a').on('click', function () {
        $('body,html').animate({ scrollTop: 0 }, 'slow');
        return false;
    });
}

function topImage() {
    $('.top-image').css({ 'margin-top': $('.header').outerHeight() });
}

function controllScroll(scrollo) {
    if (scrollo > $('.top-image').outerHeight()) {
        $('#back-top').addClass('active');
    } else {
        $('#back-top').removeClass('active');
    }

    myHeight = $('.background').outerHeight() - ($(window).height() + $('.cont-form').outerHeight() + $('.container_footer').outerHeight());
    //console.log(scrollo +'/'+ myHeight);


    if (scrollo >= myHeight) {
        $('#richiedi-fluttua').removeClass('active');
    } else {
        $('#richiedi-fluttua').addClass('active');
    }
}

function scrollPage() {
    $('header a').not($('header a.cta')).on('click', function () {
        myVar = $(this).attr('id');
        //console.log(myVar);
        $('body,html').stop().animate({ scrollTop: $('.' + myVar + '').offset().top - $('header').outerHeight() }, 'slow');
        if ($('.top-menu').hasClass('active')) {
            $('.navbar-toggle').click();
        }
        return false;
    });
    $('.cta.orange').on('click', function () {
        if ($("a#fancybox-close").length > 0) $("a#fancybox-close").click();else $("a.fancybox-close").click();
        $('body,html').stop().animate({ scrollTop: $('.cont-form').offset().top - $('header').outerHeight() }, 'slow');
        return false;
    });

    $('#richiedi-fluttua').on('click', function () {

        $('body,html').stop().animate({ scrollTop: $('.cont-form').offset().top - $('header').outerHeight() }, 'slow');
        return false;
    });
}

function tabEnterprise() {
    $('.tab-list a').on('click', function () {
        var myIndex = $(this).parent().index();
        //console.log(myIndex);
        $('.tab-list a').removeClass('on');
        $(this).addClass('on');
        $('.cont-tab .tab').removeClass('on');
        $('.cont-tab .tab:eq(' + myIndex + ')').addClass('on');
        return false;
    });
}

function slideClienti() {
    $('.slide-cliente').slick({
        dots: true
    });
}

function funzionalita() {
    if ($(window).width() > 768) {
        $('.box a').removeAttr('style');
        $('.box').each(function () {
            var myPadding = 0;
            var mywidth = $(this).width();
            var nDiv = $('a', this).size();
            var totDiv = 0;
            //console.log('mywidth=', mywidth);
            $('a', this).each(function () {
                var divwidth = Math.round($(this).width());
                //console.log('divwidth=', divwidth);
                totDiv = totDiv + divwidth;
            });
            //var divwidth = $('div', this).width();
            //console.log(mywidth + ' / ' + nDiv + ' / ' + totDiv);
            var myPadding = Math.round((mywidth - totDiv) / (nDiv * 2)) - nDiv - 2;
            //console.log(myPadding);
            $('a', this).css({ 'padding-left': myPadding, 'padding-right': myPadding });
        });
    }
}

function boxApp() {
    $('.box-app .col-md-2:eq(0)').not($('.lynfast .box-app .col-md-2:eq(0)')).addClass('col-sm-offset-1');
    $('.box-image').height($('.box-image').width());
    var hTxt = 0;
    $('.box-app .txt').each(function () {
        var myH = $(this).outerHeight();
        if (myH >= hTxt) {
            hTxt = myH;
        }
    });
    $('.box-app .txt').height(hTxt);
}

function tabApp() {
    if ($('.app-label').length > 0) {
        //$('.box-app .app-label:eq(0)').addClass('on');
        $('.box-app a').on('click', function () {
            closeTab();
            $('.tab-list-app .chiudi').show();
            var myIndexApp = $(this).parents('.app-label').index();
            //console.log(myIndexApp);
            $('.box-app .app-label').removeClass('on');
            $(this).parents('.app-label').addClass('on');
            $('.tab-list-app .tab').removeClass('on');
            $('.tab-list-app .tab:eq(' + myIndexApp + ')').addClass('on');
            return false;
        });
    }
}

function closeTab() {
    $('.tab-list-app .chiudi').on('click', function () {
        $('.tab-list-app .tab').removeClass('on');
        $('.app-label').removeClass('on');
        $(this).hide();
        return false;
    });
}

/*--------------------------------------------------
DOC READY
--------------------------------------------------*/
$(function () {

    // menuMobile();
    // topImage();
    tornaSu();
    scrollPage();
    tabEnterprise();
    slideClienti();
    boxApp();
    tabApp();

    if ($('.fancy-cont').length > 0) {
        $('.fancy-cont').fancybox({
            type: 'inline'
        });
    }
});

/*--------------------------------------------------
WIN RESIZE
--------------------------------------------------*/
$(window).resize(function () {
    // menuMobile();
    // topImage();
    boxApp();
});

$(window).on("orientationchange", function (event) {
    funzionalita();
});

/*--------------------------------------------------
WIN LOAD
--------------------------------------------------*/
$(window).load(function () {
    funzionalita();
});

/*--------------------------------------------------
WIN SCROLL
--------------------------------------------------*/
$(window).scroll(function (e) {
    controllScroll($(document).scrollTop());
});