'use strict';

/*----------------------------------------------------------
 *  Main nav
 *--------------------------------------------------------*/
function mainNav() {

  var mainNav = document.querySelector('.main-nav');

  // Ancore che aprono un sottomenu su mobile
  var dropdownTogglers = $('.main-nav a').filter(function (i, e) {
    return $(e).next().is('ul') || $(e).next().is('.dropdown-menu');
  });

  dropdownTogglers.addClass('dropdown-toggler');

  // Menu Desktop
  var menuDesktop = {
    active: false,
    init: function init() {
      var self = this;

      $('.main-nav .nav-link').on('mouseenter', function () {
        if (self.active) {
          $('.main-nav .dropdown-menu .menu').css('height', 'auto').menu("collapseAll", null, true);
        }
      });

      $(".dropdown-menu .menu").menu({
        classes: {
          "ui-menu": "highlight"
        },
        position: {
          my: "left top",
          at: "right top"
        },
        blur: function blur(event, ui) {},
        focus: function focus(event, ui) {

          var el = ui.item,
              $parent = $(el).parents('.menu'),
              $child = $(el).find('.ui-menu'),
              maxH = 0;

          if ($child.height()) {
            $child.height() >= $parent.height() ? $parent.height($child.height() + 'px') : $parent.height('auto');
          }
        }
      });
    }
  };

  // Menu mobile    
  var menuMobile = {
    init: function init() {
      dropdownTogglers.each(function (i, e) {
        $(e).on('click', function (ev) {
          ev.preventDefault();
          ev.stopImmediatePropagation();
          $(e).toggleClass('open').next().stop().slideToggle('slow');
        });
      });
    },
    destroy: function destroy() {
      dropdownTogglers.each(function (i, e) {
        $(e).off('click');
      });
    }
  };

  function checkWidth() {
    if (window.matchMedia('(min-width: 992px)').matches) {
      $('.main-nav > .navbar').addClass('fixed-top');
      if (!menuDesktop.active) {
        menuMobile.destroy();
        menuDesktop.init();
        menuDesktop.active = true;
      }
    } else {
      $('.main-nav > .navbar').removeClass('fixed-top');

      if (menuDesktop.active) {
        $(".dropdown-menu .menu").menu('destroy');
        menuDesktop.active = false;
      }

      menuMobile.init();
    }
  }

  function checkScroll() {
    // alert(1);
    $(window).scrollTop() > 0 ? mainNav.classList.add('main-nav--scrolled') : mainNav.classList.remove('main-nav--scrolled');
  }

  // Ricerca
  $('.main-nav__search .btn').hover(function () {
    $('.main-nav__search').addClass('main-nav__search--open');
  }, function () {
    $('.main-nav__search').removeClass('main-nav__search--open');
  }).click(function () {
    if (!$('.main-nav__search').hasClass('main-nav__search--open')) {
      $('.main-nav__search').addClass('main-nav__search--open');
    } else if ($('.main-nav__search input').val() === '') {
      return false;
    }
  });
  $('.main-nav__search input').on('focus', function () {
    this.placeholder = '';
  }).on('blur', function () {
    this.placeholder = this.getAttribute('aria-label');
  });

  dropdownTogglers.click(function (ev) {
    ev.preventDefault();
  });

  var debounced = _.debounce(function () {
    checkWidth();
    checkScroll();
  }, 200);

  $(window).resize(debounced).resize().scroll(debounced);
}

/* --------------------------------------------
  Hero
---------------------------------------------*/
function hero() {

  $('.hero__slider').on('init', function (ev, slider) {

    // Nasconde punti se c'è una sola slide (bug di slick)
    if (slider.options.slidesToShow >= slider.slideCount) {
      $(slider.$slider).css('margin-bottom', 0).find('.slick-dots').remove();
    }

    console.log(slider.$slides[0].querySelector('video'));

    if (slider.$slides[0].querySelector('video')) {
      var video = slider.$slides[0].querySelector('video'),
          play = video.play;
      video.load();

      video.addEventListener('canplaythrough', function () {
        video.play();
      });
    }
  }).slick({
    dots: true,
    arrows: false,
    fade: true,
    mobileFirst: true,
    prevArrow: '\n        <button type="button" class="slick-prev">\n          <img src="img/hero/arrow-prev.svg"  alt="">\n        </button>',
    nextArrow: '\n        <button type="button" class="slick-next">\n          <img src="img/hero/arrow-next.svg"  alt="">\n        </button>',
    responsive: [{
      breakpoint: 768,
      settings: {
        arrows: true
      }
    }]
  }).on('beforeChange', function (event, slick, current) {
    try {
      slick.$slides[current].querySelector('video').pause();
    } catch (err) {}
  }).on('afterChange', function (event, slick, current) {
    try {
      $(slick.$slides[current]).find('video')[0].play();
    } catch (err) {}
  });
}

/* -------------------------------------
  Intro
--------------------------------------*/
function intro() {

  var $navItem = $('.intro__nav-item'),
      $slider = $('.intro__slider');

  $navItem.first().addClass('intro__nav-item--active');

  $navItem.click(function () {
    $slider.slick('slickGoTo', $(this).index());
    $navItem.not(this).removeClass('intro__nav-item--active');
    this.classList.add('intro__nav-item--active');
  });

  $slider.on('init', function (ev, slider) {
    // Nasconde punti se c'è una sola slide (bug di slick)
    if (slider.options.slidesToShow >= slider.slideCount) {
      $(slider.$slider).css('margin-bottom', 0).find('.slick-dots').remove();
    }
  }).slick({
    mobileFirst: true,
    slidesToShow: 1,
    arrows: true,
    dots: false,
    draggable: false,
    // autoplay: true,
    swipe: false,
    fade: true,
    prevArrow: '\n        <button type="button" class="slick-prev">\n          <img src="img/ui/arrow-prev.svg"  alt="">\n        </button>',
    nextArrow: '\n        <button type="button" class="slick-next">\n          <img src="img/ui/arrow-next.svg"  alt="">\n        </button>'
  }).on('beforeChange', function (ev, slider, current, next) {
    $navItem.not(':eq(' + next + ')').removeClass('intro__nav-item--active').end().eq(next).addClass('intro__nav-item--active');
  });
}

/* -------------------------------------
  Mercati Verticali
--------------------------------------*/
function businessEvolution() {
  $('.business-evolution__slider').on('init', function (ev, slider) {
    // Nasconde punti se c'è una sola slide (bug di slick)
    if (slider.options.slidesToShow >= slider.slideCount) {
      $(slider.$slider).css('margin-bottom', 0).find('.slick-dots').remove();
    }
  }).slick({
    mobileFirst: true,
    slidesToShow: 1,
    dots: true,
    arrows: false,
    infinite: false,
    responsive: [{
      breakpoint: 576,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 992,
      settings: 'unslick'
    }]
  });
}

/* -------------------------------------
    Lanci
--------------------------------------*/
function lanci() {
  $('.lanci__slider').on('init', function (ev, slider) {
    // Nasconde punti se c'è una sola slide (bug di slick)
    if (slider.options.slidesToShow >= slider.slideCount) {
      $(slider.$slider).css('margin-bottom', 0).find('.slick-dots').remove();
    }
  }).slick({
    mobileFirst: true,
    slidesToShow: 1,
    dots: true,
    arrows: false,
    responsive: [{
      breakpoint: 576,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 992,
      settings: {
        slidesToShow: 4
      }
    }]
  });
}

/* -------------------------------------
  Quotes
--------------------------------------*/
function quotes() {
  $('.quotes__slider').on('init', function (ev, slider) {
    // Nasconde punti se c'è una sola slide (bug di slick)
    if (slider.options.slidesToShow >= slider.slideCount) {
      $(slider.$slider).css('margin-bottom', 0).find('.slick-dots').remove();
    }
  }).slick({
    mobileFirst: true,
    slidesToShow: 1,
    autoplay: true,
    pauseOnHover: true,
    arrows: true,
    dots: true,
    responsive: [{
      breakpoint: 576,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 992,
      settings: {
        slidesToShow: 3
      }
    }]
  });
}

/* -------------------------------------
  Blog
--------------------------------------*/
function blog() {
  $('.blog__slider').on('init', function (ev, slider) {
    // Nasconde punti se c'è una sola slide (bug di slick)
    if (slider.options.slidesToShow >= slider.slideCount) {
      $(slider.$slider).css('margin-bottom', 0).find('.slick-dots').remove();
    }
  }).slick({
    mobileFirst: true,
    slidesToShow: 1,
    arrows: false,
    dots: true,
    responsive: [{
      breakpoint: 576,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    }]
  });
}

$(function () {

  mainNav();
  hero();
  intro();
  businessEvolution();
  lanci();
  quotes();
  blog();
});